

#include <basicmac.h>
#include <hal/hal.h>
#include <FS.h>
#include <SPIFFS.h>
#include <CayenneLPP.h>
#include <LightTrackerGeofence.h>
#include <SPI.h>


#include "PCA9539.h"
#include "SparkFun_Ublox_Arduino_Library.h" 
#include "MCP3X21.h"
#include "esp_camera.h"
#include "driver/rtc_io.h"
#include "Seeed_BMP280.h"



#include <Wire.h>

// SX1262 has the following connections:
#define nssPin 15
#define rstPin 32
#define dio1Pin 4
#define busyPin 33

#define GpsON  ioport.digitalWrite(pa6, LOW);
#define GpsOFF ioport.digitalWrite(pa6, HIGH);
#define CameraON  ioport.digitalWrite(pb2, LOW);
#define CameraOFF ioport.digitalWrite(pb2, HIGH);
#define LedON  ioport.digitalWrite(pa0, HIGH);
#define LedOFF ioport.digitalWrite(pa0, LOW);
#define FILE_PHOTO "/photo.jpg"

//#define DEVMODE // Development mode. Uncomment to enable for debugging.


#define CAMERA_MODEL_AI_THINKER

#if defined(CAMERA_MODEL_AI_THINKER)
#define PWDN_GPIO_NUM     -1
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

#else
  #error "Camera model not selected"
#endif



//***************************** UPDATE HERE WITH YOUR DEVICE KEYS **************************************/

//You should copy device keys from Helium or TTN Console and update following keys. Please check out: https://github.com/lightaprs/LightTracker-1.0/wiki/Adding-Device-on-Helium-Console

// This EUI must be in little-endian format, so least-significant-byte (lsb)
// first. When copying an EUI from Helium Console or ttnctl output, this means to reverse the bytes. 

static const u1_t PROGMEM DEVEUI[8]={0x1F, 0xC0, 0xED, 0x46, 0x51, 0xF9, 0x81, 0x60}; //helium or ttn

// This EUI must be in little-endian format, so least-significant-byte (lsb)
// first. When copying an EUI from Helium Console or ttnctl output, this means to reverse the bytes.  

static const u1_t PROGMEM APPEUI[8]={0x32, 0x17, 0x39, 0x04, 0x86, 0xF9, 0x81, 0x60}; //helium or ttn

// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In practice, a key taken from Helium Console or ttnctl can be copied as-is.

static const u1_t PROGMEM APPKEY[16] = {0x6B, 0x1F, 0x78, 0x5B, 0x44, 0x2D, 0x31, 0x20, 0x85, 0xEB, 0xCA, 0x2D, 0xC6, 0x48, 0xC4, 0xB8}; //helium or ttn
//*****************************************************************************************************/


//************************** LoRaWAN Settings ********************
const unsigned TX_INTERVAL = 60000;  // Schedule TX every this many miliseconds (might become longer due to duty cycle limitations).

//try to keep telemetry size smaller than 51 bytes if possible. Default telemetry size is 45 bytes.
CayenneLPP telemetry(51);

// The LoRaWAN region to use, automatically selected based on your location. So GPS fix is necesarry
u1_t os_getRegion (void) { return Lorawan_Geofence_region_code; } //do not change this

// GEOFENCE 
uint8_t Lorawan_Geofence_no_tx  = 0; //do not change this
uint8_t Lorawan_Geofence_region_code = _REGCODE_UNDEF; //do not change this
uint8_t Lorawan_Geofence_special_region_code = _REGCODE_UNDEF; //do not change this

uint8_t lastLoRaWANRegion = _REGCODE_UNDEF; //do not change this

boolean OTAAJoinStatus = false; //do not change this.
int channelNoFor2ndSubBand = 8; //do not change this. Used for US915 and AU915 / TTN and Helium
uint32_t last_packet = 0; //do not change this. Timestamp of last packet sent. 


//pinmap for SX1262 LoRa module
#if !defined(USE_STANDARD_PINMAP)
const lmic_pinmap lmic_pins ={
     .nss = nssPin,
     .tx = LMIC_UNUSED_PIN,
     .rx = LMIC_UNUSED_PIN,
     .rst = rstPin,
     .dio = {/* DIO0 */ LMIC_UNUSED_PIN, /* DIO1 */ dio1Pin, /* DIO2 */ LMIC_UNUSED_PIN},
     .busy = busyPin,
     .tcxo = LMIC_CONTROLLED_BY_DIO3,
};
#endif // !defined(USE_STANDARD_PINMAP)

//************************** uBlox GPS  Settings ********************
long lastTime = 0; //Simple local timer. Limits amount if I2C traffic to Ublox module.
boolean gpsFix=false; //do not change this.
boolean ublox_high_alt_mode_enabled = false; //do not change this.
boolean gpsBMPSetup=false; //do not change this.
//********************************* Power Settings ******************************
int   battWait=60;    //seconds sleep if super capacitors/batteries are below battMin (important if power source is solar panel) 
float battMin=3.5;    // min Volts to TX. (Works with 3.3V too but 3.5V is safer) 
float gpsMinVolt=4.5; //min Volts for GPS to wake up. (important if power source is solar panel) //do not change this
//************************** Camera  Settings ********************
boolean CameraSetup=false; //do not change this.
//************************** BMP280  Settings ********************
float    bmp_pressure;
float    bmp_temperature ;
float    bmp_altitude ;
uint32_t MSL = 102009; // Mean Sea Level in Pa
//********************************* Misc Settings ******************************
int txCount = 1;
float voltage = 0;
const uint8_t address = 0x4D;
const uint16_t ref_voltage = 3300;  // in mV

uint8_t measurementSystem = 0; //0 for metric (meters, km, Celcius, etc.), 1 for imperial (feet, mile, Fahrenheit,etc.) 

BMP280 bmp280; //temp and pressure sensor
MCP3021 mcp3021(address); //ı2c analog battery measure
SFE_UBLOX_GPS myGPS; // gps
PCA9539 ioport(0x74); // ıo expander


void setup() {

  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
  Wire.begin(26,27); 
  delay(5000); //do not change this
  
  ioport.pinMode(pa6, OUTPUT); // GPS open and close port
  GpsON;
  Serial.println(F("GPS hazır.")); 

  ioport.pinMode(pb2, OUTPUT); // CAMERA open and close port
  CameraON;
  Serial.println(F("Kamera hazır."));
  ioport.pinMode(pa0, OUTPUT); // LED open and close port
  LedOFF;
   
  Serial.begin(115200);
  Serial.setDebugOutput(true); 
  
  // Wait up to 5 seconds for serial to be opened, to allow catching
  // startup messages on native USB boards (that do not reset when
  // serial is opened).
  unsigned long start = millis();
  while (millis() - start < 5000 && !Serial);

  Serial.println();
  Serial.println(F("Starting"));
  Serial.println();
  
  
  mcp3021.init(&Wire);
    
  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    ESP.restart();
  }
  else {
    delay(500);
    Serial.println("SPIFFS mounted successfully");
  }    
}

void loop() {

   
 //  if (((voltage > battMin) && gpsFix) || ((voltage > gpsMinVolt) && !gpsFix)) {}

   if(!CameraSetup){
    Serial.println(F("OV2640 Camera setup"));   
    setupCamera_BMP();
    CameraSetup = true; }

   if(!gpsBMPSetup){
    
    Serial.println(F("GPS setup"));   
    setupGPS();
    Serial.println(F("Searching for GPS fix...")); 
    gpsBMPSetup = true; }

   if(gpsFix) {
    // Let LMIC handle LoRaWAN background tasks
    os_runstep(); 
    }

  if (millis() - lastTime > 1000)
  {
    lastTime = millis(); //Update the timer

    long latitude = myGPS.getLatitude();
    Serial.print(F("Lat: "));
    Serial.print(latitude);

    long longitude = myGPS.getLongitude();
    Serial.print(F(" Long: "));
    Serial.print(longitude);
    Serial.print(F(" (degrees * 10^-7)"));

    long altitude = myGPS.getAltitude();
    Serial.print(F(" Alt: "));
    Serial.print(altitude);
    Serial.print(F(" (mm)"));

    byte SIV = myGPS.getSIV();
    Serial.print(F(" SIV: "));
    Serial.print(SIV);

    Serial.println();
    Serial.print(myGPS.getYear());
    Serial.print("-");
    Serial.print(myGPS.getMonth());
    Serial.print("-");
    Serial.print(myGPS.getDay());
    Serial.print(" ");
    Serial.print(myGPS.getHour());
    Serial.print(":");
    Serial.print(myGPS.getMinute());
    Serial.print(":");
    Serial.print(myGPS.getSecond());

    Serial.print("  Time is ");
    if (myGPS.getTimeValid() == false)
    {
      Serial.print("not ");
    }
    Serial.print("valid  Date is ");
    if (myGPS.getDateValid() == false)
    {
      Serial.print("not ");
    }
    Serial.print("valid");

    Serial.println();
  }



    

  // If TX_INTERVAL passed, *and* our previous packet is not still
  // pending (which can happen due to duty cycle limitations), send
  // the next packet.
  if ((millis() - last_packet > TX_INTERVAL && !(LMIC.opmode & (OP_JOINING|OP_TXRXPEND))) || !gpsFix){
  
//    GpsON; Mustafa abiye sor.Yukarıda zaten açık!!!
    delay(500);
/*
    if(!ublox_high_alt_mode_enabled){
      setupUBloxDynamicModel();
    }
*/
    
     // Calling myGPS.getPVT() returns true if there actually is a fresh navigation solution available.

    if (myGPS.getPVT() && (myGPS.getFixType() !=0) && (myGPS.getSIV() > 0)) {  
//      gpsFix=true;     
      
/*      
      checkRegionByLocation();

      if(lastLoRaWANRegion != Lorawan_Geofence_region_code) {
          Serial.println(F("Region has changed, force LoRaWAN OTAA Login")); 
          OTAAJoinStatus = false;
          lastLoRaWANRegion = Lorawan_Geofence_region_code;
        }

      if(!OTAAJoinStatus && (Lorawan_Geofence_no_tx == 0)){            
          Serial.println(F("LoRaWAN OTAA Login initiated..."));     
          startJoining();
          Serial.println(F("LoRaWAN OTAA Login success..."));     
          OTAAJoinStatus = true;
         
      }  */

     for (int i=0; i<10; i++){      
      ReadPressure();
      delay(100);
      voltage = readBatt();
      if(i=9){
      Wire.begin(27,26);
      }
      }
      
        
 /*      updateTelemetry();
      
      #if defined(DEVMODE)        
        Serial.print(F("Telemetry Size: "));
        Serial.print(telemetry.getSize());
        Serial.println(F(" bytes"));
      #endif   DEV MOD GELİŞTİRİLİRKEN BURASI GÜNCELLENECEKTİR. */

 /*     //need to save power
      if (readBatt() < 4.5) {
      
       GpsOFF;
       ublox_high_alt_mode_enabled = false; //gps sleep mode resets high altitude mode.
       delay(500);      
      }   ENERJİ HARCAMA DURUMLARI GELİŞTİRİLİRKEN BURASI GÜNCELLENECEKTİR. 
      
      if (Lorawan_Geofence_no_tx == 0) {
          sendLoRaWANPacket();
          Serial.println(F("LoRaWAN packet sent.."));  
        }                 */
       
   

    
/*
      #if defined(DEVMODE)
        printGPSandSensorData();
      #endif    }   */


   //this code block protecting serial connected (3V + 3V) super caps from overcharging by powering on GPS module.
  //GPS module uses too much power while on, so if voltage is too high for supercaps, GPS ON.
  
 /* if (readBatt() > 6.5) {
       GpsON;
       delay(500);
   }  ENERJİ HARCAMA DURUMLARI GELİŞTİRİLİRKEN BURASI GÜNCELLENECEKTİR.    */
  
 /*  else {

    GpsOFF;
    ublox_high_alt_mode_enabled = false; //gps sleep mode resets high altitude mode.     
    delay(battWait * 1000);
    
  } not: burası tümleşik kod yazarken loop üzerindeki ilk if kosuluna ait if-else yapısıdır. */
  
  delay(1000);

} } }


void  ReadVoltage(){
    
    uint16_t result = mcp3021.read();
    result = mcp3021.read();

    Serial.print(F("ADC: "));
    Serial.print(result);
    Serial.print(F(" mV: "));
    Serial.println(readBatt());

    delay(1000);}
    
float readBatt() {
  Wire.begin(26,27);
  float R1 = 56.0; // 56K
  float R2 = 10.0; // 10K
  float value = 0.0;
  do {
    for(int i=0;i<10;i++){
      value +=mcp3021.read();
    } 
    value=value/10;
    value = (value * 3.3) / 1024.0;
    value = value / (R2/(R1+R2));
  } while (value > 16.0);
    Serial.print(F(" mV: "));
    Serial.println(value);
  return value ;
}
    
void ReadPressure(){
    Wire.begin(26,27);
    
    bmp_pressure = bmp280.getPressure(); 
    bmp_temperature = bmp280.getTemperature();
    bmp_altitude =  bmp280.calcAltitude(MSL,bmp_pressure, bmp_temperature );

    Serial.print(F("Pressure = "));
    Serial.print(bmp_pressure);
    Serial.println(" Pa");


    Serial.print(F("Temperature = "));
    Serial.print(bmp_temperature);
    Serial.println(" *C");


    Serial.print(F("Altitude = "));
    Serial.print(bmp_altitude); 
    Serial.println(" m");
    LedOFF;
    
}

// Check if photo capture was successful
bool checkPhoto( fs::FS &fs ) {
  File f_pic = fs.open( FILE_PHOTO );
  unsigned int pic_sz = f_pic.size();
  return ( pic_sz > 100 );
}

// Capture Photo and Save it to SPIFFS
void capturePhotoSaveSpiffs( void ) {
 
  camera_fb_t * fb = NULL; // pointer
  bool ok = 0; // Boolean indicating if the picture has been taken correctly

  do {

    // Take a photo with the camera
    Serial.println("Taking a photo...");

    fb = esp_camera_fb_get();
    if (!fb) {
      Serial.println("Camera capture failed");
      return;
    }

    // Photo file name
    Serial.printf("Picture file name: %s\n", FILE_PHOTO);
    File file = SPIFFS.open(FILE_PHOTO, FILE_WRITE);

    // Insert the data in the photo file
    if (!file) {
      Serial.println("Failed to open file in writing mode");
    }
    else {
      file.write(fb->buf, fb->len); // payload (image), payload length
      Serial.print("The picture has been saved in ");
      Serial.print(FILE_PHOTO);
      Serial.print(" - Size: ");
      Serial.print(file.size());
      Serial.println(" bytes");
    }
    // Close the file
    file.close();
    esp_camera_fb_return(fb);

    // check if file has been correctly saved in SPIFFS
    ok = checkPhoto(SPIFFS);
  } while ( !ok );
}

//to join LoRaWAN network, every region requires different parameters. Please refer to : https://lora-alliance.org/resource_hub/rp2-1-0-3-lorawan-regional-parameters/
void startJoining() {
Serial.println(F("111111"));
    // LMIC init
    os_init(nullptr);
    Serial.println(F("2222222"));
    LMIC_reset();

 Serial.println(F("33333333"));
 // Start join
    LMIC_startJoining();   

    //DO NOT CHANGE following code blocks unless you know what you are doing :)
 Serial.println(F("4444444"));
    //Europe
    if(Lorawan_Geofence_region_code == _REGCODE_EU868) {

      Serial.println(F("Region EU868"));

      //A little hack for Russian region since BasicMAC does not officially support RU864-870. Tested on TTN and worked..   
      if(Lorawan_Geofence_special_region_code == _REGCODE_RU864) {
        Serial.println(F("Special Region RU864"));
        LMIC_setupChannel(0, 868900000, DR_RANGE_MAP(0, 2));
        LMIC_setupChannel(0, 869100000, DR_RANGE_MAP(0, 2));      
      } 
       //DR2 (SF10 BW125kHz)
       //SF10 is better/optimum spreading factor for high altitude balloons
       LMIC_setDrTxpow(2,KEEP_TXPOWADJ);        

     //Japan, Malaysia, Singapore, Brunei, Cambodia, Hong Kong, Indonesia, Laos, Taiwan, Thailand, Vietnam
      } else if (Lorawan_Geofence_region_code == _REGCODE_AS923) {
        Serial.println(F("Region AS923"));
        
     //A little hack for Korean region since BasicMAC does not officially support KR920-923. Tested on TTN and worked..
      if(Lorawan_Geofence_special_region_code == _REGCODE_KR920) {
        Serial.println(F("Special Region KR920"));        
        LMIC_setupChannel(0, 922100000, DR_RANGE_MAP(0, 2));
        LMIC_setupChannel(0, 922300000, DR_RANGE_MAP(0, 2));
        LMIC_setupChannel(0, 922500000, DR_RANGE_MAP(0, 2));
      }
           
       //DR2 (SF10 BW125kHz)
       //For AS923, DR2 join only since max payload limit is 11 bytes.
       LMIC_setDrTxpow(2,KEEP_TXPOWADJ); 

      //North and South America (Except Brazil)
      } else if (Lorawan_Geofence_region_code == _REGCODE_US915) {

        Serial.println(F("Region US915"));

       //DR0 (SF10 BW125kHz)
       //For US915, DR0 join only since max payload limit is 11 bytes.
       LMIC_setDrTxpow(0,KEEP_TXPOWADJ);
       //TTN and Helium only supports second sub band (channels 8 to 15)
       //so we should force BasicMAC to initiate a join with second sub band channels.
       LMIC_selectChannel(8); 
      
      //Australia and New Zeleand   
      } else if (Lorawan_Geofence_region_code == _REGCODE_AU915) {

         Serial.println(F("Region AU915"));
       //DR2 (SF10 BW125kHz)
       //For AU915, DR2 join only since max payload limit is 11 bytes.
       LMIC_setDrTxpow(2,KEEP_TXPOWADJ);
       //TTN and Helium only supports second sub band (channels 8 to 15)
       //so we should force BasicMAC to initiate a join with second sub band channels.
       LMIC_selectChannel(8);
        
      } else {
        
          LMIC_setDrTxpow(2,KEEP_TXPOWADJ);  
        
        }

    LMIC_setAdrMode(false);
    LMIC_setLinkCheckMode(0);  
    
    // Make sure the first packet is scheduled ASAP after join completes
    last_packet = millis() - TX_INTERVAL;


    // Optionally wait for join to complete (uncomment this is you want
    // to run the loop while joining).
    while ((LMIC.opmode & (OP_JOINING))) {
        os_runstep();
      }
  
    
}

// Telemetry size is very important, try to keep it lower than 51 bytes. Always lower is better.
void sendLoRaWANPacket(){
    
    //Europa
    if(Lorawan_Geofence_region_code == _REGCODE_EU868) {

        if(telemetry.getSize() < 52) {
            //DR2 (SF10 BW125kHz) max payload size is 51 bytes.
            LMIC_setDrTxpow(2,KEEP_TXPOWADJ);          
          } else if (telemetry.getSize() < 116){
            //DR3 (SF9 BW125kHz) max payload size is 115 bytes.
            LMIC_setDrTxpow(3,KEEP_TXPOWADJ);                                        
          } else {
            //DR4 (SF8 BW125kHz) max payload size is 222 bytes.
            LMIC_setDrTxpow(4,KEEP_TXPOWADJ);   
            
            }
      //Japan, Malaysia, Singapore, Brunei, Cambodia, Hong Kong, Indonesia, Laos, Taiwan, Thailand, Vietnam
      }  else if (Lorawan_Geofence_region_code == _REGCODE_AS923) {

        if(telemetry.getSize() < 54) {
             //DR3 (SF9 BW125kHz) max payload size is 53 bytes.
            LMIC_setDrTxpow(3,KEEP_TXPOWADJ);         
          } else if (telemetry.getSize() < 126){
            //DR4 (SF8 BW125kHz) max payload size is 125 bytes.
            LMIC_setDrTxpow(4,KEEP_TXPOWADJ);                                        
          } else {
            //DR5 (SF7 BW125kHz) max payload size is 222 bytes.
            LMIC_setDrTxpow(5,KEEP_TXPOWADJ);   
            
            }   

      //North and South America (Except Brazil) or Australia and New Zeleand 
      } else if (Lorawan_Geofence_region_code == _REGCODE_US915 || Lorawan_Geofence_region_code == _REGCODE_AU915) {

        //North and South America (Except Brazil)
        if (Lorawan_Geofence_region_code == _REGCODE_US915){
          
          if(telemetry.getSize() < 54) {
             //DR1 (SF9 BW125kHz) max payload size is 53 bytes.
              LMIC_setDrTxpow(1,KEEP_TXPOWADJ);         
          } else if (telemetry.getSize() < 126){
            //DR2 (SF8 BW125kHz) max payload size is 125 bytes.
            LMIC_setDrTxpow(2,KEEP_TXPOWADJ);                                        
          } else {
            //DR3 (SF7 BW125kHz) max payload size is 222 bytes.
            LMIC_setDrTxpow(3,KEEP_TXPOWADJ);               
          }          

        //Australia and New Zeleand                       
        } else if (Lorawan_Geofence_region_code == _REGCODE_AU915){
          
          if(telemetry.getSize() < 54) {
             //DR3 (SF9 BW125kHz) max payload size is 53 bytes.
            LMIC_setDrTxpow(3,KEEP_TXPOWADJ);         
          } else if (telemetry.getSize() < 126){
            //DR4 (SF8 BW125kHz) max payload size is 125 bytes.
            LMIC_setDrTxpow(4,KEEP_TXPOWADJ);                                        
          } else {
            //DR5 (SF7 BW125kHz) max payload size is 222 bytes.
            LMIC_setDrTxpow(5,KEEP_TXPOWADJ);             
            }          
        }
        
       
       //TTN and Helium only supports second sub band (channels 8 to 15)
       //so we should force BasicMAC use second sub band channels.
       LMIC_selectChannel(channelNoFor2ndSubBand);
       
       ++channelNoFor2ndSubBand;
       if(channelNoFor2ndSubBand > 15) {
          channelNoFor2ndSubBand = 8;
        }
     //India     
     } else if (Lorawan_Geofence_region_code == _REGCODE_IN865) {

        if(telemetry.getSize() < 52) {
            //DR2 (SF10 BW125kHz) max payload size is 51 bytes.
            LMIC_setDrTxpow(2,KEEP_TXPOWADJ);          
          } else if (telemetry.getSize() < 116){
            //DR3 (SF9 BW125kHz) max payload size is 115 bytes.
            LMIC_setDrTxpow(3,KEEP_TXPOWADJ);                                        
          } else {
            //DR4 (SF8 BW125kHz) max payload size is 222 bytes.
            LMIC_setDrTxpow(4,KEEP_TXPOWADJ);   
            
            }
      }     
      
    LMIC_setAdrMode(false);
    LMIC_setLinkCheckMode(0);
    
    LMIC_setTxData2(1, telemetry.getBuffer(), telemetry.getSize(), 0);
    last_packet = millis();
    txCount++;
    Serial.print(F("Packet queued..."));
  
}

void setupGPS() {

  Wire.begin(27,26);
  delay(1000);
  
  if (myGPS.begin() == false) //Connect to the Ublox module using Wire port
  {
    Serial.println(F("Ublox GPS not detected at default I2C address. Please check wiring. Freezing."));
    while (1)
      ;
  }

  // do not overload the buffer system from the GPS, disable UART output
  myGPS.setUART1Output(0); //Disable the UART1 port output 
  myGPS.setUART2Output(0); //Disable Set the UART2 port output
  myGPS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)

  //myGPS.enableDebugging(); //Enable debug messages over Serial (default)

  myGPS.setNavigationFrequency(2);//Set output to 2 times a second. Max is 10
  byte rate = myGPS.getNavigationFrequency(); //Get the update rate of this module
  Serial.print("Current update rate for GPS: ");
  Serial.println(rate);

  myGPS.saveConfiguration(); //Save the current settings to flash and BBR  
  
}

void setupCamera_BMP(){
  
  Wire.begin(26,27);
  CameraON;
  LedON;
  delay(1000);

  if (!bmp280.init()) {
    Serial.println("Device not connected or broken!");
  }

  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
  
  if(psramFound()){
    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 10;
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }

  // Initialize camera
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }
  
  capturePhotoSaveSpiffs(); 
  
}

/*
void printGPSandSensorData()
{
 
    lastTime = millis(); //Update the timer

    byte fixType = myGPS.getFixType();

    Serial.print(F("FixType: "));
    Serial.print(fixType);    

    int SIV = myGPS.getSIV();
    Serial.print(F(" Sats: "));
    Serial.print(SIV);

    float flat = myGPS.getLatitude() / 10000000.f;
    
    Serial.print(F(" Lat: "));
    Serial.print(flat);    

    float flong = myGPS.getLongitude() / 10000000.f;    
    Serial.print(F(" Long: "));
    Serial.print(flong);        

    float altitude = myGPS.getAltitude() / 1000;
    Serial.print(F(" Alt: "));
    Serial.print(altitude);
    Serial.print(F(" (m)"));

    //float speed = myGPS.getGroundSpeed() * 0.0036f;
    //SerialUSB.print(F(" Speed: "));
    //SerialUSB.print(speed);
    //SerialUSB.print(F(" (km/h)"));

    //long heading = myGPS.getHeading() / 100000;
    //SerialUSB.print(F(" Heading: "));
    //SerialUSB.print(heading);
    //SerialUSB.print(F(" (degrees)"));
        
    Serial.print(" Time: ");    
    Serial.print(myGPS.getYear());
    Serial.print("-");
    Serial.print(myGPS.getMonth());
    Serial.print("-");
    Serial.print(myGPS.getDay());
    Serial.print(" ");
    Serial.print(myGPS.getHour());
    Serial.print(":");
    Serial.print(myGPS.getMinute());
    Serial.print(":");
    Serial.print(myGPS.getSecond());
    
    Serial.print(" Temp: ");
    Serial.print(bmp.readTemperature());
    Serial.print(" C");
    
    Serial.print(" Press: ");    
    Serial.print(bmp.readPressure() / 100.0);
    Serial.print(" hPa");

    Serial.println();
   
} 
*/
void updateTelemetry() {

  float tempAltitudeLong = 0; //meters or feet
  float tempAltitudeShort = 0; //km or miles
  float tempSpeed = 0; //km or miles
  float tempTemperature = 0; //Celcius or Fahrenheit

  if(measurementSystem == 0){ //Metric
    
    tempAltitudeLong = myGPS.getAltitude() / 1000.f; //meters
    tempAltitudeShort = tempAltitudeLong / 1000.f; //kilometers   
    tempSpeed = myGPS.getGroundSpeed() * 0.0036f; //km/hour    
    tempTemperature = bmp_temperature; //Celsius   
    
   } else { //Imperial
    
    tempAltitudeLong = (myGPS.getAltitude() * 3.2808399)  / 1000.f;//feet
    tempAltitudeShort = tempAltitudeLong / 5280.f;//miles       
    tempSpeed = myGPS.getGroundSpeed() *  0.00223694f; //mile/hour    
    tempTemperature =  (bmp_temperature * 1.8f) + 32; //Fahrenheit      
  }    

  //latitude,longtitude,altitude,speed,course,sattelite,battery,temp,pressure

  telemetry.reset();// clear the buffer
  telemetry.addGPS(1, myGPS.getLatitude() / 10000000.f, myGPS.getLongitude() / 10000000.f, tempAltitudeLong);   // channel 3, coordinates and altitude (meters or feet)
  telemetry.addTemperature(2, tempTemperature); // Celcius or Fahrenheit
  telemetry.addAnalogInput(3, voltage); //Battery/Supercaps voltage
  telemetry.addDigitalInput(4, myGPS.getSIV()); //GPS sattelites in view
  telemetry.addAnalogInput(5, tempSpeed);  //km/h or mile/h
  telemetry.addDigitalInput(6, myGPS.getHeading() / 100000); //course in degrees   
  telemetry.addBarometricPressure(7, bmp_pressure / 100.f); //pressure  
  telemetry.addAnalogInput(8, tempAltitudeShort); //kilometers or miles
 // telemetry.addAccelerometer(9,myIMU.readFloatAccelX(),myIMU.readFloatAccelY()- 0.98f,myIMU.readFloatAccelZ());   

}


void checkRegionByLocation() {

  float tempLat = myGPS.getLatitude() / 10000000.f;
  float tempLong = myGPS.getLongitude() / 10000000.f;
  
  Lorawan_Geofence_position(tempLat,tempLong);
    
}


void os_getJoinEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}
void os_getNwkKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}

void onLmicEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println(F("EV_JOINED"));

            // Disable link check validation (automatically enabled
            // during join, but not supported by TTN at this time).
            LMIC_setLinkCheckMode(0);
            break;
        case EV_RFU1:
            Serial.println(F("EV_RFU1"));
            break;
        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
            break;
        case EV_TXCOMPLETE:
            Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
            if (LMIC.txrxFlags & TXRX_ACK)
              Serial.println(F("Received ack"));
            if (LMIC.dataLen) {
              Serial.print(F("Received "));
              Serial.print(LMIC.dataLen);
              Serial.println(F(" bytes of payload"));
            }
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
        case EV_SCAN_FOUND:
            Serial.println(F("EV_SCAN_FOUND"));
            break;
        case EV_TXSTART:
            Serial.println(F("EV_TXSTART"));
            break;
        case EV_TXDONE:
            Serial.println(F("EV_TXDONE"));
            break;
        case EV_DATARATE:
            Serial.println(F("EV_DATARATE"));
            break;
        case EV_START_SCAN:
            Serial.println(F("EV_START_SCAN"));
            break;
        case EV_ADR_BACKOFF:
            Serial.println(F("EV_ADR_BACKOFF"));
            break;

         default:
            Serial.print(F("Unknown event: "));
            Serial.println(ev);
            break;
    }
}
/*
void setupUBloxDynamicModel() {
    // If we are going to change the dynamic platform model, let's do it here.
    // Possible values are:
    // PORTABLE, STATIONARY, PEDESTRIAN, AUTOMOTIVE, SEA, AIRBORNE1g, AIRBORNE2g, AIRBORNE4g, WRIST, BIKE
    //DYN_MODEL_AIRBORNE4g model increases ublox max. altitude limit from 12.000 meters to 50.000 meters. 
    if (myGPS.setDynamicModel(DYN_MODEL_AIRBORNE4g) == false) // Set the dynamic model to DYN_MODEL_AIRBORNE4g
    {
      Serial.println(F("***!!! Warning: setDynamicModel failed !!!***"));
    }
    else
    {
      #if defined(DEVMODE)
        Serial.print(F("Dynamic platform model changed successfully! : "));
        Serial.println(myGPS.getDynamicModel());
      #endif  
    }
  
}
*/ 
