#include <Arduino.h>
#include <esp_camera.h>
#include "ssdv.h"
#include <RadioLib.h>
#include <SPI.h>
#include "PCA9539.h"
#include "Wire.h"
#include <FS.h>
#include <SPIFFS.h>
#include <stdlib.h>
#include <EEPROM.h>
#include "soc/rtc_cntl_reg.h"

// define the number of bytes you want to access
#define EEPROM_SIZE 11

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP 60        /* Time ESP32 will go to sleep (in seconds) */

SPIClass SPI2(HSPI);

int capture_interval = 30000; // microseconds between captures
long current_millis;
long last_capture_millis = 0;
static esp_err_t cam_err;
uint16_t res_len,lenn;

int8_t indexson;
uint8_t indexsayac=0;
int bufSize = 0;

int cur=0; //for process ssdv function 
uint16_t indexx=0,c=0; //for process ssdv function 

int8_t ssdvPacketCount_2=0,devamm=0;  // ssdvPacketCount_2 gönderdiğimiz 3lü paket sayısını tuttuğumuz değişken , devamm ise enerjiden paket durdurursak kaldığımız 3 lü paketten devam etmemizi sağlıyor. 25.paketin 2.datası gönderildi ax=3 olur son paketi gönderir.  
int cur3=0; // cur3 flash hafıza içindeki dosyanın içinde nerede kalacağını tuttuğumuz değişkendir.

int8_t ssdvPacketCount=0;  //for flashtolora functıon
int cur2=0; //for flashtolora functıon
int8_t p=0;
 
// CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     -1
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

// ssdv definitions
#define IMG_BUFF_SIZE               128         // size of the buffer feeding SSDV process
#define LORA_BUFFER                 256
#define FILE_PHOTO "/photo5.jpg"              
#define SSDVDATA "/kayıt9.txt"    // Resim datasını ssdvden okuyup flasha kayıt ederken oluşturduğumuz dosya bunu her resimde ismini değiştirmek gerek, dosyayı silip yeeniden yazsak bile eski datalar kalabiliyor.Test edildi!!!
uint8_t ssdvQuality = 4;                 // 0-7 corresponding to JPEG quality: 13, 18, 29, 43, 50, 71, 86 and 100

static const char callsign[] = "TA2MHR";        // maximum of 6 characters
ssdv_t ssdv;

uint8_t imgBuff[IMG_BUFF_SIZE];
uint8_t flashBuff[LORA_BUFFER]; //değiştirmeyin.
uint8_t ssdvBuff[LORA_BUFFER]; //değiştirmeyin.
uint8_t yeniBuff[LORA_BUFFER]; //değiştirmeyin.

int8_t imageID = 0;

const int HSPI_MISO = 12;
const int HSPI_MOSI = 13;
const int HSPI_SCK = 14;  
const int HSPI_CS = 15;

SX1262 radio = new Module(15, 4, 32, 33, SPI2);

PCA9539 ioport(0x74);

void save_photo();
void Photo_Flash_To_LoRa();
void cameraconfig();

void setup() {
  
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector
  
  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    ESP.restart();
  }
  else {
    delay(500);
    Serial.println("SPIFFS mounted successfully");
  }
 
  SPI2.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS);
  Wire.begin(26,27);
  ioport.pinMode(pb2, OUTPUT);
  ioport.digitalWrite(pb2, LOW); // Kamera Aktif.

  Serial.begin(115200);
  delay(1000);
  Serial.println("Start..");

  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin();
  //state=radio.setFrequency(868);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed(3), code "));
    Serial.println(state);
    while (true);
  }
 
  cameraconfig(); // Kamera konfigrasyonlarının yapıldığı yer.
  
  EEPROM.begin(EEPROM_SIZE); // Kameradan önce tanımlanmamalıdır yoksa kamerada hata vermektedir!!!
  
  save_photo();
 
}

void loop()
{
  /*
  current_millis = millis();
  if (current_millis - last_capture_millis > capture_interval) { // Take another picture
   
    save_photo();
    last_capture_millis = millis();
    //imageID++;
  }
  */
}

/*
uint16_t writeeprom(int8_t a,int8_t b,uint16_t dataa,uint16_t result ){

   byte resdusukbyte = lowByte(dataa);
   byte resyuksekbyte = highByte(dataa);
   EEPROM.write(a,resdusukbyte);
   EEPROM.write(b,resyuksekbyte);
   byte d = EEPROM.read(a);
   byte y = EEPROM.read(b);
   return result = d + (y<<8);
}  
*/

int iread(uint8_t* buffer,int numBytes,uint16_t cbIndex ){

File file2 = SPIFFS.open(FILE_PHOTO, FILE_READ);
 
 // have we reached past end of imagebuffer
  if((cbIndex + numBytes ) < lenn ){    
    bufSize = numBytes;  
  }
  else{  
    bufSize = lenn - cbIndex; // fb->len
    indexson=5; // işlemlerini sonlandırmak için    
}

// clear the dest buffer
memset(buffer,0,numBytes);

file2.seek(cbIndex,SeekSet); // index okuması için flash hafızadaki resim datasının bufferındaki cursoru ayarlıyoruz.

file2.read(buffer,bufSize);

if(indexson==5){
indexson=2;
file2.close();    
}
  return bufSize;
}

int process_ssdv(){
  
  // önceden_resim_cekildi_mi?
  EEPROM.write(7,58); // Fotograf çekildiğinde bu bu fonksiyona girip flasha fotograf cekildiğini anlamamız için bu değeri girdik reset alırsak bu değeri kontrol edip devam ediyoruz.
  EEPROM.commit();

  // initialise ssdv config structure
  ssdv_enc_init(&ssdv, SSDV_TYPE_NOFEC, (char *)callsign, imageID++, ssdvQuality);
  // set the output lora packet buffer for ssdv where the final ssdv packet will end up
  ssdv_enc_set_buffer(&ssdv, flashBuff);

  File filessdvv = SPIFFS.open(SSDVDATA, FILE_APPEND);
 
   while(1){
 
    while((c = ssdv_enc_get_packet(&ssdv)) == SSDV_FEED_ME)
    {          
      indexx += iread(imgBuff, IMG_BUFF_SIZE,indexx);
      Serial.print("index = ");
      Serial.println(indexx);    
      ssdv_enc_feed(&ssdv, imgBuff, IMG_BUFF_SIZE);    
    }
    
    if(indexx>640) indexsayac++;
    
    if((c == SSDV_EOI))
    {

     filessdvv.close();  
     EEPROM.write(10,indexsayac);
     EEPROM.commit();
     Serial.println("ssdv EOI");  
     Photo_Flash_To_LoRa();
     break;    
    }
    else if(c != SSDV_OK)
    {
        Serial.println("ssdv Error");
        break;
    }
   
      if (!filessdvv) {
        Serial.println("Failed to open file in append mode");
      }
      else {    
 
      filessdvv.seek(cur,SeekSet);  
      filessdvv.write(flashBuff,256);
      cur+=256; }
}

}


void eksikfoto_tamamla(){ // Enerji kesilirse veya kart reset alırsa ve resim çekilip flasha kayıt edilmişse bu fonksiyondan devam ediyoruz.


byte h = EEPROM.read(0);
byte p = EEPROM.read(1);

byte t = EEPROM.read(8);
byte s = EEPROM.read(9);
cur3 = t + (s<<8);
   
indexsayac=EEPROM.read(10);   

ssdvPacketCount_2=EEPROM.read(3); // En son kacıncı 3lü paket attık? 20. mi 25. mi ?

Serial.print("Güç sistemine ikinci giriş başarılı şu an flash hafızadaki konum:");
Serial.println(cur3); // Flashtan en son hangi kısımda kaldık kontrol ediyoruz.
   
File filessdv4 = SPIFFS.open(SSDVDATA, FILE_READ);  
       
while(1){
 
   if (!filessdv4){
        Serial.println("Failed to open file in reading mode");
    }
   else{
    
   memset(yeniBuff,0,256);   // önceden data olabilir bufferı doldurmadan temizlemek iyidir :)  
   filessdv4.seek(cur3,SeekSet); // cursor için önemli nokta cursor x.kısımda olsun oradaki değeri okuyarak başlıyor kursor 0 dan 256 kadar toplam 256 data okuyor 256.yı okumuyor! Ardından 256 yapıp oradan devam ediyoruz. 

   Serial.print("Read Cursor Pozisyonu=");
   Serial.println(filessdv4.position());  // Cursor pozisyonunu kontrol ediyoruz.
   filessdv4.read(yeniBuff,256);
   
   byte curdusukbyte = lowByte(cur3); // cursor 2 byte olduğu için eppromda iki ayrı yerde tutup birleştiriyoruz.
   byte curyuksekbyte = highByte(cur3);
   EEPROM.write(8,curdusukbyte);
   EEPROM.write(9,curyuksekbyte);
   EEPROM.commit(); // Commit olmazsa epproma yazmaz.
   
  }
   
    for(uint16_t i = 0; i < 256; i++) {
      yeniBuff[i] = yeniBuff[i+1];    } // LoRa max 255 byte gönderebiliyoruz o yüzden 1 byte eksik gönderip gatewayde ekleyeceğiz.
   
   // lora transmit
   Serial.print(" packet sent");
   Serial.println(ssdvPacketCount_2);
   uint8_t partBuff[91]; // LoRa bufferı
   
   EEPROM.write(2,imageID); // Resim görüntüsü tek sefer yazmak için üst tarafa aldık.
   EEPROM.write(3,ssdvPacketCount_2);
   EEPROM.commit();

   // Eğer kart reset alır veya güç problemi olursa  hangi parcanın kacıncısındayız onun kontrol ediyoruz. 25 paketin 1. parçası mı? 2.parçası mı? 3. parçası mı? 
   if(EEPROM.read(4)!=2)     devamm=EEPROM.read(4)+1; 
   else devamm=0;

   for(uint8_t a=devamm; a<3;a++){
   memset(partBuff, 0, 91); //buffer temizle

   partBuff[0]= 0x55;
   partBuff[1]= imageID;
   partBuff[2]= ssdvPacketCount_2;
   
   partBuff[3]= a;  
   partBuff[4]= a; 
   for(int z=0;z<85;z++)partBuff[(5+z)]=yeniBuff[z+(a*85)]; //Resmi aldığımız bufferdan LoRa'nın bufferina yazıyoruz.
   
   int state = radio.transmit(partBuff,90);//LORA_BUFFER;
   
   if (state == ERR_NONE){
      // the packet was successfully transmitted
      
      EEPROM.write(4,a);
      EEPROM.commit();  //Gönderme işlemi başarılıysa giden parcayı epproma yazıyoruz.
      Serial.print(EEPROM.read(4)); 
      
      if(a==2){
        ssdvPacketCount_2++;  
        EEPROM.write(3,ssdvPacketCount_2);

        cur3+=256; 

        byte curdusukkbyte = lowByte(cur3);
        byte curyuksekkbyte = highByte(cur3);
        EEPROM.write(8,curdusukkbyte);
        EEPROM.write(9,curyuksekkbyte);
        EEPROM.commit();  
      }
      
      Serial.println(F("success!"));

      // print measured data rate
      Serial.print(F("[SX1262] Datarate:\t"));
      Serial.print(radio.getDataRate());
      Serial.println(F(" bps"));
       
    } else if (state == ERR_PACKET_TOO_LONG) {
      // the supplied packet was longer than 256 bytes
      Serial.println(F("too long!"));

    } else if (state == ERR_TX_TIMEOUT) {
      // timeout occured while transmitting packet
      Serial.println(F("timeout!"));

    } else {
      // some other error occurred
      Serial.print(F("failed(2), code "));
      Serial.println(state);

    }

   delay(1000); // Bu kısım koymamız önemli yoksa cok hızlı oluyor resetleme olunca kalan parcayı eeproma yazamadan gitmiş oluyor o yuzden silmeyiniz!!!!
    
  }


   if(EEPROM.read(3)==10 && EEPROM.read(4)==2){
    Serial.println("Deep-Sleep giriliyor...");  
    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
    esp_deep_sleep_start();  
   }
   if(EEPROM.read(3)==15 && EEPROM.read(4)==2){
    Serial.println("Deep-Sleep giriliyor...");  
    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
    esp_deep_sleep_start();  
   }
   if(EEPROM.read(3)==20  && EEPROM.read(4)==2){
    Serial.println("Deep-Sleep giriliyor...");  
    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
    esp_deep_sleep_start();  
   }
   if(EEPROM.read(3)==30  && EEPROM.read(4)==2){
    Serial.println("Deep-Sleep giriliyor...");  
    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
    esp_deep_sleep_start();  
   }
   Serial.print(EEPROM.read(3));      
   memset(yeniBuff,0,256); 
 
   if(ssdvPacketCount_2==(indexsayac-1)){ //Resim toplam data uzunluğuna geldik mi veya geçtik mi?  
    
      for(int i=0; i<EEPROM_SIZE; i++)EEPROM.write(i,0); // Tüm eppromu temizliyoruz.
      EEPROM.commit();  
      SPIFFS.remove("/kayıt9.txt"); // SSDV den gelen dataları kayıt ettiğimiz dosya bir öncekini uçuş başlamadan önce sileceğiz. Yeni bir dosya ismi yazarak yeni ssdv datalarını oraya kayıt edeceğiz.
      filessdv4.close(); // Flashtaki acıp okuduğumuz dosyayı kapatıyoruz.
      Serial.print("SSDV EOI SONN !!!");      
      break;
    }

  }
 
}

void Photo_Flash_To_LoRa(){
        
    EEPROM.write(2,imageID);
    EEPROM.commit();    
   
    File filessdv2 = SPIFFS.open(SSDVDATA, FILE_READ);  
    if (!filessdv2){
        Serial.println("Failed to open file in read mode");
    }    
   
   while(1){ // SSDV kendi datasını oluşturup flasha yazmayı bitirdikten sonra kod buraya gelip flashtan ssdv datalarını okuyor.ve LoRa üzerinden gönderim yapıyor.      
   // lora transmit
   Serial.print(" packet sent");
   Serial.println(ssdvPacketCount);
   uint8_t partBuff[91];
         
   filessdv2.seek(cur2,SeekSet);  
   Serial.print("Read Cursor Pozisyonu=");
   Serial.println(filessdv2.position());  
   filessdv2.read(ssdvBuff,256);
   
   byte currdusukbyte = lowByte(cur2);
   byte curryuksekbyte = highByte(cur2);
   EEPROM.write(8,currdusukbyte);
   EEPROM.write(9,curryuksekbyte);
   EEPROM.commit();
   
   for(uint16_t i = 0; i < 256; i++) {
      ssdvBuff[i] = ssdvBuff[i+1]; } //LoRa buffer üzerinden max 255byte gönderebildiğimiz için 1 byte eksik gönderiyoruz. Gateway onu en sonda ekleme yapıyor.

   EEPROM.write(3,ssdvPacketCount);
   EEPROM.commit();
     
    for(uint8_t a=0; a<3;a++){
    memset(partBuff, 0, 90);//buffer temizle

    partBuff[0]= 0x55;
    partBuff[1]= imageID;
    partBuff[2]= ssdvPacketCount;
    partBuff[3]= a;
    partBuff[4]= a;
   for(int z=0;z<85;z++)partBuff[(5+z)]=ssdvBuff[z+(a*85)];
     
   int state = radio.transmit(partBuff,90); //LORA_BUFFER


    if (state == ERR_NONE) {
      // the packet was successfully transmitted
      
       EEPROM.write(4,a);  
       EEPROM.commit();
       Serial.print(EEPROM.read(4));
       
       if(a==2){
        ssdvPacketCount++; 
        EEPROM.write(3,ssdvPacketCount);

        cur2+=256;
         
        byte ccurrdusukbyte = lowByte(cur2);
        byte ccurryuksekbyte = highByte(cur2);
        EEPROM.write(8,ccurrdusukbyte);
        EEPROM.write(9,ccurryuksekbyte);
        EEPROM.commit();                         
      }
       
      Serial.println(F("success!"));

      // print measured data rate
      Serial.print(F("[SX1262] Datarate:\t"));
      Serial.print(radio.getDataRate());
      Serial.println(F(" bps"));
     

    } else if (state == ERR_PACKET_TOO_LONG) {
      // the supplied packet was longer than 256 bytes
      Serial.println(F("too long!"));

    } else if (state == ERR_TX_TIMEOUT) {
      // timeout occured while transmitting packet
      Serial.println(F("timeout!"));

    } else {
      // some other error occurred
      Serial.print(F("failed(2), code "));
      Serial.println(state);
    }
   delay(1000); // bu kısım önemli hızlıca loradan data gönderilirse epproma 3 lü parçanın 1.sini yazarken gatewaye 2. parca gidiyor karısılık oluyor gatewayde de bekleme yapıyoruz burada da!!
   
  }
   if(EEPROM.read(3)==5 && EEPROM.read(4)==2){
    Serial.println("Deep-Sleep giriliyor...");  
    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
    esp_deep_sleep_start();  
   }     
   
   Serial.print(EEPROM.read(3));  // Epproma bakıyoruz kac paket gönderdik.
   memset(ssdvBuff,0,256);

   if(ssdvPacketCount==(indexsayac-1)){ //Resim toplam data uzunluğuna geldik mi veya geçtik mi?  
        
      for(int i=0; i<EEPROM_SIZE; i++)EEPROM.write(i,0);
      EEPROM.commit();  
      SPIFFS.remove("/kayıt9.txt"); // SSDV den gelen dataları kayıt ettiğimiz dosya bir öncekini uçuş başlamadan önce sileceğiz. Yeni bir dosya ismi yazarak yeni ssdv datalarını oraya kayıt edeceğiz.
      filessdv2.close();
      Serial.print("!!!!!SSDV BİTTİ!!!");
      break;
   }
}    
  
}

void cameraconfig(){

  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
// 0-8 corresponfing to 320x240, 352x288, 640x480, 800x480, 1024x768, 1280x960, 1600x1200, 2048x1536, 2592x1944
//  FRAMESIZE_QVGA 320x240, FRAMESIZE_VGA 640x480, FRAMESIZE_SVGA 800x600
//  FRAMESIZE_XGA 1024x768, FRAMESIZE_UXGA 1600x1200, FRAMESIZE_QXGA 2048x1536
  config.frame_size = FRAMESIZE_VGA;
  config.jpeg_quality = 10;
  config.fb_count = 1;

  // camera init
  cam_err = esp_camera_init(&config);
  if (cam_err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", cam_err);
    return;
  }else Serial.println("Camera..OK");

  delay(2000);//kamera beyaz dengesi vb. ayarları yapmak için zamana ihtiyaç duyuyor 
}

void save_photo()
{
  
 SPIFFS.remove("/kayıt8.txt"); //Test sırasında yarıda kesilince gönderim bitmediğinden dosya yapısı acık kalıyor!!
 
Serial.print("CHECK : ");
Serial.println( EEPROM.read(7)); // Güç kesintisinden dolayı devam eden resim mi yoksa yeni resim mi eppromdan kontrol ediyoruz.Devam eden resimse kodu farklı devam ediyor.

if(EEPROM.read(7)==58){
   
   eksikfoto_tamamla();  
}

else{
 
  camera_fb_t *fb = NULL;  
  Serial.print("Taking picture: ");
  fb = esp_camera_fb_get();
 
  if (!fb) {
      Serial.println("Camera capture failed");
      return;
    }
   
       // Photo file name
    Serial.printf("Picture file name: %s\n", FILE_PHOTO);
    File file = SPIFFS.open(FILE_PHOTO, FILE_WRITE);
    delay(500);
    // Insert the data in the photo file
   
    if (!file) {
      Serial.println("Failed to open file in writing mode");
    }
    else {
      file.write(fb->buf, fb->len); // payload (image), payload length      
      res_len= (uint16_t)fb->len;
      Serial.print("The picture has been saved in ");
      Serial.print(FILE_PHOTO);
      Serial.print(" - Size: ");
      Serial.print(file.size());
      Serial.println(" bytes");    }

      byte resdusukbyte = lowByte(res_len);
      byte resyuksekbyte = highByte(res_len);
      EEPROM.write(0,resdusukbyte);
      EEPROM.write(1,resyuksekbyte);
      EEPROM.commit();
      byte d = EEPROM.read(0);
      byte y = EEPROM.read(1);
      lenn = d + (y<<8); // bu kısım iread fonksiyonunun resmi flash üzerinden okuyabilmesi için önemlidir.Kesinlikle kaldırmayalım!!!!
   
      file.close();  
 
      process_ssdv();  
      esp_camera_fb_return(fb);  }
}
