#include <RadioLib.h>
// SX1262 has the following connections:
// NSS pin:   8
// DIO1 pin:  3
// NRST pin:  9
// BUSY pin:  2
SX1262 radio = new Module(8, 3, 9, 2);

uint8_t LORA_received_packet        = 2;                            // 0: none, 1: UKHAS telemetry, 2: SSDV packet, 3: unrecognized packet


volatile bool receivedFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// this function is called when a complete packet
// is received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we got a packet, set the flag
  receivedFlag = true;
}

// SETUP FUNCTION ---------------------------------------------------------------------------------
void setup()
{
  SerialUSB.begin(115200);  
  while(!SerialUSB);                                           // Arduino <-> PC (Gateway)
   //SerialUSB.print(F("[SX1262] Initializing ... "));

  int state = radio.begin();
  if (state == ERR_NONE) {
    //SerialUSB.println(F("success!"));
  } else {
    SerialUSB.print(F("failed, code "));
    SerialUSB.println(state);
    while (true);
  }

  radio.setDio1Action(setFlag);

  //SerialUSB.print(F("[SX1262] Starting to listen ... "));
  state = radio.startReceive();
  if (state == ERR_NONE) {
    //SerialUSB.println(F("success!"));
  } else {
    SerialUSB.print(F("failed, code "));
    SerialUSB.println(state);
    while (true);
  }
}


// LOOP FUNCTION ----------------------------------------------------------------------------------
void loop()
{
if(receivedFlag) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    receivedFlag = false;

    // you can read received data as an Arduino String
    String str;

    uint8_t byteArr[256];
    int state = radio.readData(byteArr,255);

    if (state == ERR_NONE) {
      LORA_send_packet_to_gateway(LORA_received_packet, byteArr, 255);
    } else if (state == ERR_CRC_MISMATCH) {
      // packet was received, but is malformed
      SerialUSB.println(F("CRC error!"));

    } else {
      // some other error occurred
      SerialUSB.print(F("failed, code "));
      SerialUSB.println(state);

    }

    // put module back to listen mode
    radio.startReceive();

    // we're ready to receive more packets,
    // enable interrupt service routine
    enableInterrupt = true;
  }

}



void LORA_send_packet_to_gateway(uint8_t type, uint8_t data[], uint8_t pktLen)
{
  switch(type)
  {
    case 0: // 0: none, 1: UKHAS telemetry, 2: SSDV packet, 3: unrecognized packet
      SerialUSB.print("z:");                                                                                 // signal CRC error
      for(uint8_t i = 0; i < pktLen; i++) SerialUSB.write(data[i]);
      SerialUSB.print('\n');
      break;
      
    case 1:
      SerialUSB.print("t:");                                                                                 // signal normal UKHAS telemetry
      for(uint8_t i = 0; i < pktLen; i++) SerialUSB.write(data[i]);
      SerialUSB.print('\n');
      break;
      
    case 2:
      SerialUSB.print("j:");                                                                                 // signal SSDV packet
      SerialUSB.write(0x55);//ssdv için lora sınırına takılan ve eksik gönderilen ilk byte burada tekrar ekleniyor                                                                                 // insert the left out SYNC BYTE
      for(uint8_t i = 0; i < pktLen; i++) SerialUSB.write(data[i]);
      SerialUSB.print('\n');
      break;
      
    case 3:
      SerialUSB.print("p:");                                                                                 // signal unrecognized packet
      for(uint8_t i = 0; i < pktLen; i++) SerialUSB.write(data[i]);
      SerialUSB.print('\n');
      break;

    default:
      break;
  }
}









