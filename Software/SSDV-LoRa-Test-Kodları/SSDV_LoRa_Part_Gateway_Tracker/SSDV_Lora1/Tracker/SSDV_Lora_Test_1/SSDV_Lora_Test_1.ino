#include <Arduino.h>
#include <esp_camera.h>
//#include <TinyGPS++.h>
#include "ssdv.h"
#include <RadioLib.h>
#include <SPI.h>
#include "PCA9539.h"
#include "Wire.h"

SPIClass SPI2(HSPI);

//TinyGPSPlus GPS;

int capture_interval = 30000; // microseconds between captures
long current_millis;
long last_capture_millis = 0;
static esp_err_t cam_err;

int t,n=0;
//int counter = 0;

// CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     -1
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

// ssdv definitions
#define IMG_BUFF_SIZE               128         // size of the buffer feeding SSDV process 
#define LORA_BUFFER                 256
#define GPS_BUFFER                  256
uint8_t ssdvQuality = 7;                        // 0-7 corresponding to JPEG quality: 13, 18, 29, 43, 50, 71, 86 and 100


static const char callsign[] = "TA2NHP";        // maximum of 6 characters
ssdv_t ssdv;
uint8_t imgBuff[IMG_BUFF_SIZE];
uint8_t loraBuff[LORA_BUFFER];
char gpsMessage[GPS_BUFFER];
char tempStr[40];
int imageID = 0;

const int HSPI_MISO = 12;
const int HSPI_MOSI = 13;
const int HSPI_SCK = 14;  
const int HSPI_CS = 15;

SX1262 radio = new Module(15, 4, 32, 33, SPI2);

PCA9539 ioport(0x74);

void setup() {
  
  SPI2.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS); 
  Wire.begin(26,27);
  ioport.pinMode(pb2, OUTPUT);
  ioport.digitalWrite(pb2, LOW); // Kamera Aktif.
  
  Serial.begin(115200);
  delay(1000);
  

  Serial.println("Start..");

  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin();
  //state=radio.setFrequency(868);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed(3), code "));
    Serial.println(state);
    while (true);
  }

  
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
// 0-8 corresponfing to 320x240, 352x288, 640x480, 800x480, 1024x768, 1280x960, 1600x1200, 2048x1536, 2592x1944
//  FRAMESIZE_QVGA 320x240, FRAMESIZE_VGA 640x480, FRAMESIZE_SVGA 800x600
//  FRAMESIZE_XGA 1024x768, FRAMESIZE_UXGA 1600x1200, FRAMESIZE_QXGA 2048x1536
  config.frame_size = FRAMESIZE_SXGA; // 
  config.jpeg_quality = 2;
  config.fb_count = 2;

 
  // camera init
  cam_err = esp_camera_init(&config);
  if (cam_err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", cam_err);
    return;
  }else Serial.println("Camera..OK");


  delay(2000);//kamera beyaz dengesi vb. ayarları yapmak için zamana ihtiyaç duyuyor
  save_photo();
  
}

void loop()
{
  /*
  current_millis = millis();
  if (current_millis - last_capture_millis > capture_interval) { // Take another picture
    
    save_photo();
    last_capture_millis = millis();
    //imageID++;
  }
 */ 
}


int iread(uint8_t *buffer,int numBytes,camera_fb_t *fb, int fbIndex ){

  int bufSize = 0;
  // have we reached past end of imagebuffer
  if((fbIndex + numBytes ) < fb->len){
  
    bufSize = numBytes;
  }
  else{

    bufSize = fb->len - fbIndex;
  }
  // clear the dest buffer
  memset(buffer,0,numBytes);
  memcpy(buffer,&fb->buf[fbIndex],bufSize);
  return bufSize;
}



char Hex(uint8_t index)
  {
    char HexTable[] = "0123456789ABCDEF";
    
    return HexTable[index];
  }

int process_ssdv(camera_fb_t *fb){

  int index=0,c = 0,ssdvPacketCount=0;

  // initialise ssdv config structure
  ssdv_enc_init(&ssdv, SSDV_TYPE_NOFEC, (char *)callsign, imageID++, ssdvQuality);
  // set the output lora packet buffer for ssdv where the final ssdv packet will end up
  ssdv_enc_set_buffer(&ssdv, loraBuff);
  Serial.print("Sending Image: length = ");
  Serial.println(fb->len);

  while(1){
  
    while((c = ssdv_enc_get_packet(&ssdv)) == SSDV_FEED_ME)
    {
        index += iread(imgBuff, IMG_BUFF_SIZE,fb,index);
        Serial.print("index = ");
        Serial.print(index);
        ssdv_enc_feed(&ssdv, imgBuff, IMG_BUFF_SIZE);
    }
    
    if(c == SSDV_EOI)
    {
        Serial.println("ssdv EOI");
        break;
    }
    else if(c != SSDV_OK)
    {
        Serial.println("ssdv Error");
        break;
    }

    // move lora data backwrds 1 byte This seems needed for pits gateway to read it. (TT7)
    for(uint16_t i = 0; i < 256; i++) {// 256
      loraBuff[i] = loraBuff[i+1];
    }

    
    // lora transmit
    Serial.print(" packet sent");
    Serial.println(ssdvPacketCount);

    Serial.println(sizeof(loraBuff),DEC);
    int state = radio.transmit(loraBuff,(LORA_BUFFER -1));//LORA_BUFFER);
    
    if (state == ERR_NONE) {
    // the packet was successfully transmitted
    
    Serial.println(F("success!"));

    // print measured data rate
    Serial.print(F("[SX1262] Datarate:\t"));
    Serial.print(radio.getDataRate());
    Serial.println(F(" bps"));

  } else if (state == ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 256 bytes
    Serial.println(F("too long!"));

  } else if (state == ERR_TX_TIMEOUT) {
    // timeout occured while transmitting packet
    Serial.println(F("timeout!"));

  } else {
    // some other error occurred
    Serial.print(F("failed(2), code "));
    Serial.println(state);

  }
    ssdvPacketCount++;
    delay(10);  
  }
}

void save_photo()
{

  Serial.print("Taking picture: ");
  camera_fb_t *fb = esp_camera_fb_get();
  //delay(10);
  //fb = esp_camera_fb_get();
  process_ssdv(fb);
  esp_camera_fb_return(fb);
}
