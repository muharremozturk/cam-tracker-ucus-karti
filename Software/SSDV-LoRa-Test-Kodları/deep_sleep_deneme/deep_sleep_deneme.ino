    /////////////////////////////////////////////////////////////////
   //             ESP32 Deep Sleep Example 1             v1.00    //
  //       Get the latest version of the code here:              //
 //       http://educ8s.tv/esp32-deep-sleep-tutorial            //
/////////////////////////////////////////////////////////////////

#include "PCA9539.h"
#include "Wire.h"
#include <RadioLib.h>
#include "SPI.h"
SPIClass SPI2(HSPI);



SX1262 radio = new Module(15, 4, 32, 33,SPI2);



PCA9539 ioport(0x74); 


#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP 60        /* Time ESP32 will go to sleep (in seconds) */

RTC_DATA_ATTR int bootCount = 0;


void setup(){

  Wire.begin(26,27);    

  ioport.pinMode(pa0, OUTPUT); // led açma kapatma portu
  ioport.pinMode(pb2, OUTPUT); // kamera açma kapatma portu
  ioport.pinMode(pa6, OUTPUT); // gps acma kapata portu */
  delay(100);

  Serial.begin(115200);

  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;  
  const int HSPI_CS = 15;

  SPI2.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS); 

  delay(500);

 ioport.digitalWrite(pa0,LOW); 
 Serial.print("led kapalı"); 

   delay(500);
   
 ioport.digitalWrite(pb2,HIGH); 
 Serial.print("kamera kapalı");  
   delay(500);
   

 ioport.digitalWrite(pa6,HIGH); 
 Serial.print("gps kapalı"); 
 delay(500);

 radio.sleep(); // lora uykuya...
 delay(500);
 
  if(bootCount == 0) //Run this only the first time
  {
      
      bootCount = bootCount+1;
      Serial.print("ilk çalışma anı");
  }else
  {
   
      Serial.print("ilk çalışma anı bootcound different for zero");     
  }
  
  delay(3000);


  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  esp_deep_sleep_start();
}

void loop(){
  
}
