#include <RadioLib.h>
// SX1262 has the following connections:
// NSS pin:   8
// DIO1 pin:  3
// NRST pin:  9
// BUSY pin:  2
SX1262 radio = new Module(8, 3, 9, 2);

uint8_t LORA_received_packet        = 2;                            // 0: none, 1: UKHAS telemetry, 2: SSDV packet, 3: unrecognized packet
uint8_t lastID=0;
uint8_t lastPacket=0;
uint8_t lastPart=0;
uint8_t lastByteArr[256];

volatile bool receivedFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// this function is called when a complete packet
// is received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we got a packet, set the flag
  receivedFlag = true;
}

// SETUP FUNCTION ---------------------------------------------------------------------------------
void setup()
{
  SerialUSB.begin(115200);  
  while(!SerialUSB);                                           // Arduino <-> PC (Gateway)
   //SerialUSB.print(F("[SX1262] Initializing ... "));

  int state = radio.begin();
  if (state == ERR_NONE) {
    //SerialUSB.println(F("success!"));
  } else {
    SerialUSB.print(F("failed, code "));
    SerialUSB.println(state);
    while (true);
  }

  radio.setDio1Action(setFlag);

  //SerialUSB.print(F("[SX1262] Starting to listen ... "));
  state = radio.startReceive();
  if (state == ERR_NONE) {
    //SerialUSB.println(F("success!"));
  } else {
    SerialUSB.print(F("failed, code "));
    SerialUSB.println(state);
    while (true);
  }

   memset(lastByteArr, 0, 256);
}



// LOOP FUNCTION ----------------------------------------------------------------------------------
void loop()
{
if(receivedFlag) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    receivedFlag = false;

    // you can read received data as an Arduino String
    String str;
    uint8_t byteArr[134]; // Loradan gelen data 3 lü datada 90 - 6 lı datada 47 - 2 li datada ..
    
    int state = radio.readData(byteArr,133);  // 3 lü datada 90 - 6 lı datada 47 - 2 li datada .. 
    
    
    if (state == ERR_NONE){      
      if(byteArr[0]==0x55){ // Paketin başındaki imza doğruysa devam ediyor.
        lastPart=byteArr[3];
        if(byteArr[1]==lastID && byteArr[2]==lastPacket) // Paketin 1. ve 2. bytenındaki 256bytelık bir resim parçasının hangi 3 lük partisi yeni mi yoksa devam eden bir part mı?
        {
            uint8_t dataPos=byteArr[3]*128; //  3 lü paketten hangi parçanın geldiğini anlıyoruz.
            if(lastPart==1){
            for(int x=0;x<127;x++){
            lastByteArr[x+dataPos]=byteArr[x+5]; // 4 bytelık headerlarını atarak 3 lü parçayı birleştirdiğimiz yer               
            }}
            else{
            for(int x=0;x<128;x++){
            lastByteArr[x+dataPos]=byteArr[x+5]; // 4 bytelık headerlarını atarak 3 lü parçayı birleştirdiğimiz yer 
          } }   //ÖNEMLİ BURADA KAÇ PARÇAYA BÖLDÜĞÜMÜZ KISIMIDA GÖNDERDİK!!!!
          
//          lastPart=byteArr[3];
          SerialUSB.print(F("SAYAC:"));
          SerialUSB.println(lastPart);
         }else{// 3lü parçanın ilk parçası geldiğinde değilse değişkenleri sıfırlar
          lastID=byteArr[1];          
          lastPacket=byteArr[2];

          SerialUSB.print(F("Paket Sayısı:"));
          SerialUSB.println(lastPacket);
 
//       memset(lastByteArr, 0, 256); // burayı kaldrıdık. Tracker kodunda indexler flasha kayıt edilip lorayla gönderildiği zaman ilk reset aldığın normalde üstteki if yapısana girmesi gerekirken girmiyor test ettim. Bizzat gördüm o yüzden girmesede sıfırlama yapmayıp doğrudan paketi yolladık.

          uint8_t dataPos=byteArr[3]*128;         
          if(lastPart==1){
          for(int x=0;x<127;x++){
            lastByteArr[x+dataPos]=byteArr[x+5]; // 4 bytelık headerlarını atarak 3 lü parçayı birleştirdiğimiz yer               
            }}
            else{
            for(int x=0;x<128;x++){
            lastByteArr[x+dataPos]=byteArr[x+5]; // 4 bytelık headerlarını atarak 3 lü parçayı birleştirdiğimiz yer 
          } }            
          
          
          SerialUSB.print(F("SAYAC:"));
          SerialUSB.println(lastPart);
        }
      }

    if(lastPart==1){
        LORA_send_packet_to_gateway(LORA_received_packet, lastByteArr, 255);
        lastPart=0;
        memset(lastByteArr, 0, 256); //buffer temizle
        memset(byteArr, 0, 134); //buffer temizle
    }
      
    } else if (state == ERR_CRC_MISMATCH) {
      // packet was received, but is malformed
      SerialUSB.println(F("CRC error!"));

    } else {
      // some other error occurred
      SerialUSB.print(F("failed, code "));
      SerialUSB.println(state);
    }

    // put module back to listen mode
    radio.startReceive();

    // we're ready to receive more packets,
    // enable interrupt service routine
    enableInterrupt = true;
  }

}

void LORA_send_packet_to_gateway(uint8_t type, uint8_t data[], uint8_t pktLen)
{
  switch(type)
  {
    case 0: // 0: none, 1: UKHAS telemetry, 2: SSDV packet, 3: unrecognized packet
      SerialUSB.print("z:");                                                                                 // signal CRC error
      for(uint8_t i = 0; i < pktLen; i++) SerialUSB.write(data[i]);
      SerialUSB.print('\n');
      break;
      
    case 1:
      SerialUSB.print("t:");                                                                                 // signal normal UKHAS telemetry
      for(uint8_t i = 0; i < pktLen; i++) SerialUSB.write(data[i]);
      SerialUSB.print('\n');
      break;
      
    case 2:
      SerialUSB.print("j:");                                                                                 // signal SSDV packet
      SerialUSB.write(0x55);//ssdv için lora sınırına takılan ve eksik gönderilen ilk byte burada tekrar ekleniyor                                                                                 // insert the left out SYNC BYTE
      for(uint8_t i = 0; i < pktLen; i++) SerialUSB.write(data[i]);
      SerialUSB.print('\n');
      break;
      
    case 3:
      SerialUSB.print("p:");                                                                                 // signal unrecognized packet
      for(uint8_t i = 0; i < pktLen; i++) SerialUSB.write(data[i]);
      SerialUSB.print('\n');
      break;

    default:
      break;
  }
} 
