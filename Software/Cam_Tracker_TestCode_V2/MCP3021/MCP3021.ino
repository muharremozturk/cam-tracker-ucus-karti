#include <Wire.h>
#include "MCP3X21.h"  // https://github.com/pilotak/MCP3X21

const uint8_t address = 0x4D;
const uint16_t ref_voltage = 3300;  // in mV

MCP3021 mcp3021(address);

void setup() {
    Serial.begin(115200);
    Wire.begin(26, 27);
    mcp3021.init(&Wire);

}

void loop() {
    uint16_t result = mcp3021.read();
    result = mcp3021.read();

    Serial.print(F("ADC: "));
    Serial.print(result);
    Serial.print(F(", mV: "));
    Serial.println(readBatt());

    delay(1000);
}
float readBatt() {
  float R1 = 56.0; // 56K
  float R2 = 10.0; // 10K
  float value = 0.0;
  do {
    for(int i=0;i<10;i++){
      value +=mcp3021.read();
    } 
    value=value/10;
    value = (value * 3.3) / 1024.0;
    value = value / (R2/(R1+R2));
  } while (value > 16.0);
  return value ;
}