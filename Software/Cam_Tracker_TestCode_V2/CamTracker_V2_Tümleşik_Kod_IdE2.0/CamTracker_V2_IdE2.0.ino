#include "PCA9539.h"
#include "SparkFun_Ublox_Arduino_Library.h" 
#include "MCP3X21.h"
#include "esp_camera.h"
#include "SPI.h"
#include "driver/rtc_io.h"
#include <FS.h>
#include <SPIFFS.h>
#include <WiFi.h>
#include <Wire.h>
#include "Seeed_BMP280.h"
#include <RadioLib.h>


SPIClass SPI2(HSPI);


const uint8_t address = 0x4D;
const uint16_t ref_voltage = 3300;  // in mV

BMP280 bmp280;
MCP3021 mcp3021(address);
SFE_UBLOX_GPS myGPS;
PCA9539 ioport(0x74);
SX1262 radio = new Module(15, 4, 32, 33,SPI2);

uint32_t MSL = 102009; // Mean Sea Level in Pa
uint32_t SIV;
long lastTime = 0;
int  k=10;


float mcp_data[1];
float bmp280_data[3];
long gps_data[3];


void SendLoRa(void);
void ReadPressure(void);
void ReadVoltage(void);


#define CAMERA_MODEL_AI_THINKER

#if defined(CAMERA_MODEL_AI_THINKER)
#define PWDN_GPIO_NUM     -1
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

#else
  #error "Camera model not selected"
#endif

#define FILE_PHOTO "/photo.jpg"

struct Data{
byte byteArray[29];
int counter=0;
};

Data data;


void setup() {

  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
   
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Serial.println();

  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;
  const int HSPI_CS = 15;
  
  SPI2.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS);

  Serial.print(F("[SX1262] Initializing ... "));

  int transmissionState = radio.begin();
  if (transmissionState == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(transmissionState);
    while (true);
  }

  
  Wire.begin(26,27);
  mcp3021.init(&Wire);
   
  ioport.pinMode(pa6, OUTPUT); // GPS açma kapatma portu
  ioport.pinMode(pb2, OUTPUT); // KAMERA açma kapatma portu
  ioport.pinMode(pa0, OUTPUT); // LED acma kapata portu
  
  
  ioport.digitalWrite(pa6, LOW);
  delay(100);
  ioport.digitalWrite(pb2, LOW);
  delay(100);
  ioport.digitalWrite(pa0, HIGH);
  delay(3000);
 
  if (!bmp280.init()) {
    Serial.println("Device not connected or broken!");
  }

  
  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    ESP.restart();
  }
  else {
    delay(500);
    Serial.println("SPIFFS mounted successfully");
  }
   
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
  
  if(psramFound()){
    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 10;
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }

  // Initialize camera
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }
  
  capturePhotoSaveSpiffs();
     
  Wire.begin(27,26);
  if (myGPS.begin() == false) 
  {
    Serial.println(F("Ublox GPS not detected at default I2C address. Please check wiring. Freezing."));
    while (1)
      ;
  }
     myGPS.setI2COutput(COM_TYPE_UBX); 
     myGPS.saveConfiguration();

  Wire.begin(26,27);

     
}

void loop() {
    
    
   for (int i=0; i<5; i++){
    if(k==10){   
    ReadVoltage();
    delay(100); 
    ReadPressure();
    }
    if(i==4)  k=0;
   }


    if (millis() - lastTime > 1000)
    {   
     Wire.begin(27,26); 
    lastTime = millis(); //Update the timer

    gps_data[0] = myGPS.getLatitude();
    Serial.print(F("Lat: "));
    Serial.print(gps_data[0]);
  

    gps_data[1] = myGPS.getLongitude();
    Serial.print(F(" Long: "));
    Serial.print(gps_data[1]);
    Serial.print(F(" (degrees * 10^-7)"));
    

    gps_data[2] = myGPS.getAltitude();
    Serial.print(F(" Alt: "));
    Serial.print(gps_data[2]);
    Serial.print(F(" (mm)"));
   

    SIV = myGPS.getSIV();
    Serial.print(F(" SIV: "));
    Serial.print(SIV);
    

    Serial.println();
    Serial.print(myGPS.getYear());
    Serial.print("-");
    Serial.print(myGPS.getMonth());
    Serial.print("-");
    Serial.print(myGPS.getDay());
    Serial.print(" ");
    Serial.print(myGPS.getHour());
    Serial.print(":");
    Serial.print(myGPS.getMinute());
    Serial.print(":");
    Serial.print(myGPS.getSecond());

    Serial.print("  Time is ");
    if (myGPS.getTimeValid() == false)
    {
      Serial.print("not ");
    }
    Serial.print("valid  Date is ");
    if (myGPS.getDateValid() == false)
    {
      Serial.print("not ");
    }
    Serial.print("valid");

    Serial.println();
    Wire.begin(26,27);
    
  }

 SendLoRa();

}

void toByte() {
    for (int k = 0; k <1 ; k++) {
      floatAddByte(mcp_data[k], &data.counter, data.byteArray);
    }

    for (int k = 0; k < 3; k++) {
      floatAddByte(bmp280_data[k], &data.counter, data.byteArray);
    }

    for (int k = 0; k < 3; k++) {
      longAddByte(gps_data[k], &data.counter, data.byteArray);
    }
    byteAddByte(SIV, &data.counter, data.byteArray);
    
    data.counter = 0;
  
}


void SendLoRa(void){
  toByte();
  
  Serial.println("[SX1262] Transmitting packet ... ");
    
    int  transmissionState = radio.transmit(data.byteArray,29);

    if (transmissionState == ERR_NONE) {

      Serial.println(F("transmission finished!"));


    } else {
      Serial.print(F("failed, code "));
      Serial.println(transmissionState);
    }

    // wait a second before transmitting again
    delay(1000);    
     k=10;
}
  
void  ReadVoltage(){
    uint16_t result = mcp3021.read();
    result = mcp3021.read();

    Serial.print(F("ADC: "));
    Serial.print(result);
    Serial.print(F(", mV: "));
    Serial.println(readBatt());

    delay(1000);}


 void ReadPressure(){
    bmp280_data[0] = bmp280.getPressure(); 
    bmp280_data[1] = bmp280.getTemperature();
    bmp280_data[2]=  bmp280.calcAltitude(MSL,bmp280_data[0], bmp280_data[1] );

    Serial.print(F("Pressure = "));
    Serial.print(bmp280_data[0]);
    Serial.println(" Pa");


    Serial.print(F("Temperature = "));
    Serial.print( bmp280_data[1]);
    Serial.println(" *C");


    Serial.print(F("Altitude = "));
    Serial.print(bmp280_data[2]); 
    Serial.println(" m");
}

float readBatt() {
  float R1 = 56.0; // 56K
  float R2 = 10.0; // 10K
  float value = 0.0;
  do {
    for(int i=0;i<10;i++){
      value +=mcp3021.read();
    } 
    value=value/10;
    value = (value * 3.3) / 1024.0;
    value = value / (R2/(R1+R2));
  } while (value > 16.0);
  mcp_data[0]=value;
  return value ;
}

// Check if photo capture was successful
bool checkPhoto( fs::FS &fs ) {
  File f_pic = fs.open( FILE_PHOTO );
  unsigned int pic_sz = f_pic.size();
  return ( pic_sz > 100 );
}

// Capture Photo and Save it to SPIFFS
void capturePhotoSaveSpiffs( void ) {
 
  camera_fb_t * fb = NULL; // pointer
  bool ok = 0; // Boolean indicating if the picture has been taken correctly

  do {

    // Take a photo with the camera
    Serial.println("Taking a photo...");

    fb = esp_camera_fb_get();
    if (!fb) {
      Serial.println("Camera capture failed");
      return;
    }

    // Photo file name
    Serial.printf("Picture file name: %s\n", FILE_PHOTO);
    File file = SPIFFS.open(FILE_PHOTO, FILE_WRITE);

    // Insert the data in the photo file
    if (!file) {
      Serial.println("Failed to open file in writing mode");
    }
    else {
      file.write(fb->buf, fb->len); // payload (image), payload length
      Serial.print("The picture has been saved in ");
      Serial.print(FILE_PHOTO);
      Serial.print(" - Size: ");
      Serial.print(file.size());
      Serial.println(" bytes");
    }
    // Close the file
    file.close();
    esp_camera_fb_return(fb);

    // check if file has been correctly saved in SPIFFS
    ok = checkPhoto(SPIFFS);
  } while ( !ok );
}


void floatAddByte(float data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void longAddByte(long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}