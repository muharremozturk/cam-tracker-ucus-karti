#include <Arduino.h>
#include <SX126x-Arduino.h>
#include <LoRaWan-Arduino.h>
#include <SPI.h>

#define LORAWAN_APP_DATA_BUFF_SIZE 256  /**< Size of the data to be transmitted. */
#define LORAWAN_APP_TX_DUTYCYCLE 10000 /**< Defines the application data transmission duty cycle. 30s, value in [ms]. */
#define APP_TX_DUTYCYCLE_RND 1000   /**< Defines a random delay for application data transmission duty cycle. 1s, value in [ms]. */
#define JOINREQ_NBTRIALS 3         /**< Number of trials for the join request. */


#define LORAWAN_DEFAULT_DATARATE DR_3                   /*LoRaMac datarates definition, from DR_0 to DR_5*/
#define LORAWAN_DEFAULT_TX_POWER TX_POWER_5     /*LoRaMac tx power definition, from TX_POWER_0 to TX_POWER_15*/

DeviceClass_t CurrentClass = CLASS_A;          /* Class definition*/
LoRaMacRegion_t CurrentRegion = LORAMAC_REGION_EU868;    /* Region:EU868*/
lmh_confirm CurrentConfirm = LMH_UNCONFIRMED_MSG;  


struct Data{ // byte byte gönderme denemesi
   uint8_t  byteArray[18];
   int counter=0;
  };
  Data data; // byte byte gönderme 
  

hw_config hwConfig;

// ESP32 - SX126x pin configuration
int PIN_LORA_RESET = 32;   // LORA RESET
int PIN_LORA_NSS = 15;   // LORA SPI CS
int PIN_LORA_SCLK = 14;  // LORA SPI CLK
int PIN_LORA_MISO = 12;  // LORA SPI MISO
int PIN_LORA_DIO_1 = 4; // LORA DIO_1
int PIN_LORA_BUSY = 33;  // LORA SPI BUSY
int PIN_LORA_MOSI = 13;  // LORA SPI MOSI
int RADIO_TXEN = -1;   //2 LORA ANTENNA TX ENABLE ,issues kısmında bu txen ve rxen kesinlikle tanımlı olması gerektiğini söylemişler yoksa asla başarılı olamayacağını bildirmiişler.
int RADIO_RXEN = -1;   //25 LORA ANTENNA RX ENABLE ,Anten anahtarını kontrol etmeden gönderebilirsiniz,Join Accept  paketini asla alamazsınız.


int32_t chipTemp = 0; // Data sayısı

uint8_t nodeDeviceEUI[8] = {0x60, 0x81, 0xF9, 0x18, 0x5A, 0x36, 0xEF, 0xD7}; // MUST BE MSB

uint8_t nodeAppEUI[8] = {0x60, 0x81, 0xF9, 0x6E, 0x4B, 0xF1, 0x18, 0xD8}; // MUST BE MSB

uint8_t nodeAppKey[16] = {0xE7, 0xE7, 0x39, 0xAC, 0x01, 0xE2, 0x2A, 0x10, 0x92, 0x3F, 0x56, 0xA4, 0x6E, 0x5D, 0x3A, 0x2E}; // MUST BE MSB



// Foward declaration
/** LoRaWAN callback when join network finished */
static void lorawan_has_joined_handler(void);
/** LoRaWAN callback when join network failed */
static void lorawan_join_fail_handler(void);
/** LoRaWAN callback when data arrived */
static void lorawan_rx_handler(lmh_app_data_t *app_data);
/** LoRaWAN callback after class change request finished */
static void lorawan_confirm_class_handler(DeviceClass_t Class);
/** LoRaWAN callback after class change request finished */
static void lorawan_unconfirm_tx_finished(void);
/** LoRaWAN callback after class change request finished */
static void lorawan_confirm_tx_finished(bool result);
/** LoRaWAN Function to send a package */
static void send_lora_frame(void);
static uint32_t timers_init(void);

// APP_TIMER_DEF(lora_tx_timer_id);                                              ///< LoRa tranfer timer instance.
TimerEvent_t appTimer;                              ///< LoRa tranfer timer instance.
static uint8_t m_lora_app_data_buffer[LORAWAN_APP_DATA_BUFF_SIZE];        ///< Lora user application data buffer.
static lmh_app_data_t m_lora_app_data = {m_lora_app_data_buffer, 0, 0, 0, 0}; ///< Lora user application data structure.

/**@brief Structure containing LoRaWan parameters, needed for lmh_init()
 */
static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,LORAWAN_DEFAULT_DATARATE, LORAWAN_PUBLIC_NETWORK, JOINREQ_NBTRIALS, LORAWAN_DEFAULT_TX_POWER};
// DR_8
// LORAWAN_DEFAULT_DATARATE
/**@brief Structure containing LoRaWan callback functions, needed for lmh_init()
*/
static lmh_callback_t lora_callbacks = {BoardGetBatteryLevel, BoardGetUniqueId, BoardGetRandomSeed,
                    lorawan_rx_handler, lorawan_has_joined_handler, lorawan_confirm_class_handler, lorawan_join_fail_handler,
                    lorawan_unconfirm_tx_finished, lorawan_confirm_tx_finished};

/**@brief Setup function
 */
void setup()
{
  // Define the HW configuration between MCU and SX126x
  hwConfig.CHIP_TYPE = SX1262_CHIP;     // Example uses an eByte E22 module with an SX1262
  hwConfig.PIN_LORA_RESET = PIN_LORA_RESET; // LORA RESET
  hwConfig.PIN_LORA_NSS = PIN_LORA_NSS;  // LORA SPI CS
  hwConfig.PIN_LORA_SCLK = PIN_LORA_SCLK;   // LORA SPI CLK
  hwConfig.PIN_LORA_MISO = PIN_LORA_MISO;   // LORA SPI MISO
  hwConfig.PIN_LORA_DIO_1 = PIN_LORA_DIO_1; // LORA DIO_1
  hwConfig.PIN_LORA_BUSY = PIN_LORA_BUSY;   // LORA SPI BUSY
  hwConfig.PIN_LORA_MOSI = PIN_LORA_MOSI;   // LORA SPI MOSI
  hwConfig.RADIO_TXEN = -1;         // LORA ANTENNA TX ENABLE
  hwConfig.RADIO_RXEN = -1;         // LORA ANTENNA RX ENABLE
  hwConfig.USE_DIO2_ANT_SWITCH = true;    // Example uses an CircuitRocks Alora RFM1262 which uses DIO2 pins as antenna control
  hwConfig.USE_DIO3_TCXO = true;        // Example uses an CircuitRocks Alora RFM1262 which uses DIO3 to control oscillator voltage
  hwConfig.USE_DIO3_ANT_SWITCH = false;  // Only Insight ISP4520 module uses DIO3 as antenna control

  // Initialize Serial for debug output
  Serial.begin(115200);

  Serial.println("=====================================");
  Serial.println("SX126x LoRaWan test");
  Serial.println("=====================================");

  delay(5000);

  // Initialize Scheduler and timer
  uint32_t err_code = timers_init();
  if (err_code != 0)
  {
    Serial.printf("timers_init failed - %d\n", err_code);
  }

  // Initialize LoRa chip.
  err_code = lora_hardware_init(hwConfig);
  if (err_code != 0)
  {
    Serial.printf("lora_hardware_init failed - %d\n", err_code);
  }

  // Setup the EUIs and Keys
  lmh_setDevEui(nodeDeviceEUI);
  lmh_setAppEui(nodeAppEUI);
  lmh_setAppKey(nodeAppKey); // Only needed for OOTA registration


  // Initialize LoRaWan
  err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  if (err_code != 0)
  {
    Serial.printf("lmh_init failed - %d\n", err_code);
  }

  // For some regions we might need to define the sub band the gateway is listening to
  /// \todo This is for Dragino LPS8 gateway. How about other gateways???
  if (!lmh_setSubBandChannels(1))
  {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
  }

  Serial.println("Start Join request");
  // Start Join procedure
  lmh_join();
}

/**@brief Main loop
 */
void loop()
{
  // We are on FreeRTOS, give other tasks a chance to run
  // delay(100);
}

static void lorawan_join_fail_handler(void)
{
  Serial.println("OTAA joined failed");
  Serial.println("Check LPWAN credentials and if a gateway is in range");
  // Restart Join procedure
  Serial.println("Restart network join request");
}

/**@brief LoRa function for handling HasJoined event.
 */
static void lorawan_has_joined_handler(void)
{
#if (OVER_THE_AIR_ACTIVATION != 0)
  Serial.println("Network Joined");
#else
  Serial.println("OVER_THE_AIR_ACTIVATION != 0");

#endif
  lmh_class_request(CurrentClass);

  TimerSetValue(&appTimer, LORAWAN_APP_TX_DUTYCYCLE);
  TimerStart(&appTimer);
  // app_timer_start(lora_tx_timer_id, APP_TIMER_TICKS(LORAWAN_APP_TX_DUTYCYCLE), NULL);
  Serial.println("Sending frame");
  send_lora_frame();
}

/**@brief Function for handling LoRaWan received data from Gateway
 *
 * @param[in] app_data  Pointer to rx data
 */
static void lorawan_rx_handler(lmh_app_data_t *app_data)
{
  Serial.printf("LoRa Packet received on port %d, size:%d, rssi:%d, snr:%d\n",
          app_data->port, app_data->buffsize, app_data->rssi, app_data->snr);

  for (int i = 0; i < app_data->buffsize; i++)
  {
    Serial.printf("%0X ", app_data->buffer[i]);
  }
  Serial.println("");

  switch (app_data->port)
  {
  case 3:
    // Port 3 switches the class
    if (app_data->buffsize == 1)
    {
      switch (app_data->buffer[0])
      {
      case 0:
        lmh_class_request(CLASS_A);
        break;

      case 1:
        lmh_class_request(CLASS_B);
        break;

      case 2:
        lmh_class_request(CLASS_C);
        break;

      default:
        break;
      }
    }
    break;

  case LORAWAN_APP_PORT:
    // YOUR_JOB: Take action on received data
    break;

  default:
    break;
  }
}

/**@brief Function to confirm LORaWan class switch.
 *
 * @param[in] Class  New device class
 */
static void lorawan_confirm_class_handler(DeviceClass_t Class)
{
  Serial.printf("switch to class %c done\n", "ABC"[Class]);

  // Informs the server that switch has occurred ASAP
  m_lora_app_data.buffsize = 0;
  m_lora_app_data.port = LORAWAN_APP_PORT;
  lmh_send(&m_lora_app_data, CurrentConfirm);
}

/**
 * @brief Called after unconfirmed packet was sent
 *
 */
static void lorawan_unconfirm_tx_finished(void)
{
  Serial.println("Uncomfirmed TX finished");
}

/**
 * @brief Called after confirmed packet was sent
 *
 * @param result Result of sending true = ACK received false = No ACK
 */
static void lorawan_confirm_tx_finished(bool result)
{
  Serial.printf("Comfirmed TX finished with result %s", result ? "ACK" : "NAK");
}

/**@brief Function for sending a LoRa package.
 */
static void send_lora_frame(void)
{
  if (lmh_join_status_get() != LMH_SET)
  {
    //Not joined, try again later
    Serial.println("Did not join network, skip sending frame");
    return;
  }
  

 byte  deneme = 0x55;
 float tempAltitudeLong = 998.72; //meters or feet
 float tempAltitudeShort = 25.03; //km or miles
 float tempSpeed = 4.73; //km or miles
 float tempTemperature = 4; //Celcius or Fahrenheit
 byte  denemee = 0x56;
 memset(data.byteArray, 0, 18);//buffer temizle
 
 byteAddByte(deneme,&data.counter,data.byteArray);
 floatAddByte(tempAltitudeLong,&data.counter,data.byteArray);  
 floatAddByte(tempAltitudeShort,&data.counter,data.byteArray);  
 floatAddByte(tempSpeed,&data.counter,data.byteArray);  
 floatAddByte(tempTemperature,&data.counter,data.byteArray);  
 byteAddByte(denemee,&data.counter,data.byteArray);
 data.counter=0;

 memset(m_lora_app_data_buffer, 0, 18);//buffer temizle
 for(int i=0; i<18; i++) m_lora_app_data_buffer[i]=data.byteArray[i];

  m_lora_app_data.port = LORAWAN_APP_PORT;
  m_lora_app_data.buffsize = 18;

  Serial.print("Data: ");
  Serial.println((char *)m_lora_app_data_buffer);
  Serial.print("Size: ");
  Serial.println(m_lora_app_data.buffsize);
  Serial.print("Port: ");
  Serial.println(m_lora_app_data.port);

  lmh_error_status error = lmh_send(&m_lora_app_data, CurrentConfirm);
  if (error == LMH_SUCCESS)
  {
  }
  Serial.printf("lmh_send result %d\n", error);

}

/**@brief Function for handling a LoRa tx timer timeout event.
 */
static void tx_lora_periodic_handler(void)
{ 
  TimerSetValue(&appTimer, LORAWAN_APP_TX_DUTYCYCLE);
  TimerStart(&appTimer);
  Serial.println("Sending frame");
  send_lora_frame(); 
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static uint32_t timers_init(void)
{
  appTimer.timerNum = 3;
  TimerInit(&appTimer, tx_lora_periodic_handler);
  return 0; 
}



// SENSÖR VERİLERİNİ BYTE CEVİRME İŞLEMLERİNİ YAPTIĞIMIZ ALAN BUFFERA BYTE-BYTE YAZMAK İÇİN YAPMAK ZORUNDAYIZ. 

void floatAddByte(float data,int *count,uint8_t *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void intAddByte(int data,int *count,uint8_t *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,uint8_t *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void uint16AddByte(uint16_t data,int *count,uint8_t *array){
  for(int i=0;i<2;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void byteAddByte(byte data,int *count,uint8_t *array){
    array[*count]=data;
    (*count)++;
}
// SENSÖR VERİLERİNİ BYTE CEVİRME İŞLEMLERİNİ YAPTIĞIMIZ ALAN BUFFERA BYTE-BYTE YAZMAK İÇİN YAPMAK ZORUNDAYIZ. 
