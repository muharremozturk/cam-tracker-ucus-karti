#include <FS.h>
#include <SPIFFS.h>
#include <Arduino.h>
#include <SX126x-Arduino.h>
#include <LoRaWan-Arduino.h>
#include <EEPROM.h>
#include <SPI.h>
#include <Wire.h>

#define LORAWAN_APP_DATA_BUFF_SIZE 256  /**< Size of the data to be transmitted. */

TaskHandle_t Task1; //For Dual Core System
 
DeviceClass_t CurrentClass = CLASS_A;    
LoRaMacRegion_t CurrentRegion = LORAMAC_REGION_EU868;   
lmh_confirm CurrentConfirm = LMH_UNCONFIRMED_MSG;  


hw_config hwConfig;

// ESP32 - SX126x pin configuration
int PIN_LORA_RESET = 32;   // LORA RESET
int PIN_LORA_NSS = 15;   // LORA SPI CS
int PIN_LORA_SCLK = 14;  // LORA SPI CLK
int PIN_LORA_MISO = 12;  // LORA SPI MISO
int PIN_LORA_DIO_1 = 4; // LORA DIO_1
int PIN_LORA_BUSY = 33;  // LORA SPI BUSY
int PIN_LORA_MOSI = 13;  // LORA SPI MOSI
int RADIO_TXEN = -1;   //2 LORA ANTENNA TX ENABLE ,
int RADIO_RXEN = -1;   //25 LORA ANTENNA RX ENABLE ,


//***************************** UPDATE HERE WITH YOUR DEVICE KEYS **************************************/

uint8_t nodeDeviceEUI[8] = {0x60, 0x81, 0xF9, 0x18, 0x5A, 0x36, 0xEF, 0xD7}; // MUST BE MSB

uint8_t nodeAppEUI[8] = {0x60, 0x81, 0xF9, 0x6E, 0x4B, 0xF1, 0x18, 0xD8}; // MUST BE MSB

uint8_t nodeAppKey[16] = {0xE7, 0xE7, 0x39, 0xAC, 0x01, 0xE2, 0x2A, 0x10, 0x92, 0x3F, 0x56, 0xA4, 0x6E, 0x5D, 0x3A, 0x2E}; // MUST BE MSB



// Foward declaration
/** LoRaWAN callback when join network finished */
static void lorawan_has_joined_handler(void);
/** LoRaWAN callback when join network failed */
static void lorawan_join_fail_handler(void);
/** LoRaWAN callback when data arrived */
static void lorawan_rx_handler(lmh_app_data_t *app_data);
/** LoRaWAN callback after class change request finished */
static void lorawan_confirm_class_handler(DeviceClass_t Class);
/** LoRaWAN callback after class change request finished */
static void lorawan_unconfirm_tx_finished(void);
/** LoRaWAN callback after class change request finished */
static void lorawan_confirm_tx_finished(bool result);
/** LoRaWAN Function to send a package */
static void send_lora_frame(void);




static uint8_t m_lora_app_data_buffer[LORAWAN_APP_DATA_BUFF_SIZE];        ///< Lora user application data buffer.
static lmh_app_data_t m_lora_app_data = {m_lora_app_data_buffer, 0, 0, 0, 0}; ///< Lora user application data structure.


/**@brief Structure containing LoRaWan callback functions, needed for lmh_init()
*/


static lmh_callback_t lora_callbacks = {BoardGetBatteryLevel, BoardGetUniqueId, BoardGetRandomSeed,
                    lorawan_rx_handler, lorawan_has_joined_handler, lorawan_confirm_class_handler, lorawan_join_fail_handler,
                    lorawan_unconfirm_tx_finished, lorawan_confirm_tx_finished};



// Initialize Scheduler and timer
uint32_t err_code;  
int8_t  i=0; // LoRaWAN kodunun ikinci coreda tek sefer çalışması için kullanıyoruz.  
  

void setup()
{  
//  setCpuFrequencyMhz(20); // Save power by going slowly
  delay(5000);//do not change this
 
  // Initialize Serial for debug output
  Serial.begin(115200);

  Serial.println("=====================================");
  Serial.println("SX126x LoRaWan test");
  Serial.println("=====================================");

  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    ESP.restart();
  }
  else {
    delay(500);
    Serial.println("SPIFFS mounted successfully");
  }  
   
  // Define the HW configuration between MCU and SX126x
  hwConfig.CHIP_TYPE = SX1262_CHIP;     // Example uses an eByte E22 module with an SX1262
  hwConfig.PIN_LORA_RESET = PIN_LORA_RESET; // LORA RESET
  hwConfig.PIN_LORA_NSS = PIN_LORA_NSS;  // LORA SPI CS
  hwConfig.PIN_LORA_SCLK = PIN_LORA_SCLK;   // LORA SPI CLK
  hwConfig.PIN_LORA_MISO = PIN_LORA_MISO;   // LORA SPI MISO
  hwConfig.PIN_LORA_DIO_1 = PIN_LORA_DIO_1; // LORA DIO_1
  hwConfig.PIN_LORA_BUSY = PIN_LORA_BUSY;   // LORA SPI BUSY
  hwConfig.PIN_LORA_MOSI = PIN_LORA_MOSI;   // LORA SPI MOSI
  hwConfig.RADIO_TXEN = -1;         // LORA ANTENNA TX ENABLE
  hwConfig.RADIO_RXEN = -1;         // LORA ANTENNA RX ENABLE
  hwConfig.USE_DIO2_ANT_SWITCH = true;    // Example uses an CircuitRocks Alora RFM1262 which uses DIO2 pins as antenna control
  hwConfig.USE_DIO3_TCXO = true;        // Example uses an CircuitRocks Alora RFM1262 which uses DIO3 to control oscillator voltage
  hwConfig.USE_DIO3_ANT_SWITCH = false;  // Only Insight ISP4520 module uses DIO3 as antenna control

  // Initialize LoRa chip.
  err_code = lora_hardware_init(hwConfig);
  if (err_code != 0)
  {
    Serial.printf("lora_hardware_init failed - %d\n", err_code);
  }

  // Setup the EUIs and Keys
  lmh_setDevEui(nodeDeviceEUI);
  lmh_setAppEui(nodeAppEUI);
  lmh_setAppKey(nodeAppKey); // Only needed for OOTA registration  

 
   xTaskCreatePinnedToCore( // For Dual Core System
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    10000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */                  
    delay(100);
}

/**@brief Main loop
 */

void Task1code( void * pvParameters ){
 // Serial.print("Task1 running on core ");
 // Serial.println(xPortGetCoreID());
 
       
  for(;;){ // We can use inside for loop like void loop if you use this , code will be run core 0
    vTaskDelay(3);
   if(i==0){  
   
  // to start only once
  Serial.println("=================================================================================================================================");
  
  Serial.println("                                                ...!!LoRaWAN INITIALIZE!!...                                                     ");

  Serial.println("=================================================================================================================================");
  
/**@brief Structure containing LoRaWan parameters, needed for lmh_init()
 */
  static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_3 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; //  lora_param_init = {ADR,DATARATE , LORAWAN_PUBLIC_NETWORK,KATILMA SAYISI, TX ÇIKIŞ GÜÇÜ}

  // Initialize LoRaWan
  err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
  if (err_code != 0)
  {
    Serial.printf("lmh_init failed - %d\n", err_code);
  }

  // For some regions we might need to define the sub band the gateway is listening to
  /// \todo This is for Dragino LPS8 gateway. How about other gateways???
  if (!lmh_setSubBandChannels(1))
  {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
  }
   
  lmh_join();

  i=1;
  }  
   
}}

void loop()
{
   send_lora_frame(); /** 38 datayı gönderme yaptığımız yer */  
   delay(20000); //EU868 DR3-SF-9 38BYTE DATA GÖNDERİLİRKEN,PAKETLER ARASI BEKLENİLEN SÜRE 35SANİYEDİR. 35000
}
 

static void lorawan_join_fail_handler(void)
{  
  Serial.println("OTAA joined failed");
  Serial.println("Check LPWAN credentials and if a gateway is in range");
  // Restart Join procedure
  Serial.println("Restart network join request");
  //YENİ REGİONA GEÇİŞTE EĞER İLK BAŞLATMA KOMUTUNDAN SONRA ÇALIŞMAZSA BURADA TEKRARDAN BAĞLANMASI İÇİN FONKSİYON CAĞIRABİLİRİZ!!!
}

/**@brief LoRa function for handling HasJoined event.
 */
static void lorawan_has_joined_handler(void)
{

#if (OVER_THE_AIR_ACTIVATION != 0)
  Serial.println("Network Has Joined");  
#else
  Serial.println("OVER_THE_AIR_ACTIVATION != 0");
  

#endif

}

/**@brief Function for handling LoRaWan received data from Gateway
 *
 * @param[in] app_data  Pointer to rx data
 */
static void lorawan_rx_handler(lmh_app_data_t *app_data)
{
  Serial.printf("LoRa Packet received on port %d, size:%d, rssi:%d, snr:%d\n",
          app_data->port, app_data->buffsize, app_data->rssi, app_data->snr);

  for (int i = 0; i < app_data->buffsize; i++)
  {
    Serial.printf("%0X ", app_data->buffer[i]);
  }
  Serial.println("");

  switch (app_data->port)
  {
  case 3:
    // Port 3 switches the class
    if (app_data->buffsize == 1)
    {
      switch (app_data->buffer[0])
      {
      case 0:
        lmh_class_request(CLASS_A);
        break;

      case 1:
        lmh_class_request(CLASS_B);
        break;

      case 2:
        lmh_class_request(CLASS_C);
        break;

      default:
        break;
      }
    }
    break;

  case LORAWAN_APP_PORT:
    // YOUR_JOB: Take action on received data
    break;

  default:
    break;
  }
}

/**@brief Function to confirm LORaWan class switch.
 *
 * @param[in] Class  New device class
 */
static void lorawan_confirm_class_handler(DeviceClass_t Class)
{
  Serial.printf("switch to class %c done\n", "ABC"[Class]);

  // Informs the server that switch has occurred ASAP
  m_lora_app_data.buffsize = 0;
  m_lora_app_data.port = LORAWAN_APP_PORT;
  lmh_send(&m_lora_app_data, CurrentConfirm);
}

/**
 * @brief Called after unconfirmed packet was sent
 *
 */
static void lorawan_unconfirm_tx_finished(void)
{
  Serial.println("Uncomfirmed TX finished");
}

/**
 * @brief Called after confirmed packet was sent
 *
 * @param result Result of sending true = ACK received false = No ACK
 */
static void lorawan_confirm_tx_finished(bool result)
{
  Serial.printf("Comfirmed TX finished with result %s", result ? "ACK" : "NAK");
}

/**@brief Function for sending a LoRa package.
 */
static void send_lora_frame(void)
{
  if (lmh_join_status_get() != LMH_SET)
  {
    //Not joined, try again later
    Serial.println("We are in the Send_Lora_Frame Function");    
    Serial.println("did not join network, skip sending frame");
    return;
  }

  m_lora_app_data.port = LORAWAN_APP_PORT;
  
  m_lora_app_data.buffer[0] = 'H';
  m_lora_app_data.buffer[1] = 'e';
  m_lora_app_data.buffer[2] = 'l';
  m_lora_app_data.buffer[3] = 'l';
  m_lora_app_data.buffer[4] = 'o';
  m_lora_app_data.buffer[5] = ' ';
  m_lora_app_data.buffer[6] = 'w';
  m_lora_app_data.buffer[7] = 'o';
  m_lora_app_data.buffer[8] = 'r';
  m_lora_app_data.buffer[9] = 'l';
  m_lora_app_data.buffer[10] = 'd';
  m_lora_app_data.buffer[11] = '!'; 
  m_lora_app_data.buffsize = 12;

  Serial.print("Data: ");
  Serial.println((char *)m_lora_app_data.buffer);
  Serial.print("Size: ");
  Serial.println(m_lora_app_data.buffsize);
  Serial.print("Port: ");
  Serial.println(m_lora_app_data.port);


  lmh_error_status error = lmh_send(&m_lora_app_data, CurrentConfirm); //gönderme kısmı
  if (error == LMH_SUCCESS)
  {
  }
  Serial.printf("lmh_send result %d\n", error);
}
