#include "PCA9539.h"
#include "Wire.h"

PCA9539 ioport(0x74); // Base address starts at 0x74 for A0 = L and A1 = L

//Base address for PCA9539A devices
//Address   A1    A0
//0x74      L     L
//0x75      L     H
//0x76      H     L
//0x77      H     H
//Be sure to check I2C address with I2C Scanner by Nick Gammon if you are having communication errors
//Some vendors do not hold to this address range. (Diodes Inc, et al)
//http://www.gammon.com.au/i2c


//For pins used as input, the PCA9539A devices do not include pullups.
//You will need to add external pullups if needed.

/**
   @name setup()
   initialize the program
*/
void setup()
{
  //
  // set pins to output
  //
  Wire.begin(26,27);

  ioport.pinMode(pa6, OUTPUT);
  ioport.pinMode(pb2, OUTPUT);
  ioport.pinMode(pa0, OUTPUT);

}

/**
   @name loop()
   main loop of program and runs endlessly
*/
bool a=0;
void loop()
{ 
    ioport.digitalWrite(pa6, LOW);
    ioport.digitalWrite(pb2, LOW);
    ioport.digitalWrite(pa0, LOW);
    delay(1000);
    ioport.digitalWrite(pa0, HIGH);
    delay(1000);
}
