#include <Wire.h>
//#include <Adafruit_BMP280.h>
#include "MCP3X21.h"
#include "SparkFun_Ublox_Arduino_Library.h"  //https://github.com/sparkfun/SparkFun_Ublox_Arduino_Library
#include "PCA9539.h"
#include <RadioLib.h>
#include "esp_camera.h"
#include <WiFi.h>
#include <BMx280I2C.h>

#define BattMin 4.7f
#define BattMax 4.9f
#define TempMin 27.0f
#define TempMax 35.0f
#define PressMin 95500
#define PressMax 105500
#define gpsTime 2021
#define gpsLat 409096970 
#define gpsLong 292174036  
#define gpsAlt 40829 

#define PWDN_GPIO_NUM     -1
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

SPIClass SPI2(HSPI);
#define I2C_ADDRESS 0x77
//create a BMx280I2C object using the I2C interface with I2C Address 0x76
BMx280I2C bmx280(I2C_ADDRESS);
const uint8_t address = 0x4D;
const uint16_t ref_voltage = 3300;  // in mV
MCP3021 mcp3021(address);
PCA9539 ioport(0x74);  // Base address starts at 0x74 for A0 = L and A1 = L
SFE_UBLOX_GPS myGPS;
SX1262 radio = new Module(15, 4, 32, 33, SPI2);

const char* ssid = "VODAFONE_70CD";
const char* password = "ulaseren9519";

void startCameraServer();

void setup() {
  Serial.begin(115200);
  Wire.begin(26, 27);
  while (!Serial)
    ;
  Serial.println("Serial    :[OK]");
  Serial.println("[OK]");
  fullTest();
}

void loop() {
  // put your main code here, to run repeatedly:
}

float readBatt() {
  float R1 = 56;  // 560K
  float R2 = 10;  // 100K
  float value = 0.0f;
  do {
    value = mcp3021.read();
    value += mcp3021.read();
    value += mcp3021.read();
    value = value / 3.0f;
    value = (value * 3.32) / 1024.0f;
    value = value / (R2 / (R1 + R2));
  } while (value > 10.0);
  return value;
}
void wireTest() {
  //--------- Wire ----------------------------------------
  Serial.print("Wire Start   :");
  Serial.println("[OK]");
  ioport.pinMode(pa6, OUTPUT);
  ioport.digitalWrite(pa6, HIGH);
  delay(100);
  byte error, address;
  int nDevices = 0;
  byte bmpS = 0;
  byte mcp = 0;
  byte pca = 0;
  byte ubloxG = 0;
  for (address = 1; address < 127; address++) {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0) {
      if (address == 119) {  //0x77
        Serial.println("BMP280 0x77    :[OK]");
        bmpS = 1;
      } else if (address == 77) {  //0x19
        Serial.println("MCP3021 0x4D (ADC)  :[OK]");
        mcp = 1;
      } else if (address == 116) {  //0x74
        Serial.println("PCA9539 0x74 (Port expender) :[OK]");
        pca = 1;
      } else if (address == 66) {  //0x42
        Serial.println("UBLOX MAX-M8Q 0x42  :[OK]");
        Serial.println("UBLOX Power Mosfet Hatasi. Bu Asamada Ublox a Erisilmemeli!!");
        while (1)
          ;
      } else {
        Serial.print("Bilinmeyen I2C aygit bulundu. Address 0x");
        if (address < 16)
          Serial.print("0");
        Serial.print(address, HEX);
        Serial.println("  !!!");
      }
      nDevices++;
    } else if (error == 4) {
      Serial.print("Bilinmeyen Hata. Address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
    }
  }
  if (nDevices == 3 && bmpS == 1 && mcp == 1 && pca == 1) {
    Serial.println("Bulunan adres 3 :[OK]");
  } else {
    Serial.print("Bulunan adres :");
    Serial.println(nDevices);
    if (bmpS == 0) Serial.println("BMP180 0x77    :[ERROR!]");
    if (mcp == 0) Serial.println("MCP3021 0x4D (ADC)  :[ERROR!]");
    if (pca == 0) Serial.println("PCA9539 0x74 (Port expender) :[ERROR!]");
    Serial.println("I2C aygitlarini ve PULLUP direnclerini kontrol edin!!!");
    while (1)
      ;
  }

  //-----------------GPS Power ON----------------------
  Serial.print("GPS POWER ON   :");
  ioport.digitalWrite(pa6, LOW);
  Serial.println("[OK]");
  delay(100);
  error = 0;
  nDevices = 0;
  bmpS = 0;
  mcp = 0;
  pca = 0;
  ubloxG = 0;
  for (address = 1; address < 127; address++) {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0) {
      if (address == 119) {  //0x77
        Serial.println("BMP280 0x77    :[OK]");
        bmpS = 1;
      } else if (address == 77) {  //0x19
        Serial.println("MCP3021 0x4D (ADC)  :[OK]");
        mcp = 1;
      } else if (address == 116) {  //0x74
        Serial.println("PCA9539 0x74 (Port expender) :[OK]");
        pca = 1;
      } else if (address == 66) {  //0x42
        Serial.println("UBLOX MAX-M8Q 0x42  :[OK]");

        ubloxG = 1;
      } else {
        Serial.print("Bilinmeyen I2C aygit bulundu. Address 0x");
        if (address < 16)
          Serial.print("0");
        Serial.print(address, HEX);
        Serial.println("  !!!");
      }
      nDevices++;
    } else if (error == 4) {
      Serial.print("Bilinmeyen Hata. Address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
    }
  }
  //if (nDevices == 4 && bmpS == 1 && mcp == 1 && pca == 1 && ubloxG == 1){
  if (nDevices == 3 && bmpS == 1 && mcp == 1 && pca == 1) {
    Serial.println("Bulunan adres 4 :[OK]");
  } else {
    if (bmpS == 0) Serial.println("BMP180 0x77    :[ERROR!]");
    if (mcp == 0) Serial.println("MCP3021 0x4D (ADC)  :[ERROR!]");
    if (pca == 0) Serial.println("PCA9539 0x74 (Port expender) :[ERROR!]");
    if (ubloxG == 0) Serial.println("UBLOX MAX-M8Q 0x42  :[ERROR!]");
    Serial.println("I2C aygitlarini ve PULLUP direnclerini kontrol edin!!!");

    while (1)
      ;
  }
  Serial.print("GPS POWER OFF   :");
  ioport.digitalWrite(pa6, HIGH);
  delay(100);
  Serial.println("[OK]");
  //--------- BattRead ---------------------------------
  mcp3021.init(&Wire);
  Serial.print("BattRead   :");

  float batV = readBatt();
  Serial.println("[OK]");
  Serial.print("Batarya Voltaji  (");
  Serial.print(batV);
  Serial.print(" V):");
  if (batV < BattMin or BattMax > 4.9f) {
    Serial.println("Batarya Voltaji Uygun Aralikta Degil!!!");

    while (1)
      ;
  } else
    Serial.println("[OK]");
  //-----------BMP280-------------------------------------------------
  Serial.print("BMP280 Start :");

  if (!bmx280.begin())
	{
		Serial.println("begin() failed. check your BMx280 Interface and I2C Address.");
		while (1);
	}
  Serial.println("[OK]");
  bmx280.writeOversamplingPressure(BMx280MI::OSRS_P_x16);
	bmx280.writeOversamplingTemperature(BMx280MI::OSRS_T_x16);
  if (!bmx280.measure())
	{
		Serial.println("could not start measurement, is a measurement already running?");
		return;
	}
  do
	{
		delay(100);
	} while (!bmx280.hasValue());
float tmpC = bmx280.getTemperature();
Serial.print("Temperature = ");
Serial.print(tmpC);
Serial.print(" *C :");
if (tmpC < TempMin or tmpC > TempMax) {
  Serial.println("Sicaklik Uygun Aralikta Degil!!!");

  while (1)
    ;
} else
  Serial.println("[OK]");

uint32_t tmpP = bmx280.getPressure();
Serial.print("Pressure = ");
Serial.print(tmpP);
Serial.print(" Pa :");
if (tmpP < PressMin or tmpP > PressMax) {
  Serial.println("Basinc Uygun Aralikta Degil!!!");

  while (1)
    ;
} else
  Serial.println("[OK]");

//-------- GPS TEST -----------------------------------
Serial.print("GPS POWER ON   :");
ioport.digitalWrite(pa6, LOW);
Serial.println("[OK]");
delay(100);
Wire.begin(27, 26);
Serial.print("Set Wire Speed 400Khz :");

Wire.setClock(400000);
Serial.println("[OK]");
Serial.print("GPS Wire Begin :");

delay(100);
if (myGPS.begin() == false)  //Connect to the Ublox module using Wire port
{
  Serial.println(F("GPS I2C Baglantisi Kurulamadı!!!"));
  while (1)
    ;
}
Serial.println(F("[OK]"));
Serial.print(F("GPS Config :"));
// do not overload the buffer system from the GPS, disable UART output
myGPS.setUART1Output(0);           //Disable the UART1 port output
myGPS.setUART2Output(0);           //Disable Set the UART2 port output
myGPS.setI2COutput(COM_TYPE_UBX);  //Set the I2C port to output UBX only (turn off NMEA noise)
myGPS.saveConfiguration();         //Save the current settings to flash and BBR
Serial.println("[OK]");
Serial.print(F("GPS Saati Bekleniyor :"));
// while (1) {
//   Serial.print(F("."));
//   if (myGPS.getPVT()) {
//     if (myGPS.getYear() >= gpsTime) {
//       Serial.println(F("[OK]"));
//       break;
//     }
//   }
// }
// Serial.print(F("GPS Konumu Bekleniyor :"));
// while (1) {
//   Serial.print(F("."));
//   if (myGPS.getPVT()) {
//     if (myGPS.getLatitude() > gpsLat - 10000 && myGPS.getLatitude() < gpsLat + 10000 && myGPS.getLongitude() > gpsLong - 10000 && myGPS.getLongitude() < gpsLong + 10000) {
//       Serial.println(F("[OK]"));
//       break;
//     }
//   }
// }
ioport.digitalWrite(pa6, HIGH);
Wire.begin(26, 27);
delay(100);
}

int transmissionState = ERR_NONE;
volatile bool transmittedFlag = false;
volatile bool enableInterrupt = true;

void setFlag(void) {
  // check if the interrupt is enabled
  if (!enableInterrupt) {
    return;
  }
  transmittedFlag = true;
}



void configLora() {
  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;
  const int HSPI_CS = 15;

  SPI2.begin(HSPI_SCK, HSPI_MISO, HSPI_MOSI, HSPI_CS);
  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }
}



void sendDataLora() {
  Serial.println(F("LORA TEST "));
  

  int state = radio.transmit("Hello World!");

  if (state == ERR_NONE) {
    // the packet was successfully transmitted
    Serial.println(F("....[0K]"));

  } else if (state == ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 256 bytes
    Serial.println(F("too long!"));

  } else if (state == ERR_TX_TIMEOUT) {
    // timeout occured while transmitting packet
    Serial.println(F("timeout!"));

  } else {
    // some other error occurred
    Serial.print(F("failed, code "));
    Serial.println(state);

  }

}

void loraTest(){
  configLora();
  sendDataLora();
}

void cameraTest(){

  ioport.pinMode(pb2, OUTPUT);
  ioport.digitalWrite(pb2, HIGH);
  delay(100);
  Serial.println("Kamera test ");
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
  config.frame_size = FRAMESIZE_UXGA;
  config.jpeg_quality = 10;
  config.fb_count = 2;
  
  // camera init
  esp_err_t err = esp_camera_init(&config);
  if (err == ESP_OK) {
    while (true);
  }
  
  ioport.digitalWrite(pb2, LOW);
  delay(100);
  err = esp_camera_init(&config);
  if (err == ESP_OK) {
    Serial.printf("...... [OK]");
  }

  sensor_t * s = esp_camera_sensor_get();
  
  // drop down frame size for higher initial frame rate
  s->set_framesize(s, FRAMESIZE_QVGA);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  startCameraServer();

  Serial.print("Camera Ready! Use 'http://");
  Serial.print(WiFi.localIP());
  Serial.println("' to connect");
}
void startCameraServer();

void fullTest() {
  wireTest();
  loraTest();
  cameraTest();

  Serial.println(F("  _______        _      ____  _  __"));
  Serial.println(F(" |__   __|      | |    / __ \\| |/ /"));
  Serial.println(F("    | | ___  ___| |_  | |  | | ' / "));
  Serial.println(F("    | |/ _ \\/ __| __| | |  | |  <  "));
  Serial.println(F("    | |  __/\\__ \\ |_  | |__| | . \\ "));
  Serial.println(F("    |_|\\___||___/\\__|  \\____/|_|\\_\\"));
  /*                                  
 */

  //--------SON-------------------
}
