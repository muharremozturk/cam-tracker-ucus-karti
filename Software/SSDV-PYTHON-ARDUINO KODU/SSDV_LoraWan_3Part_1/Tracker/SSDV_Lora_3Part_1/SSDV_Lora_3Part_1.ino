#include <Arduino.h>
#include <esp_camera.h>
#include "ssdv.h"
#include <RadioLib.h>
#include <SPI.h>
#include "PCA9539.h"
#include "Wire.h"
#include <FS.h>
#include <SPIFFS.h>
#include <stdlib.h>
#include <EEPROM.h>
#include "soc/rtc_cntl_reg.h"

// define the number of bytes you want to access
#define EEPROM_SIZE 8

SPIClass SPI2(HSPI);


int capture_interval = 30000; // microseconds between captures
long current_millis;
long last_capture_millis = 0;
static esp_err_t cam_err;
uint16_t res_len,lenn,indexx;
int8_t indexson;
int power,t,n;
int bufSize = 0;

//int counter = 0;

 
// CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     -1
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

// ssdv definitions
#define IMG_BUFF_SIZE               128         // size of the buffer feeding SSDV process
#define LORA_BUFFER                 256
#define GPS_BUFFER                  256
#define FILE_PHOTO "/photo5.jpg"
uint8_t ssdvQuality = 4;                        // 0-7 corresponding to JPEG quality: 13, 18, 29, 43, 50, 71, 86 and 100


static const char callsign[] = "TA2NHP";        // maximum of 6 characters
ssdv_t ssdv;

uint8_t imgBuff[IMG_BUFF_SIZE];
uint8_t loraBuff[LORA_BUFFER];

char tempStr[40];
int8_t imageID = 0;

const int HSPI_MISO = 12;
const int HSPI_MOSI = 13;
const int HSPI_SCK = 14;  
const int HSPI_CS = 15;

SX1262 radio = new Module(15, 4, 32, 33, SPI2);

PCA9539 ioport(0x74);
void save_photo();
void setup() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector
  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    ESP.restart();
  }
  else {
    delay(500);
    Serial.println("SPIFFS mounted successfully");
  }
 
  SPI2.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS);
  Wire.begin(26,27);
  ioport.pinMode(pb2, OUTPUT);
  ioport.digitalWrite(pb2, LOW); // Kamera Aktif.

  Serial.begin(115200);
  delay(1000);
  Serial.println("Start..");

  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin();
  //state=radio.setFrequency(868);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed(3), code "));
    Serial.println(state);
    while (true);
  }
 
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
// 0-8 corresponfing to 320x240, 352x288, 640x480, 800x480, 1024x768, 1280x960, 1600x1200, 2048x1536, 2592x1944
//  FRAMESIZE_QVGA 320x240, FRAMESIZE_VGA 640x480, FRAMESIZE_SVGA 800x600
//  FRAMESIZE_XGA 1024x768, FRAMESIZE_UXGA 1600x1200, FRAMESIZE_QXGA 2048x1536
  config.frame_size = FRAMESIZE_VGA;
  config.jpeg_quality = 10;
  config.fb_count = 1;

  // camera init
  cam_err = esp_camera_init(&config);
  if (cam_err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", cam_err);
    return;
  }else Serial.println("Camera..OK");


  delay(2000);//kamera beyaz dengesi vb. ayarları yapmak için zamana ihtiyaç duyuyor
 
  EEPROM.begin(EEPROM_SIZE);
  save_photo();
 
}

void loop()
{
  /*
  current_millis = millis();
  if (current_millis - last_capture_millis > capture_interval) { // Take another picture
   
    save_photo();
    last_capture_millis = millis();
    //imageID++;
  }
  */
}

/*
uint16_t writeeprom(int8_t a,int8_t b,uint16_t dataa,uint16_t result ){

   byte resdusukbyte = lowByte(dataa);
   byte resyuksekbyte = highByte(dataa);
   EEPROM.write(a,resdusukbyte);
   EEPROM.write(b,resyuksekbyte);
   byte d = EEPROM.read(a);
   byte y = EEPROM.read(b);
   return result = d + (y<<8);
}  
*/

int iread(uint8_t* buffer,int numBytes,uint16_t cbIndex ){

File file2 = SPIFFS.open(FILE_PHOTO, "r");
   
 // have we reached past end of imagebuffer
  if((cbIndex + numBytes ) < lenn ){    
    bufSize = numBytes;  
  }
  else{  
    bufSize = lenn - cbIndex; // fb->len
    indexson=5; // işlemlerini sonlandırmak için    
}

// clear the dest buffer
memset(buffer,0,numBytes);

if(cbIndex>0) cbIndex=cbIndex-1;
file2.seek(cbIndex,SeekSet); // index okuması için flash hafızadaki resim datasının bufferındaki cursoru ayarlıyoruz.
//Serial.print(file2.position());

file2.read(buffer,bufSize);

byte indexdusukbyte = lowByte(cbIndex);
byte indexyuksekbyte = highByte(cbIndex);
EEPROM.write(5,indexdusukbyte);
EEPROM.write(6,indexyuksekbyte);
EEPROM.write(7,166);
EEPROM.commit();


if(indexson==5){

indexson=2;
file2.close();    
}
  return bufSize;
}

char Hex(uint8_t index)
  {
    char HexTable[] = "0123456789ABCDEF";
   
    return HexTable[index];
  }


int process_ssdv(){

  uint16_t index=0,c = 0,indexdvm=0;
  int8_t ssdvPacketCount=0,ssdvPacketCount_2=0;
 
if(power==2){  

   byte x = EEPROM.read(5);
   byte y = EEPROM.read(6);
   byte h = EEPROM.read(0);
   byte p = EEPROM.read(1);
   
   ssdvPacketCount_2=EEPROM.read(3);
   lenn = h + (p<<8);
   indexdvm = x + (y<<8);  
   indexdvm=indexdvm-127;

   Serial.print("Güç sistemine ikinci giriş başarılı şu an bu indexteyiz:");
   Serial.println(indexdvm);

  // initialise ssdv config structure
  ssdv_enc_init(&ssdv, SSDV_TYPE_NOFEC, (char *)callsign, imageID++, ssdvQuality);
  // set the output lora packet buffer for ssdv where the final ssdv packet will end up
  ssdv_enc_set_buffer(&ssdv, loraBuff);
  Serial.print("Sending Image: length = ");
  Serial.println(lenn);


   while(1){
 
    while((c = ssdv_enc_get_packet(&ssdv)) == SSDV_FEED_ME)
    {  
        if(index==640){
          index=indexdvm;
         ssdvPacketCount=ssdvPacketCount_2;
        }
        index += iread(imgBuff, IMG_BUFF_SIZE,index);
        Serial.print("index = ");
        Serial.print(index);    
        Serial.print("ssdvPacketCount = ");
        Serial.print(ssdvPacketCount);
        ssdv_enc_feed(&ssdv, imgBuff, IMG_BUFF_SIZE);                

/*        for(int i=0; i<128; i++)  Serial.print(imgBuff[i],HEX);
        Serial.println();  */  
   }
     
    if(c == SSDV_EOI)
    {
        for(int i =0; i<EEPROM_SIZE; i++)EEPROM.write(i,0);
        EEPROM.commit();
        Serial.println("ssdv EOI");
        break;
    }
    else if(c != SSDV_OK)
    {
        Serial.println("ssdv Error");
        break;
    }

    // move lora data backwrds 1 byte This seems needed for pits gateway to read it. (TT7)
    for(uint16_t i = 0; i < 256; i++) {
      loraBuff[i] = loraBuff[i+1];
    }
   
    // lora transmit
    Serial.print(" packet sent");
    Serial.println(ssdvPacketCount);
    uint8_t partBuff[90];

  for(uint8_t a=0; a<3;a++){
    memset(partBuff, 0, 90);//buffer temizle

    partBuff[0]= 0x55;
    partBuff[1]= imageID;
    partBuff[2]= ssdvPacketCount;
    partBuff[3]=a;
   
   for(int z=0;z<85;z++)partBuff[(4+z)]=loraBuff[z+(a*85)];
   
   int state = radio.transmit(partBuff,89);//LORA_BUFFER);
   if(index>=640){
   EEPROM.write(2,imageID);
   EEPROM.write(3,ssdvPacketCount);
   EEPROM.write(4,a);
   EEPROM.commit();    
   }
   if (state == ERR_NONE){
      // the packet was successfully transmitted
     
      Serial.println(F("success!"));

      // print measured data rate
      Serial.print(F("[SX1262] Datarate:\t"));
      Serial.print(radio.getDataRate());
      Serial.println(F(" bps"));

    } else if (state == ERR_PACKET_TOO_LONG) {
      // the supplied packet was longer than 256 bytes
      Serial.println(F("too long!"));

    } else if (state == ERR_TX_TIMEOUT) {
      // timeout occured while transmitting packet
      Serial.println(F("timeout!"));

    } else {
      // some other error occurred
      Serial.print(F("failed(2), code "));
      Serial.println(state);

    }
  }  
    ssdvPacketCount++;
    delay(10);  
  }
  }

else{
  Serial.print("İlk fotografımız index :");
  Serial.println(index);

  // initialise ssdv config structure
  ssdv_enc_init(&ssdv, SSDV_TYPE_NOFEC, (char *)callsign, imageID++, ssdvQuality);
  // set the output lora packet buffer for ssdv where the final ssdv packet will end up
  ssdv_enc_set_buffer(&ssdv, loraBuff);
  Serial.print("Sending Image: length = ");
  Serial.println(lenn);

  while(1){
 
    while((c = ssdv_enc_get_packet(&ssdv)) == SSDV_FEED_ME)
    {          
        index += iread(imgBuff, IMG_BUFF_SIZE,index);
        Serial.print("index = ");
        Serial.print(index);
        ssdv_enc_feed(&ssdv, imgBuff, IMG_BUFF_SIZE);              
    }

    if(c == SSDV_EOI)
    {
       for(int i=0; i<EEPROM_SIZE; i++)EEPROM.write(i,0);
       EEPROM.commit();
       Serial.println("ssdv EOI");
       break;
    }
    else if(c != SSDV_OK)
    {
        Serial.println("ssdv Error");
        break;
    }

    // move lora data backwrds 1 byte This seems needed for pits gateway to read it. (TT7)
    for(uint16_t i = 0; i < 256; i++) {
      loraBuff[i] = loraBuff[i+1];
    }

   
    // lora transmit
    Serial.print(" packet sent");
    Serial.println(ssdvPacketCount);
    uint8_t partBuff[90];

  for(uint8_t a=0; a<3;a++){
    memset(partBuff, 0, 90);//buffer temizle

    partBuff[0]= 0x55;
    partBuff[1]= imageID;
    partBuff[2]= ssdvPacketCount;
    partBuff[3]=a;
   
   for(int z=0;z<85;z++)partBuff[(4+z)]=loraBuff[z+(a*85)];

   
   int state = radio.transmit(partBuff,89);//LORA_BUFFER);

   EEPROM.write(2,imageID);
   EEPROM.write(3,ssdvPacketCount);
   EEPROM.write(4,a);
   EEPROM.commit();    
    if (state == ERR_NONE) {
      // the packet was successfully transmitted
     
      Serial.println(F("success!"));

      // print measured data rate
      Serial.print(F("[SX1262] Datarate:\t"));
      Serial.print(radio.getDataRate());
      Serial.println(F(" bps"));

    } else if (state == ERR_PACKET_TOO_LONG) {
      // the supplied packet was longer than 256 bytes
      Serial.println(F("too long!"));

    } else if (state == ERR_TX_TIMEOUT) {
      // timeout occured while transmitting packet
      Serial.println(F("timeout!"));

    } else {
      // some other error occurred
      Serial.print(F("failed(2), code "));
      Serial.println(state);

    }
  }  
    ssdvPacketCount++;
    delay(10);  

  }

}}

void save_photo()
{
Serial.print("CHECK : ");
Serial.println( EEPROM.read(7));

if(EEPROM.read(7)==166){
   power=2;
   process_ssdv();  
}

else{
 
  camera_fb_t *fb = NULL;  
  Serial.print("Taking picture: ");
  fb = esp_camera_fb_get();
 
  if (!fb) {
      Serial.println("Camera capture failed");
      return;
    }
   
       // Photo file name
    Serial.printf("Picture file name: %s\n", FILE_PHOTO);
    File file = SPIFFS.open(FILE_PHOTO, FILE_WRITE);
    delay(500);
    // Insert the data in the photo file
   
    if (!file) {
      Serial.println("Failed to open file in writing mode");
    }
    else {
      file.write(fb->buf, fb->len); // payload (image), payload length      
      res_len= (uint16_t)fb->len;
      Serial.print("The picture has been saved in ");
      Serial.print(FILE_PHOTO);
      Serial.print(" - Size: ");
      Serial.print(file.size());
      Serial.println(" bytes");    }

   byte resdusukbyte = lowByte(res_len);
   byte resyuksekbyte = highByte(res_len);
   EEPROM.write(0,resdusukbyte);
   EEPROM.write(1,resyuksekbyte);
   EEPROM.commit();
   byte d = EEPROM.read(0);
   byte y = EEPROM.read(1);
   lenn = d + (y<<8);
   
   delay(500);
   file.close();  
 
    process_ssdv();  
    esp_camera_fb_return(fb);  }
}
