from Tkinter import *
from PIL import ImageTk, Image
import serial
import sys
import ttk
import time
import datetime
import tkFont
import os
import threading
import subprocess
import Queue


def serial_ports():
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


class Application(Frame):

    def __init__(self, master, serialPort):
        """ Initialize the Frame """
        Frame.__init__(self, master)

        # serial port
        self.serialPort = serialPort
        self.activePort = ''

        # ssdv
        self.imgList = []   # lists the received images e.g. ['2018-02-09_17-27-19_TT7L_0001', '2018-02-09_17-27-51_TT7L_0002']
        self.pktList = []   # lists the received number of packets for each image e.g. [4, 3]
        self.typeList = []  # lists the image types (FEC, NO-FEC) of each received image e.g. ['g', 'g']

        abs_dir = os.path.dirname(__file__)
        abs_path = os.path.join(abs_dir, "raw")
        
        if not os.path.exists(abs_path):
            os.makedirs(abs_path)

        # serial port parser
        self.lastPktType = 'p'
        self.lastPkt = ''
        self.lastPktTime = datetime.datetime(1,1,1)    

        # ssdv window
        self.ssdv_window = Toplevel(self)
        self.ssdv_window.protocol('WM_DELETE_WINDOW', self.remove_image_window)
        self.ssdv_window.title("SSDV Image")
        self.ssdv_window.geometry("320x240")
        self.ssdv_window.withdraw()
        self.ssdv_canvas = Canvas(self.ssdv_window, width=320, height=240)

        self.grid()
        self.create_widgets()

    def create_widgets(self):
        """ Create several widgets """
        self.port_label = Label(self, text="Port:")
        self.port_label.grid(row=0, column=0, sticky=W)
        
        self.port_box_value = StringVar()
        self.port_box = ttk.Combobox(self, textvariable=self.port_box_value, width=7)
        self.port_box['values'] = (serial_ports())
        self.port_box.current(0)
        self.port_box.bind("<<ComboboxSelected>>", self.update_buttons)
        self.port_box.grid(row=0, column=1, sticky=W+E, padx=15)

        self.port_button = Button(self, text="Refresh", command=self.refresh_ports)
        self.port_button.grid(row=0, column=3, sticky=W+E)

        self.port_button2 = Button(self, text="Connect", command=self.connect_port)
        self.port_button2.grid(row=0, column=2, sticky=W+E)

        self.upload_buttonVar = IntVar()
        self.upload_button = Checkbutton(self, state=ACTIVE, text="Upload Data to Habitat", variable=self.upload_buttonVar, command=self.update_listener)
        self.upload_button.grid(row=4, column=0, sticky=W, columnspan=2, padx=20)

        self.save_buttonVar = IntVar(value=1)
        self.save_button = Checkbutton(self, state=ACTIVE, text="Save Data to File", variable=self.save_buttonVar)
        self.save_button.grid(row=5, column=0, sticky=W, columnspan=2, padx=20)

        self.img_buttonVar = IntVar()
        self.img_button = Checkbutton(self, state=ACTIVE, text="Show Latest Image", variable=self.img_buttonVar, command=self.show_image)
        self.img_button.grid(row=6, column=0, sticky=W, columnspan=2, padx=20)

        self.onoff_button = Button(self, text="ON/OFF", command=self.change_onoff, state=DISABLED)
        self.onoff_button.grid(row=5, column=2, sticky=W+E, columnspan=2)

        self.onoff_label = Label(self, text="RX OFF", state=DISABLED, font='Tahoma 8 bold')
        self.onoff_label.grid(row=6, column=2, sticky=S, columnspan=2)

        self.pktNum_label = Label(self, text="Packets:")
        self.pktNum_label.grid(row=5, column=14, sticky=W)
        self.pktNum_label_val = Label(self, text="0")
        self.pktNum_label_val.grid(row=5, column=15, sticky=E)

        self.pktAge_label = Label(self, text="Last Packet:")
        self.pktAge_label.grid(row=6, column=14, sticky=W)
        self.pktAge_label_val = Label(self, text="-")
        self.pktAge_label_val.grid(row=6, column=15, sticky=E)

        self.packets_text = Text(self, wrap=WORD, height=30, width=140)
        self.packets_text.grid(row=7, column=0, columnspan=17, sticky=W+E)

        self.status_label = Label(self, text="Status:")
        self.status_label.grid(row=8, column=0, sticky=NS)
        self.status_label_val = Label(self, text="")
        self.status_label_val.grid(row=8, column=1, sticky=W, columnspan=13)

    def refresh_ports(self):
        """ Refreshes the COM port list """
        self.port_box['values'] = (serial_ports())

    def connect_port(self):
        """ Connect to selected port """
        self.serialPort.port = self.port_box.get()
        self.serialPort.open()

        # added to support CP2102 with DTR instead of RTS
        self.serialPort.setDTR(True)
        time.sleep(0.05)
        self.serialPort.setDTR(False)
        time.sleep(0.05)
        self.serialPort.setDTR(True)
        
        self.activePort = self.port_box.get()
        self.update_buttons()
        self.status("Connected to port " + self.port_box.get() + ".")
        
        self.onoff_button.config(state=ACTIVE)
        self.onoff_label.config(state=NORMAL)
        self.onoff_label.config(text="RX ON")
        self.decode_ssdv()
        self.read_port()

    def disconnect_port(self):
        """ Closes the serial port connection """
        self.serialPort.close()
        self.activePort = 0
        self.update_buttons()

        self.onoff_button.config(state=DISABLED)
        self.onoff_label.config(text="RX OFF")
        self.onoff_label.config(state=DISABLED)
        self.status("Port disconnected.")

    def update_buttons(self, event=None):
        """ Update button based on Combobox selection """
        if self.port_box.get() == self.activePort:
            self.port_button2.config(text="Disconnect", command=self.disconnect_port, state=ACTIVE)
            self.port_button.config(state=DISABLED)
        else:
            if self.serialPort.isOpen():
                self.port_button2.config(text="Connect", command=self.connect_port, state=DISABLED)
                self.port_button.config(state=DISABLED)
                
            else:
                self.port_button2.config(text="Connect", command=self.connect_port, state=ACTIVE)
                self.port_button.config(state=ACTIVE)


    def remove_image_window(self):
        """ manages ssdv window closure """
        self.img_buttonVar.set(value=0)
        self.ssdv_window.withdraw()

    def status(self, status):
        """ Update the status bar """
        timestamp = "{:%H:%M:%S}".format(datetime.datetime.now())
        self.status_label_val.config(text=timestamp + "   " + status)

    def change_onoff(self):
        """ Turn RXing ON or OFF """
        #self.serialPort.write('o')

        #if self.RXonoff == '0':
        #    self.serialPort.write('1')
        #elif self.RXonoff == '1':
        #    self.serialPort.write('0')
        
        self.read_port()

    def decode_ssdv(self):
        """ in intervals decodes new ssdv packets """
        newPackets = 0
        
        for i in range(len(self.pktList)):
            
            if self.pktList[i] > 0:
                args = ''
                newPackets += self.pktList[i]
                self.pktList[i] = 0
                
                if self.typeList[i] == 'f':
                    args = "SSDV.exe -d \""
                else:
                    args = "SSDV.exe -d -n \""
                
                args += ("raw\\" + self.imgList[i] + "\" \"jpg\\" + self.imgList[i] + ".jpg\"")
                subprocess.Popen(args, shell=True)
                
                self.status("New SSDV packets decoded.")
        
        self.show_image()
        
        if self.serialPort.isOpen():
            # decide the frequency of decoding based on the rate at which new packets arrive
            if newPackets >= 1:
                self.after(3000, self.decode_ssdv) # milliseconds
            else:
                self.after(6000, self.decode_ssdv) # milliseconds
 
    def show_image(self):
        """ opens the latest received image in a new window """
        if self.img_buttonVar.get() == 1:
            if self.ssdv_window.state() == "withdrawn":
                self.ssdv_window.deiconify()

            try:
                img1 = ImageTk.PhotoImage(Image.open("jpg\\"+self.imgList[-1] + ".jpg"))
                self.ssdv_window.title(self.imgList[-1] + ".jpg")
                self.ssdv_window.geometry(str(img1.width()) + "x" + str(img1.height()))

                self.ssdv_canvas.config(width=img1.width(), height=img1.height())
                self.ssdv_canvas.create_image(img1.width() / 2, img1.height() / 2, image=img1)
                self.ssdv_canvas.img1 = img1
                self.ssdv_canvas.grid()
            except:
                pass
            
        else:
            if self.ssdv_window.state() == "normal":
                self.ssdv_window.withdraw()
 
    def read_port(self):
        """ Periodically check for data """
        if self.serialPort.isOpen():
            
            while (self.serialPort.inWaiting() > 0):
                line = self.serialPort.readline()
                
                if len(line) < 3:
                        break
                
                # SSDV packet may contain '\n' as part of the data
                if  line[0] == 'j' and line[1] == ':':

                    if len(line[2:]) == 257:
                           packet = line[2:-1]
                    else:
                           packet = line[2:]
                    
                    for i in range(256 - len(packet)):
                        b = self.serialPort.read()
                        packet = packet + b

                    # packet info
                    pktInfo = ''
                    timestamp = str(datetime.datetime.now())
                    pktType = packet[1]
                    callsign = ''
                    imgID = ord(packet[6])
                    pktID = (ord(packet[7]) << 8) | ord(packet[8])
                    width = ord(packet[9]) * 16
                    height = ord(packet[10]) * 16
                    flags = ord(packet[11])
                    mcuOffset = ord(packet[12])
                    mcuIndex = (ord(packet[13]) << 8) | ord(packet[14])
                    checksum = 0
                    FEC = []

                    if len(timestamp) == 19:
                        timestamp += ".00"
                    
                    if pktType == 'f': # FEC packet type
                        checksum = (ord(packet[220]) << 24) | (ord(packet[221]) << 16) | (ord(packet[222]) << 8) | ord(packet[223])
                        FEC = packet[224:]
                    elif pktType == 'g': # NO-FEC packet type
                        checksum = (ord(packet[252]) << 24) | (ord(packet[253]) << 16) | (ord(packet[254]) << 8) | ord(packet[255])

                    #decode Callsign
                    c = (ord(packet[2]) << 24) | (ord(packet[3]) << 16) | (ord(packet[4]) << 8) | ord(packet[5])

                    while c > 0:
                        d = c % 40
                        
                        if d == 0:
                            callsign = callsign + '-'
                        elif d < 11:
                            callsign = callsign + chr(48 + d - 1)
                        elif d < 14:
                            callsign = callsign + '-'
                        else:
                            callsign = callsign + chr(65 + d - 14)
                        
                        c = c / 40
                                        
                    pktInfo = timestamp[0:22] + ' ' + "SSDV from: " + callsign + ' ' + "ImageID: " + str(imgID) + ' ' + "PacketID: " + str(pktID) + ' ' + \
                              str(width) + 'x' + str(height) + ' ' + "Quality: " + str((((flags >> 3) & 7) ^ 4)) + ' '

                    if pktType == 'f':
                        pktInfo += "Normal"
                    elif pktType == 'g':
                        pktInfo += "No-FEC"

                    if ((flags >> 2) & 1) == 1:
                        pktInfo += " END OF IMAGE"
                    else:
                        pktInfo += ""

                    self.lastPkt = pktInfo
                    self.lastPktType = 'j'
                    self.status("SSDV packet received.")

                    self.update_ssdv_queue(packet)

                    # check if the file already exists
                    matches = []
                    matchesN = []
                    # example: 2017-08-20_16-29-18_TT7L_0001
                    ssdvFile = timestamp[0:10] + '_' + timestamp[11:13] + '-' + timestamp[14:16] + '-' + timestamp[17:19] + '_' + callsign + '_' + str(imgID).zfill(4)
                    
                    # save data to file
                    if self.save_buttonVar.get() == 1:

                        # save raw SSDV packets
                        abs_dir = os.path.dirname(__file__)
                        rel_dir = "raw/" + ssdvFile
                        abs_path = os.path.join(abs_dir, rel_dir)

                        # is it the first received image/packet?
                        if not self.imgList:
                            self.imgList.append(ssdvFile)
                            self.pktList.append(1)
                            self.typeList.append(pktType)

                            # store raw packet
                            f = open(abs_path, 'wb')
                            f.write(packet)
                            f.close()
                            break

                        # does file with this callsign and image ID already exist?
                        # packets may be coming in from different transmitters, need to store the packet in the right file
                        for i in range(len(self.imgList)):
                            
                            if callsign and str(imgID).zfill(4) in self.imgList[i]:
                                matches.append(self.imgList[i])
                                matchesN.append(i) # appends the sequence number within the list of the matched file

                        # if not create the file
                        if not matches:
                            self.imgList.append(ssdvFile)
                            self.pktList.append(1)
                            self.typeList.append(pktType)
                        # else store the packet in the existing file
                        # imgList holds its contents only while the program is running, restarting the application creates new files (e.g. restarting while receiving the same image)
                        else:
                            # if the transmitter was restarted and the gateway wasn't, new images under old image IDs will be received
                            rel_dir = "raw/" + matches[-1]
                            abs_path = os.path.join(abs_dir, rel_dir)
                            
                            f = open(abs_path, 'rb')
                            pkt = f.read()[-256:]
                            f.close()
                            pktIDlast = (ord(pkt[7]) << 8) | ord(pkt[8])

                            # if last saved packet ID is bigger then current, store the packet in a new file
                            if pktID <= pktIDlast:
                                self.imgList.append(ssdvFile)
                                self.pktList.append(1)
                                self.typeList.append(pktType)
                            # else store the packet in the latest file
                            else:
                                ssdvFile = matches[-1]
                                self.pktList[matchesN[-1]] += 1

                        rel_dir = "raw/" + ssdvFile
                        abs_path = os.path.join(abs_dir, rel_dir)
                        
                        # store raw packet
                        f = open(abs_path, 'ab')
                        f.write(packet)
                        f.close()
                        #self.packets_text.insert(0.0, self.lastPkt + pktInfo + '\r\n')
                        self.packets_text.insert(0.0, pktInfo + '\r\n')
                        self.pktNum_label_val.config(text=str(int(self.pktNum_label_val['text']) + 1))
                        self.lastPktTime = datetime.datetime.now()
                        

                    break
                
                line = line.strip()

                # Telemetry packet
                if  line[0] == 't' and line[1] == ':':
                    self.lastPkt = line[2:]
                    self.lastPktType = 't'
                    self.status("Telemetry packet received.")
                    
                    self.update_telemetry_queue(line[2:] + '\n')

                    # parsing
                    telem = line[2:]
                    fields = telem.split(',')

                    self.txLat_label_val.config(text=fields[3])
                    self.txLon_label_val.config(text=fields[4])
                    self.txAlt_label_val.config(text=str(int(fields[5])))
                    self.txTime_label_val.config(text=fields[2])
                    

                # Unidentified packet
                if line[0] == 'p' and line[1] == ':':
                    self.lastPkt = line[2:]
                    self.lastPktType = 'p'
                    self.status("Unidentified packet received.")
                    break

                # CRC error packet
                if line[0] == 'z' and line[1] == ':':
                    self.lastPkt = line[2:]
                    self.lastPktType = 'z'

                    #self.crcerrVal_label.config(text=str(int(self.crcerrVal_label['text']) + 1))
                    self.status("CRC error in last packet.")
                    break

                
    
            self.after(1, self.read_port) # milliseconds

    def run(self):
        """ Background thread """
        while True:
            item = self._queue.get()

            if item is None:
                break
            
            (func, arg) = item

            if func == "listener_information":
                doc = self.upload_listener_information(self.rxCall_val, self.rxLat_val, self.rxLon_val)
            elif func == "listener_telemetry":
                doc = self.upload_listener_telemetry(self.rxCall_val, self.rxLat_val, self.rxLon_val, self.rxAlt_val)
            elif func == "payload_telemetry":
                doc = self.upload_payload_telemetry(arg, self.rxCall_val)
            elif func == "ssdv_packet":
                doc = self.upload_ssdv_packet(arg, self.rxCall_val)    

    def update_listener(self):
        """ Uploads listener's details to Habitat and periodically updates it """
        # update only if "Upload Data to Habitat" is checked
        if self.upload_buttonVar.get() == 1:
            # update only if all fields are filled correctly
            if self.rxLat_ok == 1 and self.rxLon_ok == 1 and self.rxAlt_ok == 1 and self.rxCall_ok == 1:
                # send update only if fields changed
                if self.rxChangedInfo == 1:
                    self._queue.put(("listener_information", ""))
                    self.rxChangedInfo = 0

                if self.rxChangedTelem == 1:
                    self._queue.put(("listener_telemetry", ""))
                    self.rxChangedTelem = 0

                self.status("Uploading to Habitat enabled.")
            else:
                self.status("Fill out the listener information to allow uploading data!")
        else:
            self.status("Uploading to Habitat disabled.")


    def update_telemetry_queue(self, string):
        """ Adds telemetry packet to upload queue """
        # basic telemetry format check
        stringTemp = string.split('\n')

        if self.upload_buttonVar.get() == 1:
        
            if stringTemp[0][0] == '$' and stringTemp[0][1] == '$' and (stringTemp[0][-5] == '*' or stringTemp[0][-3] == '*'):
        
                if self.rxCall_ok == 1 and self.rxLat_ok == 1 and self.rxLon_ok == 1 and self.rxAlt_ok == 1:
                    
                    with self._lock:
                        if self.rxChangedInfo == 1:
                            self._queue.put(("listener_information", ""))
                            self.rxChangedInfo = 0

                        if self.rxChangedTelem == 1:
                            self._queue.put(("listener_telemetry", ""))
                            self.rxChangedTelem = 0

                    self._queue.put(("payload_telemetry", string))
                else:
                    self.status("Fill out the listener information to allow uploading data!")
                    
            else:
                self.status("Telemetry check ERROR! Not uploaded.")


    def update_ssdv_queue(self, pkt):
        """ Adds SSDV packet to upload queue """
        if self.upload_buttonVar.get() == 1:

            if ord(pkt[0]) == 85 and (ord(pkt[1]) == 102 or ord(pkt[1]) == 103) and len(pkt) == 256:

                if self.rxCall_ok == 1 and self.rxLat_ok == 1 and self.rxLon_ok == 1 and self.rxAlt_ok == 1:

                    self._queue.put(("ssdv_packet", pkt))

                else:
                    self.status("Fill out the listener information to allow uploading data!")

            else:
                self.status("SSDV packet check ERROR! Not uploaded.")


#create the window
root = Tk()

#modify root window
root.title("LoRa Gateway")
root.geometry("1150x635")

app = Application(root, serial.Serial(baudrate=500000))

#kick off the event loop
root.mainloop()
