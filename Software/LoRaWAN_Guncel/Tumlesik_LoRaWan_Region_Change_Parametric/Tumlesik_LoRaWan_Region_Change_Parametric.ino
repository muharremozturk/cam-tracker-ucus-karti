//Kamera ve sensör dataları birlikte test edildi.Kamera datası bttikten sonra farklı header ile sensör dataları gönderildi. Ayrıca resim datasını 2-3-6 parçaya bölerek gönderilmesi için
//Switch-Case kısmı loralı koddan aktarıldı test edildi.2-3-6 parça olarak resmi iletebiliyoruz. Farklı regiondaki SF9'a göre datarateler ve sub band kısımları güncellendi. 
#include "SparkFun_Ublox_Arduino_Library.h"
#include "MCP3X21.h"
#include "driver/rtc_io.h"
#include "Seeed_BMP280.h"
#include "PCA9539.h"
#include "soc/soc.h" //disable brownout problems
#include "soc/rtc_cntl_reg.h" //disable brownout problems
#include "ssdv.h"

#include <FS.h>
#include <SPIFFS.h>
#include <Arduino.h>
#include <esp_camera.h>
#include <SX126x-Arduino.h>
#include <LoRaWan-Arduino.h>
#include <stdlib.h>
#include <EEPROM.h>
#include <SPI.h>
#include <Wire.h>

#define EEPROM_SIZE 11
#define LORAWAN_APP_DATA_BUFF_SIZE 256  /**< Size of the data to be transmitted. */
#define LORAWAN_APP_TX_DUTYCYCLE 30000 /**< Defines the application data transmission duty cycle. 30s, value in [ms]. */
#define APP_TX_DUTYCYCLE_RND 1000   /**< Defines a random delay for application data transmission duty cycle. 1s, value in [ms]. */


#define GpsOFF    ioport.digitalWrite(pa6, HIGH);
#define GpsON     ioport.digitalWrite(pa6, LOW);        
#define CameraON  ioport.digitalWrite(pb2, LOW);
#define CameraOFF ioport.digitalWrite(pb2, HIGH);
#define LedON     ioport.digitalWrite(pa0, HIGH);
#define LedOFF    ioport.digitalWrite(pa0, LOW);
                             
#define PWDN_GPIO_NUM     -1
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

// SSDV DEFINITIONS
#define IMG_BUFF_SIZE               128         // size of the buffer feeding SSDV process
#define LORA_BUFFER                 256
#define FILE_PHOTO "/photo5.jpg"              
#define SSDVDATA "/cam_photo4.txt"    // Resim datasını ssdvden okuyup flasha kayıt ederken oluşturduğumuz dosya bunu her resimde ismini değiştirmek gerek, dosyayı silip yeeniden yazsak bile eski datalar kalabiliyor.Test edildi!!!

TaskHandle_t Task1; //For Dual Core System
 
DeviceClass_t CurrentClass = CLASS_A;    
LoRaMacRegion_t CurrentRegion = LORAMAC_REGION_EU868;   
lmh_confirm CurrentConfirm = LMH_UNCONFIRMED_MSG;  


hw_config hwConfig;

// ESP32 - SX126x pin configuration
int PIN_LORA_RESET = 32;   // LORA RESET
int PIN_LORA_NSS = 15;   // LORA SPI CS
int PIN_LORA_SCLK = 14;  // LORA SPI CLK
int PIN_LORA_MISO = 12;  // LORA SPI MISO
int PIN_LORA_DIO_1 = 4; // LORA DIO_1
int PIN_LORA_BUSY = 33;  // LORA SPI BUSY
int PIN_LORA_MOSI = 13;  // LORA SPI MOSI
int RADIO_TXEN = -1;   //2 LORA ANTENNA TX ENABLE ,
int RADIO_RXEN = -1;   //25 LORA ANTENNA RX ENABLE ,


//***************************** UPDATE HERE WITH YOUR DEVICE KEYS **************************************/

uint8_t nodeDeviceEUI[8] = {0x60, 0x81, 0xF9, 0x18, 0x5A, 0x36, 0xEF, 0xD7}; // MUST BE MSB

uint8_t nodeAppEUI[8] = {0x60, 0x81, 0xF9, 0x6E, 0x4B, 0xF1, 0x18, 0xD8}; // MUST BE MSB

uint8_t nodeAppKey[16] = {0xE7, 0xE7, 0x39, 0xAC, 0x01, 0xE2, 0x2A, 0x10, 0x92, 0x3F, 0x56, 0xA4, 0x6E, 0x5D, 0x3A, 0x2E}; // MUST BE MSB



// Foward declaration
/** LoRaWAN callback when join network finished */
static void lorawan_has_joined_handler(void);
/** LoRaWAN callback when join network failed */
static void lorawan_join_fail_handler(void);
/** LoRaWAN callback when data arrived */
static void lorawan_rx_handler(lmh_app_data_t *app_data);
/** LoRaWAN callback after class change request finished */
static void lorawan_confirm_class_handler(DeviceClass_t Class);
/** LoRaWAN callback after class change request finished */
static void lorawan_unconfirm_tx_finished(void);
/** LoRaWAN callback after class change request finished */
static void lorawan_confirm_tx_finished(bool result);
/** LoRaWAN Function to send a package */
static void send_lora_frame(void);
static uint32_t timers_init(void);

void ReadPressure(void);
void ReadVoltage(void);
void setupBMP(void);
void setupGPS(void);
void save_photo(void);
void Photo_Flash_To_LoRa(void);
void cameraconfig(void);


// APP_TIMER_DEF(lora_tx_timer_id);                ///< LoRa tranfer timer instance.
TimerEvent_t appTimer;                            ///< LoRa tranfer timer instance.

static uint8_t m_lora_app_data_buffer[LORAWAN_APP_DATA_BUFF_SIZE];        ///< Lora user application data buffer.
static lmh_app_data_t m_lora_app_data = {m_lora_app_data_buffer, 0, 0, 0, 0}; ///< Lora user application data structure.


/**@brief Structure containing LoRaWan callback functions, needed for lmh_init()
*/


static lmh_callback_t lora_callbacks = {BoardGetBatteryLevel, BoardGetUniqueId, BoardGetRandomSeed,
                    lorawan_rx_handler, lorawan_has_joined_handler, lorawan_confirm_class_handler, lorawan_join_fail_handler,
                    lorawan_unconfirm_tx_finished, lorawan_confirm_tx_finished};


//************************** LoRaWAN Settings ********************
const unsigned TX_INTERVAL = 300000;  //5dakikada 1 sensör datasını atacağız.
/*
// The LoRaWAN region to use, automatically selected based on your location. So GPS fix is necesarry
u1_t os_getRegion (void) { return Lorawan_Geofence_region_code; } //do not change this

// GEOFENCE
uint8_t Lorawan_Geofence_no_tx  = 0; //do not change this
uint8_t Lorawan_Geofence_region_code = _REGCODE_UNDEF; //do not change this
uint8_t Lorawan_Geofence_special_region_code = _REGCODE_UNDEF; //do not change this

uint8_t lastLoRaWANRegion = _REGCODE_UNDEF; //do not change this

boolean OTAAJoinStatus = false; //do not change this.


*/  // GEOFENCE KISMI TEST EDİLİRKEN AKTİF EDİLECEK!!!!!!

uint32_t last_packet = 0; //do not change this. Timestamp of last packet sent.

//#define DEVMODE // Development mode. Uncomment to enable for debugging.

uint8_t measurementSystem = 0; //0 for metric (meters, km, Celcius, etc.), 1 for imperial (feet, mile, Fahrenheit,etc.)

// Initialize Scheduler and timer
uint32_t err_code = timers_init();      
int8_t  i=0; // LoRaWAN kodunun ikinci coreda tek sefer çalışması için kullanıyoruz.  
int counter=0;  // Sensör verilerini byte byte ayırırken fonksiyonda kullanıyoruz.          
//************************** uBlox GPS  Settings ********************
long lastTime = 0; //Simple local timer. Limits amount if I2C traffic to Ublox module.
boolean gpsFix=false; //do not change this.
boolean ublox_high_alt_mode_enabled = false; //do not change this.
boolean gpsBMPSetup=false; //do not change this.

//********************************* Power Settings ******************************
int   battWait=60;    //seconds sleep if super capacitors/batteries are below battMin (important if power source is solar panel)
float battMin=3.5;    // min Volts to TX. (Works with 3.3V too but 3.5V is safer)
float gpsMinVolt=4.5; //min Volts for GPS to wake up. (important if power source is solar panel) //do not change this
//************************** Camera  Settings ********************
boolean CameraSetup=false; //do not change this.
//************************** BMP280  Settings ********************
float    bmp_pressure;
float    bmp_temperature ;
float    bmp_altitude ;
uint32_t MSL = 102009; // Mean Sea Level in Pa
//********************************* Misc Settings ******************************
int txCount = 1;
const uint8_t address = 0x4D;
const uint16_t ref_voltage = 3300;  // in mV
float voltage = 0;

//******************************************** SSDV KODDAKİ TANIMLAMALARI ****************************************************/

static esp_err_t cam_err;
uint16_t res_len,lenn;
int8_t indexson;
uint8_t PaketSayi=3; // burası değişitirilnce DataRate değiştirmeyi unutma!!!
uint8_t indexsayac=0;
int bufSize = 0;
int cur=0; //for process ssdv function
uint16_t indexx=0,c=0; //for process ssdv function
int8_t ssdvPacketCount_2=0,devamm=0;  // ssdvPacketCount_2 gönderdiğimiz 3lü paket sayısını tuttuğumuz değişken , devamm ise enerjiden paket durdurursak kaldığımız 3 lü paketten devam etmemizi sağlıyor. 25.paketin 2.datası gönderildi ax=3 olur son paketi gönderir.  
int cur3=0; // cur3 flash hafıza içindeki dosyanın içinde nerede kalacağını tuttuğumuz değişkendir.
int8_t ssdvPacketCount=0;  //for flashtolora functıon
int cur2=0; //for flashtolora functıon
int k=0; //iki kez join olma deneniyor.


uint8_t ssdvQuality = 4;                 // 0-7 corresponding to JPEG quality: 13, 18, 29, 43, 50, 71, 86 and 100

static const char callsign[] = "TA2MHR";        // maximum of 6 characters
ssdv_t ssdv;

uint8_t imgBuff[IMG_BUFF_SIZE];
uint8_t flashBuff[LORA_BUFFER]; // Do not change
uint8_t ssdvBuff[LORA_BUFFER]; //Do not change
uint8_t yeniBuff[LORA_BUFFER]; //Do not change
int8_t imageID = 0;

BMP280 bmp280; //temp and pressure sensor
MCP3021 mcp3021(address); //ı2c analog battery measure
SFE_UBLOX_GPS myGPS; // gps
PCA9539 ioport(0x74); // Base address starts at 0x74 for A0 = L and A1 = L

/**@brief Setup function
 */
void setup()
{  
//  setCpuFrequencyMhz(20); // Save power by going slowly
  delay(5000);//do not change this
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
 
  // Initialize Serial for debug output
  Serial.begin(115200);

  Serial.println("=====================================");
  Serial.println("SX126x LoRaWan test");
  Serial.println("=====================================");

  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    ESP.restart();
  }
  else {
    delay(500);
    Serial.println("SPIFFS mounted successfully");
  }  
   
  // Define the HW configuration between MCU and SX126x
  hwConfig.CHIP_TYPE = SX1262_CHIP;     // Example uses an eByte E22 module with an SX1262
  hwConfig.PIN_LORA_RESET = PIN_LORA_RESET; // LORA RESET
  hwConfig.PIN_LORA_NSS = PIN_LORA_NSS;  // LORA SPI CS
  hwConfig.PIN_LORA_SCLK = PIN_LORA_SCLK;   // LORA SPI CLK
  hwConfig.PIN_LORA_MISO = PIN_LORA_MISO;   // LORA SPI MISO
  hwConfig.PIN_LORA_DIO_1 = PIN_LORA_DIO_1; // LORA DIO_1
  hwConfig.PIN_LORA_BUSY = PIN_LORA_BUSY;   // LORA SPI BUSY
  hwConfig.PIN_LORA_MOSI = PIN_LORA_MOSI;   // LORA SPI MOSI
  hwConfig.RADIO_TXEN = -1;         // LORA ANTENNA TX ENABLE
  hwConfig.RADIO_RXEN = -1;         // LORA ANTENNA RX ENABLE
  hwConfig.USE_DIO2_ANT_SWITCH = true;    // Example uses an CircuitRocks Alora RFM1262 which uses DIO2 pins as antenna control
  hwConfig.USE_DIO3_TCXO = true;        // Example uses an CircuitRocks Alora RFM1262 which uses DIO3 to control oscillator voltage
  hwConfig.USE_DIO3_ANT_SWITCH = false;  // Only Insight ISP4520 module uses DIO3 as antenna control

  Wire.begin(26,27);
 
  ioport.pinMode(pa6, OUTPUT); // GPS open and close port
  GpsON;
 
  ioport.pinMode(pb2, OUTPUT); // CAMERA open and close port
  CameraON;
       
  ioport.pinMode(pa0, OUTPUT); // LED open and close port
  LedOFF;  

  mcp3021.init(&Wire); //kamera init yapamama durumu testi için buraya aldık.
   
  if (err_code != 0)
  {
    Serial.printf("timers_init failed - %d\n", err_code);
  }

  // Initialize LoRa chip.
  err_code = lora_hardware_init(hwConfig);
  if (err_code != 0)
  {
    Serial.printf("lora_hardware_init failed - %d\n", err_code);
  }

  // Setup the EUIs and Keys
  lmh_setDevEui(nodeDeviceEUI);
  lmh_setAppEui(nodeAppEUI);
  lmh_setAppKey(nodeAppKey); // Only needed for OOTA registration  

 
   xTaskCreatePinnedToCore( // For Dual Core System
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    10000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */                  
    delay(100);
}

/**@brief Main loop
 */

void Task1code( void * pvParameters ){
 // Serial.print("Task1 running on core ");
 // Serial.println(xPortGetCoreID());
 
       
  for(;;){ // We can use inside for loop like void loop if you use this , code will be run core 0
    vTaskDelay(3);
   if(i==0){  
   
  // to start only once
  Serial.println("...........!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!..........................!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!..................");

  
  Serial.println("LoRaWAN INITIALIZE....");

  
  Serial.println("...........!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!..........................!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!..................");
  
/**@brief Structure containing LoRaWan parameters, needed for lmh_init()
 */
  static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_3 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; //  lora_param_init = {ADR,DATARATE , LORAWAN_PUBLIC_NETWORK,KATILMA SAYISI, TX ÇIKIŞ GÜÇÜ}

  // Initialize LoRaWan
  err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
  if (err_code != 0)
  {
    Serial.printf("lmh_init failed - %d\n", err_code);
  }

  // For some regions we might need to define the sub band the gateway is listening to
  /// \todo This is for Dragino LPS8 gateway. How about other gateways???
  if (!lmh_setSubBandChannels(1))
  {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
  }
   
  lmh_join();

  i=1;
  }  


/*
  if(Lorawan_Geofence_region_code == _REGCODE_EU868){ 
   
   // LoRaMac datarates definition, from DR_0 to DR_5   EU868 DR 0-1-2 :51 BYTE ,DR3: 115 BYTE, DR4-5:242 BYTE 
   
   PaketSayi=3;  // DR3 (SF9 BW125kHz) SF9 is is better/optimum spreading factor for image datas                   
   CurrentRegion = LORAMAC_REGION_EU868;  // Region:EU868
   
   // to start only once
   Serial.println("LoRaWAN_EU868_INITIALIZE....");

   static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_3 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; 

   // Initialize LoRaWan
   err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
   if (err_code != 0)
    {
    Serial.printf("lmh_init failed - %d\n", err_code);
    }

   if (!lmh_setSubBandChannels(1))
    {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
    }
  
   lmh_join();
   
  }else if (Lorawan_Geofence_region_code == _REGCODE_RU864) {
  
   //LoRaMac datarates definition, from DR_0 to DR_5*/ /*RU864 DR 0-1-2 :51 BYTE ,DR3: 115 BYTE, DR4-5:242 BYTE 
   PaketSayi=3;  //DR3 (SF9 BW125kHz) SF9 is is better/optimum spreading factor for image datas
   CurrentRegion = LORAMAC_REGION_RU864;   // Region:US915
    
   // to start only once
   Serial.println("LoRaWAN_RU864_INITIALIZE....");

   static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_3 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; 

   // Initialize LoRaWan
   err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
   if (err_code != 0)
   {
    Serial.printf("lmh_init failed - %d\n", err_code);
   }
    
   if (!lmh_setSubBandChannels(1))
   {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
   }

   lmh_join();
   
  }else if (Lorawan_Geofence_region_code == _REGCODE_US915) { 
    //LoRaMac datarates definition, from DR_0 to DR_5   US915 DR 0: 11 BYTE, DR 1 : 53 BYTE , DR 2 :125 BYTE ,DR3: 242 BYTE, DR 4:242 BYTE , DR 5:TANIMLANMAMIŞ 
    
    PaketSayi=6; //DR1 (SF9 BW125kHz) SF9 is is better/optimum spreading factor for image datas,DR2 (SF8 BW125kHz)             
    CurrentRegion = LORAMAC_REGION_US915;    // Region:US915
    
    // to start only once
    Serial.println("LoRaWAN_US915_INITIALIZE....");

    static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_1 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; 

     // Initialize LoRaWan
     err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
     if (err_code != 0)
      {
       Serial.printf("lmh_init failed - %d\n", err_code);
      }

      //TTN and Helium only supports second sub band (channels 8 to 15)
      //so we should force to initiate a join with second sub band channels.

      if (!lmh_setSubBandChannels(2))
      {
       Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
      }

      lmh_join();    
      
  }else if (Lorawan_Geofence_region_code == _REGCODE_AU915) { 
    
    //LoRaMac datarates definition, from DR_0 to DR_5*/ /*AU915 DR 0-1-2 :51 BYTE ,DR3: 115 BYTE, DR4-5:242 BYTE  
    PaketSayi=3;                                  //DR3 (SF9 BW125kHz) SF9 is is better/optimum spreading factor for image datas                   
    CurrentRegion = LORAMAC_REGION_AU915;    // Region:AU915 
    
    // to start only once
    Serial.println("LoRaWAN_AU915_INITIALIZE....");

    static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_3 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; 

    // Initialize LoRaWan
    err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
    if (err_code != 0)
     {
      Serial.printf("lmh_init failed - %d\n", err_code);
     }
       
    //TTN and Helium only supports second sub band (channels 8 to 15)
    //so we should force to initiate a join with second sub band channels.
    if (!lmh_setSubBandChannels(2))
    {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
    }

    lmh_join();
    
  }else if (Lorawan_Geofence_region_code == _REGCODE_KR920) { 
    
    //LoRaMac datarates definition, from DR_0 to DR_5*/ /*KR920 DR 0-1-2 :51 BYTE ,DR3: 115 BYTE, DR4-5:242 BYTE
    PaketSayi=3; //DR3 (SF9 BW125kHz) SF9 is is better/optimum spreading factor for image datas                
    CurrentRegion = LORAMAC_REGION_KR920;    // Region:KR920
    
    // to start only once
    Serial.println("LoRaWAN_KR920_INITIALIZE....");

    static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_3 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; 

     // Initialize LoRaWan
    err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
    if (err_code != 0)
    {
     Serial.printf("lmh_init failed - %d\n", err_code);
    }

    if (!lmh_setSubBandChannels(1))
    {
     Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
    }

    lmh_join();
    
  }else if (Lorawan_Geofence_region_code == _REGCODE_AS923) { 
    
    //LoRaMac datarates definition, from DR_0 to DR_5*/ /*AS923 DR 0-1-2 :59 BYTE ,DR3: 123 BYTE, DR 4-5:250 BYTE     
    PaketSayi=3;//DR3 (SF9 BW125kHz) SF9 is is better/optimum spreading factor for image datas                   
    CurrentRegion = LORAMAC_REGION_AS923;    //Region:AS923
          
    // to start only once
    Serial.println("LoRaWAN_AS923_INITIALIZE....");

    static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_3 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; 

     // Initialize LoRaWan
    err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
    if (err_code != 0)
    {
     Serial.printf("lmh_init failed - %d\n", err_code);
    }

    if (!lmh_setSubBandChannels(1))
    {
     Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
    }
  
    lmh_join(); 
   
  }  */
   
}}

void loop()
{
  voltage = readBatt();
  Serial.print(voltage);
  Serial.println("V");
  if(!CameraSetup){
  Serial.println(F("OV2640 Camera setup"));//Ek olarak iki coreda ortak kullanılan parametrelerde kamera çalışmayabiliyor en son kodda dikkat etmeliyiz!!!!     
  cameraconfig(); // Kamera konfigrasyonlarının yapıldığı yer.Eğer kameradan önce lorawan başlatılırsa kamera başlamıyor. İlk olarak kameranın ayarlarının yapılıp initialize edilmesi gerek.   
  delay(3000);  
  setupBMP();
  CameraSetup = true; }
/*
  Wire.begin(26,27); //kod sürekli döneceği için gpsden sonra burayı tekrar başlatmak için en son güncel kartta değişecek.
  ReadPressure(); //en son bu kısımı kaldırırız.
  ReadVoltage(); //en son bu kısımı kaldırırız.*/

/*  if( lmh_join_status_get() == LMH_SET){
  
   save_photo();
   
  }*/

  if(!gpsBMPSetup){
  Wire.begin(27,26); // gps calıstırmak için değiştirdik.
  Serial.println(F("GPS setup"));  
  setupGPS();
  Serial.println(F("Searching for GPS fix..."));
  gpsBMPSetup = true; }
 
if (((voltage > battMin) && gpsFix) || ((voltage > gpsMinVolt) && !gpsFix)) {
    Serial.println("Searching  GPS .......");
    Wire.begin(27,26);
    gpsFix=true;  
/*  if(gpsFix) {
    // Let LMIC handle LoRaWAN background tasks
    os_runstep();      
  } */ /* Bizdeki lorawan kütüphanesi için buna gerek olmayabilir. */

/*
 *  
 *   if ((millis() - last_packet > TX_INTERVAL && !(LMIC.opmode & (OP_JOINING|OP_TXRXPEND))) || !gpsFix){
 
    GpsON;
    delay(500);

    if(!ublox_high_alt_mode_enabled){
      setupUBloxDynamicModel();
    }

     // Calling myGPS.getPVT() returns true if there actually is a fresh navigation solution available.

    if (myGPS.getPVT() && (myGPS.getFixType() !=0) && (myGPS.getSIV() > 0)) {
      gpsFix=true;    
     
      checkRegionByLocation();

      if(lastLoRaWANRegion != Lorawan_Geofence_region_code) {
          Serial.println(F("Region has changed, force LoRaWAN OTAA Login"));
          OTAAJoinStatus = false;
          lastLoRaWANRegion = Lorawan_Geofence_region_code;
        }

      if(!OTAAJoinStatus && (Lorawan_Geofence_no_tx == 0)){            
          Serial.println(F("LoRaWAN OTAA Login initiated..."));    
          startJoining();
          Serial.println(F("LoRaWAN OTAA Login success..."));    
          OTAAJoinStatus = true;
          freeMem();
      }
     
      updateTelemetry();
      #if defined(DEVMODE)        
        Serial.print(F("Telemetry Size: "));
        Serial.print(sizeof(m_lora_app_data.buffer));
        Serial.println(F(" bytes"));
      #endif  

      //need to save power
      if (readBatt() < 4.5) {
       GpsOFF;
       ublox_high_alt_mode_enabled = false; //gps sleep mode resets high altitude mode.
       delay(500);      
      }
     
      if (Lorawan_Geofence_no_tx == 0) {
          send_lora_frame();
          Serial.println(F("LoRaWAN packet sent.."));  
        }                
       
        freeMem();
   
    }

      #if defined(DEVMODE)
        printGPSandSensorData();
      #endif    

  }
 */ /* Bu kısım geofence ve (LMIC.opmode & (OP_JOINING|OP_TXRXPEND))terim ile ne yapılmaya calısıldığı öğrenilince düzeltilerek aktif edilecek */
   
/*  if (myGPS.getPVT() && (myGPS.getFixType() !=0) && (myGPS.getSIV() > 0))
  { //buraya bir bölge değiştirince getPVT değiştiği için ve diğer parametrelerde sağlanırsa giriliyor.
    i=5; // Core 0 initialize lorawan
    delay(10000); // Wait for initialize lorawan
   
    gpsFix=true;  
    Serial.println("GPS FİXLENDİ.");  
    //burada bölge değiştirme işlemleri de olacak!!!!!  
  }*/
  if(gpsFix){
   last_packet =millis(); 
   Serial.print("Saniye:");
   Serial.println(last_packet);

   updateTelemetry_1(); // gps eski kartta sda ve scl si yanlış bağlandığı için uptade telemetry fonksiyonu tek seferde tüm dataları basamaz.
   updateTelemetry_2(); // gps sda ve scl yanlıs baglandıgı için bu fonk. olusturuldu.
   send_lora_frame(); /** 38 datayı gönderme yaptığımız yer */  
   delay(5000); //EU868 DR3-SF-9 38BYTE DATA GÖNDERİLİRKEN,PAKETLER ARASI BEKLENİLEN SÜRE 35SANİYEDİR. 35000
  }    
  
    #if defined(DEVMODE)        
       Serial.print("Telemetry Size: ");
       Serial.print(m_lora_app_data.buffsize);
       Serial.println("bytes");
    #endif    
   
   Wire.begin(26,27);
   readBatt();
   //need to save power
   if (voltage < 4.5) {
     Serial.println("Batarya dolumu yapılıyor!!!!");
     GpsOFF;
     Wire.begin(27,26);
     ublox_high_alt_mode_enabled = false; //gps sleep mode resets high altitude mode.
     delay(500);      
   }

   #if defined(DEVMODE)
      printGPSandSensorData();
   #endif    

  }else{
    Wire.begin(26,27);
    Serial.println("else yapısına girdi have a big problemmm!!!!!!!!");
    GpsOFF;  
    Wire.begin(27,26);
    ublox_high_alt_mode_enabled = false; //gps sleep mode resets high altitude mode.    
    delay(battWait * 1000);
   
  }
 
}
 

static void lorawan_join_fail_handler(void)
{  
  Serial.println("OTAA joined failed");
  Serial.println("Check LPWAN credentials and if a gateway is in range");
  // Restart Join procedure
  Serial.println("Restart network join request");
  //YENİ REGİONA GEÇİŞTE EĞER İLK BAŞLATMA KOMUTUNDAN SONRA ÇALIŞMAZSA BURADA TEKRARDAN BAĞLANMASI İÇİN FONKSİYON CAĞIRABİLİRİZ!!!
}

/**@brief LoRa function for handling HasJoined event.
 */
static void lorawan_has_joined_handler(void)
{

#if (OVER_THE_AIR_ACTIVATION != 0)
  Serial.println("Network Has Joined");  
#else
  Serial.println("OVER_THE_AIR_ACTIVATION != 0");
  

#endif
  lmh_class_request(CurrentClass);

  TimerSetValue(&appTimer, LORAWAN_APP_TX_DUTYCYCLE);
  TimerStart(&appTimer);
  // app_timer_start(lora_tx_timer_id, APP_TIMER_TICKS(LORAWAN_APP_TX_DUTYCYCLE), NULL);
  //Serial.println("Sending frame");
  //send_lora_frame();
}

/**@brief Function for handling LoRaWan received data from Gateway
 *
 * @param[in] app_data  Pointer to rx data
 */
static void lorawan_rx_handler(lmh_app_data_t *app_data)
{
  Serial.printf("LoRa Packet received on port %d, size:%d, rssi:%d, snr:%d\n",
          app_data->port, app_data->buffsize, app_data->rssi, app_data->snr);

  for (int i = 0; i < app_data->buffsize; i++)
  {
    Serial.printf("%0X ", app_data->buffer[i]);
  }
  Serial.println("");

  switch (app_data->port)
  {
  case 3:
    // Port 3 switches the class
    if (app_data->buffsize == 1)
    {
      switch (app_data->buffer[0])
      {
      case 0:
        lmh_class_request(CLASS_A);
        break;

      case 1:
        lmh_class_request(CLASS_B);
        break;

      case 2:
        lmh_class_request(CLASS_C);
        break;

      default:
        break;
      }
    }
    break;

  case LORAWAN_APP_PORT:
    // YOUR_JOB: Take action on received data
    break;

  default:
    break;
  }
}

/**@brief Function to confirm LORaWan class switch.
 *
 * @param[in] Class  New device class
 */
static void lorawan_confirm_class_handler(DeviceClass_t Class)
{
  Serial.printf("switch to class %c done\n", "ABC"[Class]);

  // Informs the server that switch has occurred ASAP
  m_lora_app_data.buffsize = 0;
  m_lora_app_data.port = LORAWAN_APP_PORT;
  lmh_send(&m_lora_app_data, CurrentConfirm);
}

/**
 * @brief Called after unconfirmed packet was sent
 *
 */
static void lorawan_unconfirm_tx_finished(void)
{
  Serial.println("Uncomfirmed TX finished");
}

/**
 * @brief Called after confirmed packet was sent
 *
 * @param result Result of sending true = ACK received false = No ACK
 */
static void lorawan_confirm_tx_finished(bool result)
{
  Serial.printf("Comfirmed TX finished with result %s", result ? "ACK" : "NAK");
}

/**@brief Function for sending a LoRa package.
 */
static void send_lora_frame(void)
{
  if (lmh_join_status_get() != LMH_SET)
  {
    //Not joined, try again later
    Serial.println("We are in the Send_Lora_Frame Function");    
    Serial.println("did not join network, skip sending frame");
    return;
  }

  m_lora_app_data.port = LORAWAN_APP_PORT;
   
  m_lora_app_data.buffsize = 38;

  Serial.print("Data: ");
  Serial.println((char *)m_lora_app_data.buffer);
  Serial.print("Size: ");
  Serial.println(m_lora_app_data.buffsize);
  Serial.print("Port: ");
  Serial.println(m_lora_app_data.port);


  lmh_error_status error = lmh_send(&m_lora_app_data, CurrentConfirm); //gönderme kısmı
  if (error == LMH_SUCCESS)
  {
  }
  Serial.printf("lmh_send result %d\n", error);
}

/**@brief Function for handling a LoRa tx timer timeout event.
 */
static void tx_lora_periodic_handler(void){
/* Serial.println("tx_lora_periodic_handler");
 TimerSetValue(&appTimer, LORAWAN_APP_TX_DUTYCYCLE);
 TimerStart(&appTimer);
 Serial.println("Sending frame");
 t=t+1;
 
 send_lora_frame(); */ //belirli sürede atmasını timers_init ve tx_lora_periodic_handler ile sağlamaktayız.
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static uint32_t timers_init(void)
{ /*
  Serial.println("timers_init");
  appTimer.timerNum = 3;
  TimerInit(&appTimer, tx_lora_periodic_handler);
  return 0; */  /*belirli sürede atmasını timers_init ve tx_lora_periodic_handler ile sağlamaktayız. */
}
/*
void checkRegionByLocation() {

  float tempLat = myGPS.getLatitude() / 10000000.f;
  float tempLong = myGPS.getLongitude() / 10000000.f;
 
  Lorawan_Geofence_position(tempLat,tempLong);
   
} // geofence kodu aktif edince bu kısmı aktif edeceğiz!!!!!
*/

void  ReadVoltage(){
   
    uint16_t result = mcp3021.read();
    result = mcp3021.read();

    Serial.print(F("ADC: "));
    Serial.println(result);

   
    Serial.print(F("mV: "));
    Serial.println(readBatt());

    delay(1000);}
   
float readBatt() {
  float R1 = 56.0; // 56K
  float R2 = 10.0; // 10K

  do {
    for(int i=0;i<10;i++){
      voltage +=mcp3021.read();
    }
    voltage=voltage/10;
    voltage = (voltage * 3.3) / 1024.0;
    voltage = voltage / (R2/(R1+R2));
  } while (voltage > 16.0);
  return voltage ;
}
   
void ReadPressure(){ // kaldıracağız test amaclı kalıyor suan
       
    bmp_pressure = bmp280.getPressure()/ 100.f;
    bmp_temperature = bmp280.getTemperature();
    bmp_altitude =  bmp280.calcAltitude(MSL,bmp_pressure, bmp_temperature );

    Serial.print(F("Pressure = "));
    Serial.print(bmp_pressure);
    Serial.println(" Pa");
   
    Serial.print(F("Temperature = "));
    Serial.print(bmp_temperature);
    Serial.println(" *C");

    Serial.print(F("Altitude = "));
    Serial.print(bmp_altitude);
    Serial.println(" m");  
    delay(1000);
}

void cameraconfig(){

  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
// 0-8 corresponfing to 320x240, 352x288, 640x480, 800x480, 1024x768, 1280x960, 1600x1200, 2048x1536, 2592x1944
//  FRAMESIZE_QVGA 320x240, FRAMESIZE_VGA 640x480, FRAMESIZE_SVGA 800x600
//  FRAMESIZE_XGA 1024x768, FRAMESIZE_UXGA 1600x1200, FRAMESIZE_QXGA 2048x1536
  config.frame_size = FRAMESIZE_VGA;
  config.jpeg_quality = 10;
  config.fb_count = 1;

  // camera init
  cam_err = esp_camera_init(&config);
  if (cam_err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", cam_err);
    return;
  }else Serial.println("Camera..OK");

  delay(2000);//kamera beyaz dengesi vb. ayarları yapmak için zamana ihtiyaç duyuyor  
 
  EEPROM.begin(EEPROM_SIZE); // Kameradan önce tanımlanmamalıdır yoksa kamerada hata vermektedir!!!
}

void setupGPS() {
 
  if (myGPS.begin() == false) //Connect to the Ublox module using Wire port
  {
    Serial.println(F("Ublox GPS not detected at default I2C address. Please check wiring. Freezing."));
    while (1);
  }

  // do not overload the buffer system from the GPS, disable UART output
  myGPS.setUART1Output(0); //Disable the UART1 port output
  myGPS.setUART2Output(0); //Disable Set the UART2 port output
  myGPS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)

  //myGPS.enableDebugging(); //Enable debug messages over Serial (default)

  myGPS.setNavigationFrequency(2);//Set output to 2 times a second. Max is 10
  byte rate = myGPS.getNavigationFrequency(); //Get the update rate of this module
  Serial.print("Current update rate for GPS: ");
  Serial.println(rate);

  myGPS.saveConfiguration(); //Save the current settings to flash and BBR  
 
}

void setupBMP(){
    if (!bmp280.init()) {
    Serial.println("Device not connected or broken!");}
}

void updateTelemetry_1() {
 
  float latitude = myGPS.getLatitude() / 10000000.f;
  float longtitude= myGPS.getLongitude() / 10000000.f;
  float heading= myGPS.getHeading() / 100000.f;
  float tempAltitudeLong = 0; //meters or feet
  float tempAltitudeShort = 0; //km or miles
  float tempSpeed = 0; //km or miles
  float tempTemperature = 0; //Celcius or Fahrenheit
  byte  uydu= myGPS.getSIV();
  byte  imza= 0x56; // Sensör dataları için ayrı bir data geldiğini anlamamız için
 
  if(measurementSystem == 0){ //Metric
   
    tempAltitudeLong = myGPS.getAltitude() / 1000.f; //meters
    tempAltitudeShort = tempAltitudeLong / 1000.f; //kilometers  
    tempSpeed = myGPS.getGroundSpeed() * 0.0036f; //km/hour      
   
   } else { //Imperial
   
    tempAltitudeLong = (myGPS.getAltitude() * 3.2808399)  / 1000.f;//feet
    tempAltitudeShort = tempAltitudeLong / 5280.f;//miles      
    tempSpeed = myGPS.getGroundSpeed() *  0.00223694f; //mile/hour    
  }    
   
  memset(m_lora_app_data.buffer, 0, 38);
 
  //latitude,longtitude,altitude,speed,course,sattelite,battery,temp,pressure
  byteAddByte(imza,&counter,m_lora_app_data.buffer);// Kamera datası değil GPS ve BMP datasını olduğunu anlamamız için attığımız farklı imza 0x86
  floatAddByte(latitude,&counter,m_lora_app_data.buffer); // gps lat ve long degerleri 1000 ile carpılabilir !!!!
  floatAddByte(longtitude,&counter,m_lora_app_data.buffer);
  floatAddByte(tempAltitudeLong,&counter,m_lora_app_data.buffer);  
  floatAddByte(tempAltitudeShort,&counter,m_lora_app_data.buffer);
  floatAddByte(tempSpeed,&counter,m_lora_app_data.buffer);
  floatAddByte(heading,&counter,m_lora_app_data.buffer);  
  byteAddByte(uydu,&counter,m_lora_app_data.buffer);  
 
  Serial.print("Latitude:");
  Serial.print(latitude);
  Serial.print("Longtitude:");
  Serial.print(longtitude);
  Serial.print("GPS-Altitude:");
  Serial.print(tempAltitudeLong);
  Serial.print("GPS-Altitude-2:");
  Serial.print(tempAltitudeShort);
  Serial.print("Speed:");
  Serial.print(tempSpeed);
  Serial.print("Heading:");
  Serial.print(heading);
  Serial.print("Uydu Sayısı:");
  Serial.print(uydu);

  Serial.println();
 
}

// GPS SDA-SCL yanlış bağlandığından ötürü bu tarz bir şey yaptık güncel kart gelince tek fonk. toplayacağız yapacağız!
void updateTelemetry_2() {

  Wire.begin(26,27); // GPS SDA-SCL yanlış bağlandığından ötürü test için
 
  float pressure= bmp280.getPressure()/ 100.f;
 
  Serial.print("update-basınc:");
  Serial.println(pressure);
  float tempTemperature = 0; //Celcius or Fahrenheit
 
  Serial.print("update-volt:");
  Serial.println(voltage);
  if(measurementSystem == 0){ //Metric
   
    tempTemperature = bmp280.getTemperature(); //Celsius  
    Serial.print("update-temp:");
    Serial.println(tempTemperature);
   
   } else { //Imperial
   
    tempTemperature =  (bmp280.getTemperature()* 1.8f) + 32; //Fahrenheit      
  }  
   
//  byteAddByte(86,&data.counter,data.byteArray);// Kamera datası değil GPS ve BMP datasını olduğunu anlamamız için attığımız farklı imza 0x86    
  floatAddByte(pressure,&counter,m_lora_app_data.buffer);
  floatAddByte(tempTemperature,&counter,m_lora_app_data.buffer);
  floatAddByte(voltage,&counter,m_lora_app_data.buffer);
  counter=0;
/*
   Serial.println("");
   Serial.println(data.byteArray[34]);
   Serial.println(data.byteArray[35]);
   Serial.println(data.byteArray[36]);
   Serial.println(data.byteArray[37]);
    float fy;
  ((uint8_t*)&fy)[0] = data.byteArray[34];
  ((uint8_t*)&fy)[1] = data.byteArray[35];
  ((uint8_t*)&fy)[2] = data.byteArray[36];
  ((uint8_t*)&fy)[3] = data.byteArray[37];
   Serial.print(fy);
 */ /* nu kısımda yanlıs giden data var mı kontrol amaclı olarak yazdık.*/

}

void printGPSandSensorData()
{
    lastTime = millis(); //Update the timer

    byte fixType = myGPS.getFixType();

    Serial.print(F("FixType: "));
    Serial.print(fixType);    

    int SIV = myGPS.getSIV();
    Serial.print(F(" Sats: "));
    Serial.print(SIV);

    float flat = myGPS.getLatitude() / 10000000.f;
   
    Serial.print(F(" Lat: "));
    Serial.print(flat);    

    float flong = myGPS.getLongitude() / 10000000.f;    
    Serial.print(F(" Long: "));
    Serial.print(flong);        

    float altitude = myGPS.getAltitude() / 1000;
    Serial.print(F(" Alt: "));
    Serial.print(altitude);
    Serial.print(F(" (m)"));

    //float speed = myGPS.getGroundSpeed() * 0.0036f;
    //Serial.print(F(" Speed: "));
    //Serial.print(speed);
    //Serial.print(F(" (km/h)"));

    //long heading = myGPS.getHeading() / 100000;
    //Serial.print(F(" Heading: "));
    //Serial.print(heading);
    //Serial.print(F(" (degrees)"));
       
    Serial.print(" Time: ");    
    Serial.print(myGPS.getYear());
    Serial.print("-");
    Serial.print(myGPS.getMonth());
    Serial.print("-");
    Serial.print(myGPS.getDay());
    Serial.print(" ");
    Serial.print(myGPS.getHour());
    Serial.print(":");
    Serial.print(myGPS.getMinute());
    Serial.print(":");
    Serial.print(myGPS.getSecond());
/*    bu dataları void loop basında gösteriyorduk ve gps sada scl farklı oldugundan suan sadece test amaclı gps acık en son kart geldiğinde bu kısımlar acık olacak.
    Serial.print(" Temp: ");
    Serial.print(bmp280.getTemperature());
    Serial.print(" C");
   
    Serial.print(" Press: ");    
    Serial.print(bmp280.getPressure());
    Serial.print(" hPa"); */

    Serial.println();
}
 
//SSDV FOTOGRAFI FLASHA KAYIT ARDINDAN SSDV FORMATINA DÖNÜŞTÜRÜP FLASHA KAYIT EDİLİP KAYIT EDİLEN DOSYA ÜZERİNDEN GÖNDERİLEN KISIM AYRICA GÜÇ KESİNTİSİ OLURSA KALAN YERDEN DEVAM EDİLİP TAMAMLANANA KADAR GÖNDERİM YAPILIYOR.
int iread(uint8_t* buffer,int numBytes,uint16_t cbIndex ){

File file2 = SPIFFS.open(FILE_PHOTO, FILE_READ);
 
 // have we reached past end of imagebuffer
  if((cbIndex + numBytes ) < lenn ){    
    bufSize = numBytes;  
  }
  else{  
    bufSize = lenn - cbIndex; // fb->len
    indexson=5; // işlemlerini sonlandırmak için    
}

// clear the dest buffer
memset(buffer,0,numBytes);

file2.seek(cbIndex,SeekSet); // index okuması için flash hafızadaki resim datasının bufferındaki cursoru ayarlıyoruz.

file2.read(buffer,bufSize);

if(indexson==5){
indexson=2;
file2.close();    
}
  return bufSize;
}

int process_ssdv(){
 
  // önceden_resim_cekildi_mi?
  EEPROM.write(7,113); // Fotograf çekildiğinde bu bu fonksiyona girip flasha fotograf cekildiğini anlamamız için bu değeri girdik reset alırsak bu değeri kontrol edip devam ediyoruz.
  EEPROM.commit();

  // initialise ssdv config structure
  ssdv_enc_init(&ssdv, SSDV_TYPE_NOFEC, (char *)callsign, imageID++, ssdvQuality);
  // set the output lora packet buffer for ssdv where the final ssdv packet will end up
  ssdv_enc_set_buffer(&ssdv, flashBuff);

  File filessdvv = SPIFFS.open(SSDVDATA, FILE_APPEND);
 
   while(1){
 
    while((c = ssdv_enc_get_packet(&ssdv)) == SSDV_FEED_ME)
    {          
      indexx += iread(imgBuff, IMG_BUFF_SIZE,indexx);
      Serial.print("index = ");
      Serial.println(indexx);    
      ssdv_enc_feed(&ssdv, imgBuff, IMG_BUFF_SIZE);    
    }
   
    if(indexx>640) indexsayac++;
   
    if((c == SSDV_EOI))
    {
     filessdvv.close();  
     EEPROM.write(10,indexsayac);
     EEPROM.commit();
     Serial.println("ssdv EOI");  
     Photo_Flash_To_LoRa();
     break;    
    }
    else if(c != SSDV_OK)
    {
        Serial.println("ssdv Error");
        break;
    }
   
      if (!filessdvv) {
        Serial.println("Failed to open file in append mode");
      }
      else {    
 
      filessdvv.seek(cur,SeekSet);  
      filessdvv.write(flashBuff,256);
      cur+=256; }
}

}

void eksikfoto_tamamla(){ // Enerji kesilirse veya kart reset alırsa ve resim çekilip flasha kayıt edilmişse bu fonksiyondan devam ediyoruz.

byte h = EEPROM.read(0);
byte p = EEPROM.read(1);

byte t = EEPROM.read(8);
byte s = EEPROM.read(9);
cur3 = t + (s<<8);
   
indexsayac=EEPROM.read(10);  
imageID=EEPROM.read(2); // Kaçıncı resmi aktarıyoruz.
ssdvPacketCount_2=EEPROM.read(3); // En son kacıncı 3lü paket attık? 20. mi 25. mi ?

Serial.print("Güç sistemine ikinci giriş başarılı şu an flash hafızadaki konum:");
Serial.println(cur3); // Flashtan en son hangi kısımda kaldık kontrol ediyoruz.
   
File filessdv4 = SPIFFS.open(SSDVDATA, FILE_READ);  
       
while(1){   
/*
   if(lmh_join_status_get() != LMH_SET){
      i=0;
      Serial.println("Did not join network, skip sending frame"); 
      delay(10000); //LoRaWAN Initialize için bekliyoruz...
      save_photo();// Bazen test ederken reset attığımızda lorawana bağlanmadı tekrar bağlanmasını bekledikten sonra aynı resim datasını iletmesi için bu fonksiyonu tekrar cağırdık.
    }*/

   if(millis()- last_packet < TX_INTERVAL){ 
   
   m_lora_app_data.port = LORAWAN_APP_PORT;
   
   if (!filessdv4){
        Serial.println("Failed to open file in reading mode");
    }
   else{
   
   memset(yeniBuff,0,256);   // önceden data olabilir bufferı doldurmadan temizlemek iyidir :)  
   filessdv4.seek(cur3,SeekSet); // cursor için önemli nokta cursor x.kısımda olsun oradaki değeri okuyarak başlıyor kursor 0 dan 256 kadar toplam 256 data okuyor 256.yı okumuyor! Ardından 256 yapıp oradan devam ediyoruz.

   Serial.print("Read Cursor Pozisyonu=");
   Serial.println(filessdv4.position());  // Cursor pozisyonunu kontrol ediyoruz.
   filessdv4.read(yeniBuff,256);
   
   byte curdusukbyte = lowByte(cur3); // cursor 2 byte olduğu için eppromda iki ayrı yerde tutup birleştiriyoruz.
   byte curyuksekbyte = highByte(cur3);
   EEPROM.write(8,curdusukbyte);
   EEPROM.write(9,curyuksekbyte);
   EEPROM.commit(); // Commit olmazsa epproma yazmaz.  
  }
   
    for(uint16_t i = 0; i < 256; i++) {
      yeniBuff[i] = yeniBuff[i+1];    } // LoRa max 255 byte gönderebiliyoruz o yüzden 1 byte eksik gönderip gatewayde ekleyeceğiz.
   
   Serial.print(" packet sent");
   Serial.println(ssdvPacketCount_2);  
   
   EEPROM.write(2,imageID); // Resim görüntüsü tek sefer yazmak için üst tarafa aldık.
   EEPROM.write(3,ssdvPacketCount_2);
   EEPROM.commit();

   // Eğer kart reset alır veya güç problemi olursa  hangi parcanın kacıncısındayız onun kontrol ediyoruz. 25 paketin 1. parçası mı? 2.parçası mı? 3. parçası mı?
   if(EEPROM.read(4)!=(PaketSayi-1))     devamm=EEPROM.read(4)+1;
   else devamm=0;

   lmh_error_status error;
   for(uint8_t a=devamm; a<PaketSayi;a++){
   switch(PaketSayi)
    {
     case 2:{// paketi eğer 2'ye bölerek gönderme yapıyorsak burası çalışacaktır.               
                 Serial.println("Data 2 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 134);//buffer temizle
                   
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount_2;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 2; 
 
                 for(int z=0;z<128;z++)m_lora_app_data.buffer[(5+z)]=yeniBuff[z+(a*128)]; // 1 ssdvbuffdan 1 adet fazla data okuduk fakat gateway tarafında onu kullanmayacağız.!!
                 
                 m_lora_app_data.buffsize = 133;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); // datanın gönderme kısmı  
     }break;
      
     case 3:{// paketi eğer 3'ye bölerek gönderme yapıyorsak burası çalışacaktır.
                 Serial.println("Data 3 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 91);//buffer temizle
                 
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount_2;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 3; 
 
                 for(int z=0;z<85;z++)m_lora_app_data.buffer[(5+z)]=yeniBuff[z+(a*85)];
                 
                 m_lora_app_data.buffsize = 90;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); // datanın gönderme kısmı           
     }break;
                
     case 6:{// paketi eğer 6'ya bölerek gönderme yapıyorsak burası çalışacaktır.     

                 Serial.println("Data 6 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 49);//buffer temizle
                 
                 if(a==(PaketSayi-1)) {      
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount_2;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 6;  
    
                 for(int z=0;z<40;z++)m_lora_app_data.buffer[(5+z)]=yeniBuff[z+(a*43)]; //son kalan data gönderilimi yapılıyor.

                 m_lora_app_data.buffer[45]= 0;
                 m_lora_app_data.buffer[46]= 0;
                 }else{ 
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount_2;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 6;
          
                 for(int z=0;z<43;z++)m_lora_app_data.buffer[(5+z)]=yeniBuff[z+(a*43)];     
                 }
                 
                 m_lora_app_data.buffsize = 48;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); // datanın gönderme kısmı                     
     }break;
}    

    if (error == LMH_SUCCESS)
    {
      // the packet was successfully transmitted    
      EEPROM.write(4,a);
      EEPROM.commit();  //Gönderme işlemi başarılıysa giden parcayı epproma yazıyoruz.
      Serial.println(EEPROM.read(4));
     
      if(a==(PaketSayi-1)){
        ssdvPacketCount_2++;  
        EEPROM.write(3,ssdvPacketCount_2);

        cur3+=256;

        byte curdusukkbyte = lowByte(cur3);
        byte curyuksekkbyte = highByte(cur3);
        EEPROM.write(8,curdusukkbyte);
        EEPROM.write(9,curyuksekkbyte);
        EEPROM.commit();  
      }
     Serial.print("Data: ");
     Serial.println((char*)m_lora_app_data.buffer);
     Serial.print("Size: ");
     Serial.println(m_lora_app_data.buffsize);
     Serial.print("Port: ");
     Serial.println(m_lora_app_data.port);
     
     Serial.println(F("success!"));
     Serial.printf("lmh_send result %d\n", error);    
    }

   delay(60000); //EU868 DR3-SF9 90BYTE İÇİN HER PAKET ARASI BEKLENECEK SÜRE!!! BU SÜRE BÖLGELERE VE DATABOYUTUNA GÖRE DEĞİŞMEKTEDİR.BÖLGELERE GÖRE PARAMETRİK YAPABİLİRİZ.
                 //ÖRN:EU868 DR3-SF9 48BYTE İÇİN HER PAKET ARASI 40SANİYE BEKLENİYOR
    }
     
   Serial.print(EEPROM.read(3));      
   memset(yeniBuff,0,256);
 
   if(ssdvPacketCount_2==(indexsayac-1)){ //Resim toplam data uzunluğuna geldik mi veya geçtik mi?  
   
      for(int i=0; i<EEPROM_SIZE; i++)EEPROM.write(i,0); // Tüm eppromu temizliyoruz.
      EEPROM.commit();  
      SPIFFS.remove("/cam_photo4.txt"); //SSDV den gelen dataları kayıt ettiğimiz dosya bir öncekini uçuş başlamadan önce sileceğiz. Yeni bir dosya ismi yazarak yeni ssdv datalarını oraya kayıt edeceğiz.
      filessdv4.close(); // Flashtaki acıp okuduğumuz dosyayı kapatıyoruz.
      Serial.print("SSDV EOI SONN !!!");          
      break;
    }

  }else  break;
   

} 
}

void Photo_Flash_To_LoRa(){
      
    EEPROM.write(2,imageID);
    EEPROM.commit();    
   
    File filessdv2 = SPIFFS.open(SSDVDATA, FILE_READ);  
    if (!filessdv2){
        Serial.println("Failed to open file in read mode");
    }    
   
   while(1){ // SSDV kendi datasını oluşturup flasha yazmayı bitirdikten sonra kod buraya gelip flashtan ssdv datalarını okuyor.ve LoRa üzerinden gönderim yapıyor.      
    
/*   if(lmh_join_status_get() != LMH_SET){
      i=0;
      Serial.println("Did not join network, skip sending frame");   
      delay(10000); //LoRaWAN Initialize için bekliyoruz...   
      save_photo(); // Bazen test ederken reset attığımızda lorawana bağlanmadı tekrar bağlanmasını bekledikten sonra aynı resim datasını iletmesi için bu fonksiyonu tekrar cağırdık.
    }*/
   if(millis()- last_packet < TX_INTERVAL){  
    
   m_lora_app_data.port = LORAWAN_APP_PORT;
   
   Serial.print(" packet sent");
   Serial.println(ssdvPacketCount);
   
   filessdv2.seek(cur2,SeekSet);  
   Serial.print("Read Cursor Pozisyonu=");
   Serial.println(filessdv2.position());  
   filessdv2.read(ssdvBuff,256);
   
   byte currdusukbyte = lowByte(cur2);
   byte curryuksekbyte = highByte(cur2);
   EEPROM.write(8,currdusukbyte);
   EEPROM.write(9,curryuksekbyte);
   EEPROM.commit();
   
   for(uint16_t i = 0; i < 256; i++) {
      ssdvBuff[i] = ssdvBuff[i+1]; } //LoRa buffer üzerinden max 255byte gönderebildiğimiz için 1 byte eksik gönderiyoruz. Gateway onu en sonda ekleme yapıyor.

   EEPROM.write(3,ssdvPacketCount);
   EEPROM.commit();

   lmh_error_status error;   
   for(uint8_t a=0; a<PaketSayi;a++){

   switch(PaketSayi)
   {
     case 2:{ // paketi eğer 2'ye bölerek gönderme yapıyorsak burası çalışacaktır.               
                 Serial.println("Data 2 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 134);//buffer temizle
                   
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 2; 
 
                 for(int z=0;z<128;z++)m_lora_app_data.buffer[(5+z)]=ssdvBuff[z+(a*128)]; // 1 ssdvbuffdan 1 adet fazla data okuduk fakat gateway tarafında onu kullanmayacağız.!!
                 
                 m_lora_app_data.buffsize = 133;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); //gönderme kısmı                 
     }break;
      
     case 3:{ // paketi eğer 3'ye bölerek gönderme yapıyorsak burası çalışacaktır.
                 Serial.println("Data 3 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 91);//buffer temizle
                 
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 3; 
 
                 for(int z=0;z<85;z++)m_lora_app_data.buffer[(5+z)]=ssdvBuff[z+(a*85)];
                 
                 m_lora_app_data.buffsize = 90;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); //gönderme kısmı                   
     }break;
                
     case 6:{// paketi eğer 6'ya bölerek gönderme yapıyorsak burası çalışacaktır.     

                 Serial.println("Data 6 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 49);//buffer temizle
                 
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 6;
          
                 for(int z=0;z<43;z++)m_lora_app_data.buffer[(5+z)]=ssdvBuff[z+(a*43)];     
                 
                 m_lora_app_data.buffsize = 48;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); //gönderme kısmı                     
     }break;
}
      
  if (error == LMH_SUCCESS)
  {
    // the packet was successfully transmitted  
    EEPROM.write(4,a);  
    EEPROM.commit();
    Serial.println(EEPROM.read(4));
       
    if(a==(PaketSayi-1)){
    ssdvPacketCount++;
    EEPROM.write(3,ssdvPacketCount);
    cur2+=256;
         
    byte ccurrdusukbyte = lowByte(cur2);
    byte ccurryuksekbyte = highByte(cur2);
    EEPROM.write(8,ccurrdusukbyte);
    EEPROM.write(9,ccurryuksekbyte);
    EEPROM.commit();  
  }
   Serial.print("Data: ");
   Serial.println((char*)m_lora_app_data.buffer);
   Serial.print("Size: ");
   Serial.println(m_lora_app_data.buffsize);
   Serial.print("Port: ");
   Serial.println(m_lora_app_data.port);
   
   
   Serial.println(F("success!"));
   Serial.printf("lmh_send result %d\n", error);  
  }
   delay(60000); //EU868 DR3-SF9 90BYTE İÇİN HER PAKET ARASI BEKLENECEK SÜRE!!! BU SÜRE BÖLGELERE VE DATABOYUTUNA GÖRE DEĞİŞMEKTEDİR.BÖLGELERE GÖRE PARAMETRİK YAPABİLİRİZ.
                 //ÖRN:EU868 DR3-SF9 48BYTE İÇİN HER PAKET ARASI 40SANİYE BEKLENİYOR
  }              
   
   Serial.print(EEPROM.read(3));  // Epproma bakıyoruz kac paket gönderdik.
   memset(ssdvBuff,0,256);

   if(ssdvPacketCount==(indexsayac-1)){ //Resim toplam data uzunluğuna geldik mi veya geçtik mi?  
       
      for(int i=0; i<EEPROM_SIZE; i++)EEPROM.write(i,0);
      EEPROM.commit();  
      SPIFFS.remove("/cam_photo4.txt"); // SSDV den gelen dataları kayıt ettiğimiz dosya bir öncekini uçuş başlamadan önce sileceğiz. Yeni bir dosya ismi yazarak yeni ssdv datalarını oraya kayıt edeceğiz.
      filessdv2.close();
      Serial.print("!!!!!SSDV BİTTİ!!!");      
      break;
   }
 }else  break;
    
} 
}



/*************************************************************************************************************************************************/
// Check if photo capture was successful
bool checkPhoto( fs::FS &fs ) {
  File f_pic = fs.open( FILE_PHOTO );
  unsigned int pic_sz = f_pic.size();
  return ( pic_sz > 100 );
}

/*************************************************************************************************************************************************/
void save_photo()
{
 
 SPIFFS.remove("/cam_photo3.txt"); //Test sırasında yarıda kesilince gönderim bitmediğinden dosya yapısı acık kalıyor!!
 
Serial.print("CHECK : ");
Serial.println( EEPROM.read(7)); // Güç kesintisinden dolayı devam eden resim mi yoksa yeni resim mi eppromdan kontrol ediyoruz.Devam eden resimse kodu farklı devam ediyor.

if(EEPROM.read(7)==113){
     
   eksikfoto_tamamla();  
}

else{
 
  camera_fb_t *fb = NULL;  
  bool ok=0; // Boolean indicating if the picture has been taken correctly
  do {
  Serial.print("Taking picture: ");
  fb = esp_camera_fb_get();
 
  if (!fb) {
      Serial.println("Camera capture failed");
      return;
    }
   
    // Photo file name
    Serial.printf("Picture file name: %s\n", FILE_PHOTO);
    File file = SPIFFS.open(FILE_PHOTO, FILE_WRITE);
    
    // Insert the data in the photo file   
    if (!file) {
      Serial.println("Failed to open file in writing mode");
    }
    else {
      file.write(fb->buf, fb->len); // payload (image), payload length      
      res_len= (uint16_t)fb->len;
      Serial.print("The picture has been saved in ");
      Serial.print(FILE_PHOTO);
      Serial.print(" - Size: ");
      Serial.print(file.size());
      Serial.println(" bytes");    }

      byte resdusukbyte = lowByte(res_len);
      byte resyuksekbyte = highByte(res_len);
      EEPROM.write(0,resdusukbyte);
      EEPROM.write(1,resyuksekbyte);
      EEPROM.commit();
      byte d = EEPROM.read(0);
      byte y = EEPROM.read(1);
      lenn = d + (y<<8); // bu kısım iread fonksiyonunun resmi flash üzerinden okuyabilmesi için önemlidir.Kesinlikle kaldırmayalım!!!!
   
      file.close();  
      esp_camera_fb_return(fb);

      // check if file has been correctly saved in SPIFFS
      ok = checkPhoto(SPIFFS);
      }while ( !ok );
      process_ssdv();  
 }
}

/*************************************************************************************************************************************************/

void setupUBloxDynamicModel() {
    // If we are going to change the dynamic platform model, let's do it here.
    // Possible values are:
    // PORTABLE, STATIONARY, PEDESTRIAN, AUTOMOTIVE, SEA, AIRBORNE1g, AIRBORNE2g, AIRBORNE4g, WRIST, BIKE
    //DYN_MODEL_AIRBORNE4g model increases ublox max. altitude limit from 12.000 meters to 50.000 meters.
    if (myGPS.setDynamicModel(DYN_MODEL_AIRBORNE4g) == false) // Set the dynamic model to DYN_MODEL_AIRBORNE4g
    {
      Serial.println(F("***!!! Warning: setDynamicModel failed !!!***"));
    }
    else
    {
      ublox_high_alt_mode_enabled = true;
      #if defined(DEVMODE)
        Serial.print(F("Dynamic platform model changed successfully! : "));
        Serial.println(myGPS.getDynamicModel());
      #endif  
    }
}
 
// SENSÖR VERİLERİNİ BYTE CEVİRME İŞLEMLERİNİ YAPTIĞIMIZ ALAN BUFFERA BYTE-BYTE YAZMAK İÇİN YAPMAK ZORUNDAYIZ.

void floatAddByte(float data,int *count,uint8_t *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void intAddByte(int data,int *count,uint8_t *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,uint8_t *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void uint16AddByte(uint16_t data,int *count,uint8_t *array){
  for(int i=0;i<2;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void byteAddByte(byte data,int *count,uint8_t *array){
    array[*count]=data;
    (*count)++;
}
// SENSÖR VERİLERİNİ BYTE CEVİRME İŞLEMLERİNİ YAPTIĞIMIZ ALAN BUFFERA BYTE-BYTE YAZMAK İÇİN YAPMAK ZORUNDAYIZ. 
