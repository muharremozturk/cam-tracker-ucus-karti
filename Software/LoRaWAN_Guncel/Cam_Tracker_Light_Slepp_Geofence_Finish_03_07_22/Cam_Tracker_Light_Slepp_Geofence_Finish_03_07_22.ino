#include "SparkFun_Ublox_Arduino_Library.h"
#include "MCP3X21.h"
#include "driver/rtc_io.h"
#include "Seeed_BMP280.h"
#include "PCA9539.h"
#include "soc/soc.h" //disable brownout problems for camera
#include "soc/rtc_cntl_reg.h" //disable brownout problems for camera
#include "ssdv.h"

#include <FS.h>
#include <SPIFFS.h>
#include <Arduino.h>
#include <esp_camera.h>
#include <SX126x-Arduino.h>
#include <LoRaWan-Arduino.h>
#include <stdlib.h>
#include <EEPROM.h>
#include <SPI.h>
#include <Wire.h>
#include <esp_sleep.h>
#include <CamTrackerGeofence.h>


#define EEPROM_SIZE 15
#define LORAWAN_APP_DATA_BUFF_SIZE 256            /**< Size of the data to be transmitted. */
#define LORAWAN_APP_TX_DUTYCYCLE 30000            /**< Defines the application data transmission duty cycle. 30s, value in [ms]. */
#define APP_TX_DUTYCYCLE_RND 1000                 /**< Defines a random delay for application data transmission duty cycle. 1s, value in [ms]. */
#define JOINREQ_NBTRIALS 3                        /**< Number of trials for the join request. */
#define LORAWAN_DEFAULT_DATARATE  DR_5            /*LoRaMac datarates definition, from DR_0 to DR_5*/ /*EU868 DR0-1-2 :51 BYTE ,DR3: 115 BYTE, DR4-5:242 BYTE*/
#define LORAWAN_DEFAULT_TX_POWER  TX_POWER_0      /*LoRaMac tx power definition, from TX_POWER_0 to TX_POWER_15*/
#define mS_TO_S_FACTOR 1000                       /* Conversion factor for mili seconds to seconds for Sleep Function */


#define GpsOFF    ioport.digitalWrite(pa6, HIGH);   //GPS close
#define GpsON     ioport.digitalWrite(pa6, LOW);    //GPS open      
#define CameraON  ioport.digitalWrite(pb2, LOW);    //Camera close
#define CameraOFF ioport.digitalWrite(pb2, HIGH);   //Camera open 
#define LedON     ioport.digitalWrite(pa0, HIGH);   //Led open 
#define LedOFF    ioport.digitalWrite(pa0, LOW);    //Led close 
                             
#define PWDN_GPIO_NUM     -1
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

//*****************************SSDV DEFINITIONS**********************************************************//
#define IMG_BUFF_SIZE               128         // size of the buffer feeding SSDV process
#define LORA_BUFFER                 256
#define FILE_PHOTO "/photo7.jpg"              //Foto name
#define SSDVDATA "/denek24.txt"    // Resim datalarını kayıt ettiğimiz dosya uçuş bitince temizleniyor.!!!!FAKAT UÇUŞ BİTMEDEN KARTA KOD YÜKLENİRSE ÜZERİNE TEKRAR YAZINCA PROBLEM OLABİLİYOR.TEST EDİLDİ.YARIM KALAN TESTLERDE BURAYA YENİ DOSYA İSMİ YAZILMALI!!! 

TaskHandle_t Task1; //For Dual Core System

DeviceClass_t CurrentClass = CLASS_A;          /* Class definition*/
LoRaMacRegion_t CurrentRegion = LORAMAC_REGION_EU868;    /* Region:EU868*/
lmh_confirm CurrentConfirm = LMH_UNCONFIRMED_MSG;  


//*****************************ESP32 - SX126x pin configuration**************************************//
hw_config hwConfig;

int PIN_LORA_RESET = 32;   // LORA RESET
int PIN_LORA_NSS = 15;   // LORA SPI CS
int PIN_LORA_SCLK = 14;  // LORA SPI CLK
int PIN_LORA_MISO = 12;  // LORA SPI MISO
int PIN_LORA_DIO_1 = 4; // LORA DIO_1
int PIN_LORA_BUSY = 33;  // LORA SPI BUSY
int PIN_LORA_MOSI = 13;  // LORA SPI MOSI
int RADIO_TXEN = -1;   //2 LORA ANTENNA TX ENABLE ,
int RADIO_RXEN = -1;   //25 LORA ANTENNA RX ENABLE ,


//***************************** UPDATE HERE WITH YOUR DEVICE KEYS **************************************/

uint8_t nodeDeviceEUI[8] = {0x60, 0x81, 0xF9, 0x9A, 0xC2, 0xA7, 0xFD, 0x67}; // MUST BE MSB 

uint8_t nodeAppEUI[8] = {0x60, 0x81, 0xF9, 0xE5, 0xE8, 0xDE, 0x6B, 0xA2}; // MUST BE MSB  

uint8_t nodeAppKey[16] = {0xAF, 0xB2, 0xA8, 0xB9, 0x6B, 0x5E, 0x1A, 0x0D, 0x5D, 0x6F, 0xD0, 0x52, 0xA6, 0xA1, 0x1C, 0x8F}; // MUST BE MSB  


//*****************************LORAWAN FUNCTIONS**************************************/

/** LoRaWAN callback when join network finished */
static void lorawan_has_joined_handler(void);
/** LoRaWAN callback when join network failed */
static void lorawan_join_fail_handler(void);
/** LoRaWAN callback when data arrived */
static void lorawan_rx_handler(lmh_app_data_t *app_data);
/** LoRaWAN callback after class change request finished */
static void lorawan_confirm_class_handler(DeviceClass_t Class);
/** LoRaWAN callback after class change request finished */
static void lorawan_unconfirm_tx_finished(void);
/** LoRaWAN callback after class change request finished */
static void lorawan_confirm_tx_finished(bool result);
/** LoRaWAN Function to send a package */
static void send_lora_frame(void);
static uint32_t timers_init(void);

//*****************************OTHER FUNCTIONS**************************************/
void ReadVoltage(void);
void setupBMP(void);
void setupGPS(void);
void save_photo(void);
void Photo_Flash_To_LoRa(void);
void eksikfoto_tamamla(void);
void cameraconfig(void);
void CamTrackerSleep(uint32_t sleeptime);

static uint8_t m_lora_app_data_buffer[LORAWAN_APP_DATA_BUFF_SIZE];        ///< Lora user application data buffer.
static lmh_app_data_t m_lora_app_data = {m_lora_app_data_buffer, 0, 0, 0, 0}; ///< Lora user application data structure.

/**@brief Structure containing LoRaWan parameters, needed for lmh_init()
 */
static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,LORAWAN_DEFAULT_DATARATE, LORAWAN_PUBLIC_NETWORK, JOINREQ_NBTRIALS, LORAWAN_DEFAULT_TX_POWER}; 


/**@brief Structure containing LoRaWan callback functions, needed for lmh_init()
*/


static lmh_callback_t lora_callbacks = {BoardGetBatteryLevel, BoardGetUniqueId, BoardGetRandomSeed,
                    lorawan_rx_handler, lorawan_has_joined_handler, lorawan_confirm_class_handler, lorawan_join_fail_handler,
                    lorawan_unconfirm_tx_finished, lorawan_confirm_tx_finished};


//************************** LORAWAN SETTINGS ************************************//

const unsigned TX_INTERVAL = 600000;  //10dakikada 1 sensör datasını atacağız.


//************************** GEOFENCE SETTINGS ************************************
uint8_t Lorawan_Geofence_no_tx  = 0; //do not change this
uint8_t Lorawan_Geofence_region_code = _REGCODE_UNDEF; //do not change this
uint8_t Lorawan_Geofence_special_region_code = _REGCODE_UNDEF; //do not change this
uint8_t lastLoRaWANRegion = _REGCODE_UNDEF; //do not change this

//GEOFENCE BÖLGE NUMARALARI: EU868->5,AU915->1,AS923->0,KR920->6,US915->8,RU864->12,IN865->7,CN470->2  bu numaralar kütüphanede basicmac!e göre tanımlanmış geofence kısmında güncelledik. 

boolean OTAAJoinStatus = false; //do not change this.
uint32_t last_packet = 0; //do not change this. Timestamp of last packet sent.

//#define DEVMODE // Development mode. Uncomment to enable for debugging.

uint8_t measurementSystem = 0; //0 for metric (meters, km, Celcius, etc.), 1 for imperial (feet, mile, Fahrenheit,etc.)

 
//********************************* Initialize Scheduler and Timer Settings ******************************
uint32_t err_code;      
int8_t  lorawan_baslat=0; // LoRaWAN kodunun ikinci coreda tek sefer çalışması için kullanıyoruz.  
boolean lorawan_tx_finish =false; // do not change this because we understand to finish tx lorawan  
int counter=0;  // Sensör verilerini byte byte ayırırken fonksiyonda kullanıyoruz.          
//************************** uBlox GPS  Settings *********************************************************
long lastTime = 0; //Simple local timer. Limits amount if I2C traffic to Ublox module.
boolean gpsFix=false; //do not change this.
boolean ublox_high_alt_mode_enabled = false; //do not change this.
boolean gpsBMPSetup=false; //do not change this.

//********************************* Power Settings ********************************************************
int   battWait=60;    //seconds sleep if super capacitors/batteries are below battMin (important if power source is solar panel)
float battMin=3.5;    // min Volts to TX. (Works with 3.3V too but 3.5V is safer)
float gpsMinVolt=4.5; //min Volts for GPS to wake up. (important if power source is solar panel) //do not change this
//************************** Camera  Settings *************************************************************
boolean CameraSetup=false; //do not change this.
uint8_t catchEror=0;     //kamerada eror oluşursa sayım yapılıyor 15 gecerse sistemi kitlemesin özellikle ikinci resim çekilmesi durumunda
//************************** BMP280  Settings *************************************************************
float    bmp_pressure;
float    bmp_temperature ;
float    bmp_altitude ;
uint32_t MSL = 102009; // Mean Sea Level in Pa
//********************************* Misc Settings ****************************************************************************
int txCount = 1;
const uint8_t address = 0x4D;
const uint16_t ref_voltage = 3300;  // in mV
float voltage = 0;

//******************************************** SSDV KODDAKİ TANIMLAMALARI ****************************************************

static esp_err_t cam_err;
uint16_t res_len,lenn;
int8_t indexson;

//******************************************** RESİM DATALARINI KAÇ PARÇA OLARAK GÖNDERECEĞİZ ********************************
uint8_t PaketSayi=2; // Resim datalarının kaç parça olarak göndermek istiyoruz 2-3-6 ? Ayrıca burası değişitirilince DataRate(LORAWAN_DEFAULT_DATARATE) ve region time değiştirmeyi unutma!!!

float indexsayac=0;  //SSDV'nin oluşturmuş olduğu index sayısını tutuyoruz. Her index 256byte fakat sıkıştırma yaptığı için bazen 3 tane 256 byte birleştirip 1 tane index yapabilmektedir.
int bufSize = 0;
int cur=0; //for process ssdv function
uint16_t indexx=0,c=0; //for process ssdv function
int8_t ssdvPacketCount_2=0,devamm=0;  // ssdvPacketCount_2 gönderdiğimiz 3lü paket sayısını tuttuğumuz değişken , devamm ise enerjiden paket durdurursak kaldığımız 3 lü paketten devam etmemizi sağlıyor. 25.paketin 2.datası gönderildi ax=3 olur son paketi gönderir.  
int cur3=0; // cur3 flash hafıza içindeki dosyanın içinde nerede kalacağını tuttuğumuz değişkendir.
int8_t ssdvPacketCount=0;  //for flashtolora functıon
int cur2=0; //for flashtolora functıon
uint32_t region_wait_time=24100; // SF7 iki resim paket arası beklenen süre air time calculator %1 duty cycle default EU868 ayarlandı.  2li paket region_wait_time->24.1sn , 3lü paket region_wait_time->17.4sn, 6lı paket region_wait_time->11.3sn
uint32_t sensor_wait_time=10300; // SF7 iki sensöt paket arası beklenen süre air time calculator %1 duty cycle default 10.3sn her bölgede aynı.

uint8_t ssdvQuality = 5;                 // 0-7 corresponding to JPEG quality: 13, 18, 29, 43, 50, 71, 86 and 100

static const char callsign[] = "TA2MHR";        // maximum of 6 characters
ssdv_t ssdv;

uint8_t imgBuff[IMG_BUFF_SIZE];
uint8_t flashBuff[LORA_BUFFER]; // Do not change
uint8_t ssdvBuff[LORA_BUFFER]; //Do not change
uint8_t yeniBuff[LORA_BUFFER]; //Do not change

int8_t imageID = 8;  // Aynı uui ile aynı image_ıd bir kez resim basabiliriz test sırasında backend kullanılacaksa dikkat edilmeli.

BMP280 bmp280; //temp and pressure sensor
MCP3021 mcp3021(address); //ı2c analog battery measure
SFE_UBLOX_GPS myGPS; // gps
PCA9539 ioport(0x74); // Base address starts at 0x74 for A0 = L and A1 = L

/**@brief Setup function
 */
void setup()
{  
//  setCpuFrequencyMhz(20); // Save power by going slowly
  delay(5000);//do not change this
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
 
  // Initialize Serial for debug output
  Serial.begin(115200);

  Serial.println("=====================================");
  Serial.println("SX126x LoRaWan test");
  Serial.println("=====================================");

  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    ESP.restart();
  }
  else {
    delay(500);
    Serial.println("SPIFFS mounted successfully");
  }  
   
  // Define the HW configuration between MCU and SX126x
  hwConfig.CHIP_TYPE = SX1262_CHIP;     // Example uses an eByte E22 module with an SX1262
  hwConfig.PIN_LORA_RESET = PIN_LORA_RESET; // LORA RESET
  hwConfig.PIN_LORA_NSS = PIN_LORA_NSS;  // LORA SPI CS
  hwConfig.PIN_LORA_SCLK = PIN_LORA_SCLK;   // LORA SPI CLK
  hwConfig.PIN_LORA_MISO = PIN_LORA_MISO;   // LORA SPI MISO
  hwConfig.PIN_LORA_DIO_1 = PIN_LORA_DIO_1; // LORA DIO_1
  hwConfig.PIN_LORA_BUSY = PIN_LORA_BUSY;   // LORA SPI BUSY
  hwConfig.PIN_LORA_MOSI = PIN_LORA_MOSI;   // LORA SPI MOSI
  hwConfig.RADIO_TXEN = -1;         // LORA ANTENNA TX ENABLE
  hwConfig.RADIO_RXEN = -1;         // LORA ANTENNA RX ENABLE
  hwConfig.USE_DIO2_ANT_SWITCH = true;    // Example uses an CircuitRocks Alora RFM1262 which uses DIO2 pins as antenna control
  hwConfig.USE_DIO3_TCXO = true;        // Example uses an CircuitRocks Alora RFM1262 which uses DIO3 to control oscillator voltage
  hwConfig.USE_DIO3_ANT_SWITCH = false;  // Only Insight ISP4520 module uses DIO3 as antenna control

  Wire.begin(26,27);
 
  ioport.pinMode(pa6, OUTPUT); // GPS open and close port
  GpsON;
 
  ioport.pinMode(pb2, OUTPUT); // CAMERA open and close port
  CameraOFF;
       
  ioport.pinMode(pa0, OUTPUT); // LED open and close port
  LedOFF;  

  mcp3021.init(&Wire); //kamera init yapamama durumu testi için buraya aldık.
   
  if (err_code != 0)
  {
    Serial.printf("timers_init failed - %d\n", err_code);
  }

  // Initialize LoRa chip.
  err_code = lora_hardware_init(hwConfig);
  if (err_code != 0)
  {
    Serial.printf("lora_hardware_init failed - %d\n", err_code);
  }

  // Setup the EUIs and Keys
  lmh_setDevEui(nodeDeviceEUI);
  lmh_setAppEui(nodeAppEUI);
  lmh_setAppKey(nodeAppKey); // Only needed for OOTA registration  
  
  EEPROM.begin(EEPROM_SIZE);  // Begin EEPROM
  
   xTaskCreatePinnedToCore( // For Dual Core System
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    10000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */                  
    delay(100);
}

/**@brief Main loop
 */

void Task1code( void * pvParameters ){
 // Serial.print("Task1 running on core ");
 // Serial.println(xPortGetCoreID());
 
       
  for(;;){ // We can use inside for loop like void loop if you use this , code will be run core 0
    vTaskDelay(3);
   if(lorawan_baslat==5){  // "5" değeri yazdım önemi yok değiştirilebilir 
   // SETUP KISMINDA DEFAULT TANIMLI OLAN EU868 BAŞLAR.
  // to start only once
  Serial.println("=================================================================================================================================");
  
  Serial.println("                                                ...!!LoRaWAN INITIALIZE!!...                                                     ");

  Serial.println("=================================================================================================================================");

  
  static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,LORAWAN_DEFAULT_DATARATE , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; //  lora_param_init = {ADR,DATARATE , LORAWAN_PUBLIC_NETWORK,KATILMA SAYISI, TX ÇIKIŞ GÜÇÜ}
   
  // Initialize LoRaWan
  err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
  if (err_code != 0)
  {
    Serial.printf("lmh_init failed - %d\n", err_code);
  }

  // For some regions we might need to define the sub band the gateway is listening to
  /// \todo This is for Dragino LPS8 gateway. How about other gateways???
  if (!lmh_setSubBandChannels(1))
  {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
  }
   
  lmh_join();

  lorawan_baslat=1;
  }  


  if(Lorawan_Geofence_region_code == _REGCODE_EU868){ 
   
   // LoRaMac datarates definition, from DR_0 to DR_5   EU868 DR 0-1-2 :51 BYTE ,DR3: 115 BYTE, DR4-5:242 BYTE 
   
   PaketSayi=2;  // DR5 (SF7 BW125kHz) SF7 is is better/optimum spreading factor for image datas                   
   CurrentRegion = LORAMAC_REGION_EU868;  // Region:EU868
   region_wait_time =24100; //SF7 iki paket arası beklenecek süre air time calculator %1 duty cycle 24.1s
   
   // to start only once
   Serial.println("=================================================================================================================================");
  
   Serial.println("                                              ...!!LoRaWAN_EU868 INITIALIZE!!...                                                 ");

   Serial.println("=================================================================================================================================");

   static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_5 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; 

   // Initialize LoRaWan
   err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
   if (err_code != 0)
    {
    Serial.printf("lmh_init failed - %d\n", err_code);
    }

   if (!lmh_setSubBandChannels(1))
    {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
    }
   
   lmh_join();
   
  }else if (Lorawan_Geofence_region_code == _REGCODE_RU864) {
  
   //LoRaMac datarates definition, RU864 DR 0-1-2 :51 BYTE ,DR3: 115 BYTE, DR4-5:242 BYTE 
   PaketSayi=2;  //DR5 (SF7 BW125kHz) SF7 is is better/optimum spreading factor for image datas
   CurrentRegion = LORAMAC_REGION_RU864;   // Region:RU864
   region_wait_time =24100; //SF7 iki paket arası beklenecek süre air time calculator %1 duty cycle 24.1s
   
   // to start only once
   Serial.println("=================================================================================================================================");
  
   Serial.println("                                              ...!!LoRaWAN_RU864 INITIALIZE!!...                                                 ");

   Serial.println("=================================================================================================================================");

   static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_5 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; 

   // Initialize LoRaWan
   err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
   if (err_code != 0)
   {
    Serial.printf("lmh_init failed - %d\n", err_code);
   }
    
   if (!lmh_setSubBandChannels(1))
   {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
   }
 
   lmh_join();
   
  }else if (Lorawan_Geofence_region_code == _REGCODE_US915) { 
    //LoRaMac datarates definition, US915 DR 0: 11 BYTE, DR 1 : 53 BYTE , DR 2 :125 BYTE ,DR3: 242 BYTE, DR 4: 242 BYTE , DR 5: N-A
    
    PaketSayi=2; //DR3 (SF7 BW125kHz) SF7 is is better/optimum spreading factor for image datas,DR3 (SF7 BW125kHz)             
    CurrentRegion = LORAMAC_REGION_US915;    // Region:US915
    region_wait_time =24100; //SF7 iki paket arası beklenecek süre air time calculator %1 duty cycle 24.1s
    
    // to start only once
    Serial.println("=================================================================================================================================");
  
    Serial.println("                                              ...!!LoRaWAN_US915 INITIALIZE!!...                                                 ");

    Serial.println("=================================================================================================================================");

    static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_3 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; 

     // Initialize LoRaWan
     err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
     if (err_code != 0)
      {
       Serial.printf("lmh_init failed - %d\n", err_code);
      }

      //TTN and Helium only supports second sub band (channels 8 to 15)
      //so we should force to initiate a join with second sub band channels.

      if (!lmh_setSubBandChannels(2))
      {
       Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
      }
    
      lmh_join();    
      
  }else if (Lorawan_Geofence_region_code == _REGCODE_AU915) { 
    
    //LoRaMac datarates definition, AU915 DR 0-1: N-A ,DR 2 : 11 BYTE ,DR 3: 53 BYTE, DR 4: 125 BYTE, DR 5:242 BYTE  
    PaketSayi=2;    //DR5 (SF7 BW125kHz) SF7 is is better/optimum spreading factor for image datas                   
    CurrentRegion = LORAMAC_REGION_AU915;    // Region:AU915 
    region_wait_time =24100; //SF7 iki paket arası beklenecek süre air time calculator %1 duty cycle 24.1s
      
    // to start only once
    Serial.println("=================================================================================================================================");
  
    Serial.println("                                              ...!!LoRaWAN_AU915 INITIALIZE!!...                                                 ");

    Serial.println("=================================================================================================================================");

    static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_5 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; 
    // Initialize LoRaWan
    err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
    if (err_code != 0)
     {
      Serial.printf("lmh_init failed - %d\n", err_code);
     }
       
    //TTN and Helium only supports second sub band (channels 8 to 15)
    //so we should force to initiate a join with second sub band channels.
    if (!lmh_setSubBandChannels(2))
    {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
    }
    
    lmh_join();
    
  }else if (Lorawan_Geofence_region_code == _REGCODE_KR920) { 
    
    //LoRaMac datarates definition, KR920 DR 0-1-2 :51 BYTE ,DR3: 115 BYTE, DR4-5:242 BYTE
    PaketSayi=2; //DR5 (SF7 BW125kHz) SF7 is is better/optimum spreading factor for image datas                
    CurrentRegion = LORAMAC_REGION_KR920;    // Region:KR920
    region_wait_time =24100; //SF7 iki paket arası beklenecek süre air time calculator %1 duty cycle 24.1s
       
    // to start only once
    Serial.println("=================================================================================================================================");
  
    Serial.println("                                              ...!!LoRaWAN_KR920 INITIALIZE!!...                                                 ");

    Serial.println("=================================================================================================================================");

    static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_5 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0}; 

     // Initialize LoRaWan
    err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
    if (err_code != 0)
    {
     Serial.printf("lmh_init failed - %d\n", err_code);
    }

    if (!lmh_setSubBandChannels(1))
    {
     Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
    }
    
    lmh_join();
    
  }else if (Lorawan_Geofence_region_code == _REGCODE_AS923) { 
    
    //LoRaMac datarates definition, AS923 DR 0-1: N/A, DR 2 : 11 BYTE ,DR3: 53 BYTE, DR 4: 125 BYTE , DR 5: 242 BYTE   
    PaketSayi=2;//DR5 (SF7 BW125kHz) SF7 is is better/optimum spreading factor for image datas                   
    CurrentRegion = LORAMAC_REGION_AS923;    //Region:AS923
    region_wait_time =24100; //SF7 iki paket arası beklenecek süre air time calculator %1 duty cycle 24.1s
    
    // to start only once
    Serial.println("=================================================================================================================================");
  
    Serial.println("                                              ...!!LoRaWAN_AS923 INITIALIZE!!...                                                 ");

    Serial.println("=================================================================================================================================");

    static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,DR_5 , LORAWAN_PUBLIC_NETWORK, 3, TX_POWER_0};  

     // Initialize LoRaWan
    err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  
    if (err_code != 0)
    {
     Serial.printf("lmh_init failed - %d\n", err_code);
    }

    if (!lmh_setSubBandChannels(1))
    {
     Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
    }
   
    lmh_join(); 
   
    } 
  }
}

void loop()
{
  voltage = readBatt();
  
  
  if( lmh_join_status_get() == LMH_SET){
      if(!CameraSetup && EEPROM.read(7)!=106){ //kart reset alırsa buraya girip hata cıkartmaması için
        do{           
           Serial.println(F("OV2640 Camera setup"));    
           cameraconfig(); // Kamera konfigrasyonlarının yapıldığı yer. 
           catchEror++; 
           if(catchEror>15) break; 
          }while(cam_err != ESP_OK);  
          
        //delay(10000);  //akım ölçmek için test amaçlı        
        setupBMP();
        CameraSetup = true; }

   save_photo();   
  }
 
  if (((voltage > battMin) && gpsFix) || ((voltage > gpsMinVolt) && !gpsFix)){ 
    Serial.println("Searching  GPS .......");    
   

  if(!gpsBMPSetup){  
    Serial.println(F("GPS setup"));  
    setupGPS();
    Serial.println(F("Searching for GPS fix..."));
    gpsBMPSetup = true; }
    
  if((millis() - lastTime > 1000)){

  if (/*myGPS.getPVT() && */(myGPS.getFixType() !=0) && (myGPS.getSIV() > 0)) //gerçek ucusta pvt kısmını aktif edeceğiz.
  { 
       
    gpsFix=true;  
    Serial.println("GPS FİXLENDİ."); 
//    delay(10000);  //akım ölçmek için test amaçlı
   
//  checkRegionByLocation();   //burada bölge değiştirme işlemleri de olacak!!!!! UÇUŞ KODU YÜKLENİRKEN BURALAR ACILACAK.
        
    if(lorawan_baslat==0){ // Tek sefer lorawan ve kamera initialize etmnek için bu yapıyı kurduk.
      lorawan_baslat=5; // Core 0 initialize Lorawanı başlatıyoruz!       
      Serial.println("!!!!!LORAWANA BAĞLANILIYOR!!!!!");      
      delay(10000); // Wait for initialize lorawan 10sn 
    }           
  }
  }   
  if((gpsFix) && ( lmh_join_status_get() == LMH_SET) ){
    
   last_packet =millis(); //Belirli dakikada sensör datası göndermek için zamanı ayarlıyoruz.
// Serial.print("Saniye:");      //test amaçlı 
// Serial.println(last_packet);  //test amaçlı 
    
   updateTelemetry(); // GPS Datalarını güncelle 
   send_lora_frame(); // 38 datayı gönderme yaptığımız yer  
   GpsOFF;        // GPS KAPAT!!!
// delay(sensor_wait_time); //EU868 DR5-SF7 38BYTE DATA GÖNDERİLİRKEN,PAKETLER ARASI BEKLENİLEN SÜRE 10.3sn .YER TESTİ İÇİN KULLANIYORUZ. UÇUŞTA BURASI KAPATILACAK.

   while(!lorawan_tx_finish){Serial.print(""); } //seriali kaldırmamız gerekiyor if koşulu yazsam bile çalışmadı while döngüsünden cıkmadı.
   lorawan_tx_finish=false;
   
   CamTrackerSleep(sensor_wait_time); // UÇUŞ KODUNDA AKTİF EDECEĞİZ VE BEKLEME ANLARINDA UYKUYA GEÇİŞ YAPACAĞIZ.
  }    
  
    #if defined(DEVMODE)        
       Serial.print("Telemetry Size: ");
       Serial.print(m_lora_app_data.buffsize);
       Serial.println("bytes");
    #endif    
   
   
   readBatt();
   //need to save power
   if (voltage < 4.5) {
     Serial.println("Batarya dolumu yapılıyor!!!!");
     GpsOFF;
     ublox_high_alt_mode_enabled = false; //gps sleep mode resets high altitude mode.
     delay(500);      
   }

   #if defined(DEVMODE)
      printGPSandSensorData();
   #endif    

  }else{
    Serial.println("else yapısına girdi have a big problemmm!!!!!!!!");
    GpsOFF;      
    ublox_high_alt_mode_enabled = false; //gps sleep mode resets high altitude mode.    
    delay(battWait * 1000); //burada uyutacaksal sleep fonksiyonunu çağırabiliriz.
   
  }
 
}
 

static void lorawan_join_fail_handler(void)
{  
  Serial.println("OTAA joined failed");
  Serial.println("Check LPWAN credentials and if a gateway is in range");
  // Restart Join procedure
  Serial.println("Restart network join request");
  lorawan_baslat=5; // Lorawan tekrardan initiliaze denemesi için 
}

/**@brief LoRa function for handling HasJoined event.
 */
static void lorawan_has_joined_handler(void)
{

#if (OVER_THE_AIR_ACTIVATION != 0)
  Serial.println("Network Has Joined");  
#else
  Serial.println("OVER_THE_AIR_ACTIVATION != 0");
  
#endif

}

/**@brief Function for handling LoRaWan received data from Gateway
 *
 * @param[in] app_data  Pointer to rx data
 */
static void lorawan_rx_handler(lmh_app_data_t *app_data)
{
  Serial.printf("LoRa Packet received on port %d, size:%d, rssi:%d, snr:%d\n",
          app_data->port, app_data->buffsize, app_data->rssi, app_data->snr);

  for (int i = 0; i < app_data->buffsize; i++)
  {
    Serial.printf("%0X ", app_data->buffer[i]);
  }
  Serial.println("");

  switch (app_data->port)
  {
  case 3:
    // Port 3 switches the class
    if (app_data->buffsize == 1)
    {
      switch (app_data->buffer[0])
      {
      case 0:
        lmh_class_request(CLASS_A);
        break;

      case 1:
        lmh_class_request(CLASS_B);
        break;

      case 2:
        lmh_class_request(CLASS_C);
        break;

      default:
        break;
      }
    }
    break;

  case LORAWAN_APP_PORT:
    // YOUR_JOB: Take action on received data
    break;

  default:
    break;
  }
}

/**@brief Function to confirm LORaWan class switch.
 *
 * @param[in] Class  New device class
 */
static void lorawan_confirm_class_handler(DeviceClass_t Class)
{
  Serial.printf("switch to class %c done\n", "ABC"[Class]);

  // Informs the server that switch has occurred ASAP
  m_lora_app_data.buffsize = 0;
  m_lora_app_data.port = LORAWAN_APP_PORT;
  lmh_send(&m_lora_app_data, CurrentConfirm);
}

/**
 * @brief Called after unconfirmed packet was sent
 *
 */
static void lorawan_unconfirm_tx_finished(void)
{
  lorawan_tx_finish=true;  
  Serial.println("Uncomfirmed TX finished");
}

/**
 * @brief Called after confirmed packet was sent
 *
 * @param result Result of sending true = ACK received false = No ACK
 */
static void lorawan_confirm_tx_finished(bool result)
{
  Serial.printf("Comfirmed TX finished with result %s", result ? "ACK" : "NAK");
}

/**@brief Function for sending a LoRa package.
 */
static void send_lora_frame(void)
{
  if (lmh_join_status_get() != LMH_SET)
  {
    //Not joined, try again later
    Serial.println("We are in the Send_Lora_Frame Function");    
    Serial.println("did not join network, skip sending frame");
    return;
  }

  m_lora_app_data.port = LORAWAN_APP_PORT;
   
  m_lora_app_data.buffsize = 38;

  Serial.print("Data: ");
  Serial.println((char *)m_lora_app_data.buffer);
  Serial.print("Size: ");
  Serial.println(m_lora_app_data.buffsize);
  Serial.print("Port: ");
  Serial.println(m_lora_app_data.port);


  lmh_error_status error = lmh_send(&m_lora_app_data, CurrentConfirm); //gönderme kısmı
  if (error == LMH_SUCCESS)
  {
  }
  Serial.printf("lmh_send result %d\n", error);
}


void checkRegionByLocation() {

 float tempLat = myGPS.getLatitude() / 10000000.f; // test için kapatıyoruz sonradan acacağız.
 float tempLong = myGPS.getLongitude() / 10000000.f; // test için kapatıyoruz sonradan acacağız.
 
 Lorawan_Geofence_position(tempLat,tempLong);
/*
 * Geofence kısmını test etmek için kullandık.
  
    if(k==5){
    Lorawan_Geofence_position(-34.45,-49.65); //us915
    Serial.print("k");
    Serial.println(k);
   }else if(k==7){
    Lorawan_Geofence_position(34.18,124.66);//kr920
    Serial.print("k");
    Serial.println(k);
   }else if(k==9){
    Lorawan_Geofence_position(5.00,-60.55);//au915
    Serial.print("k");
    Serial.println(k);
   }else if(k==11){
    Lorawan_Geofence_position(35.09,129.66);//as923
    Serial.print("k");
    Serial.println(k);
   }else if(k==13){
    Lorawan_Geofence_position(56.02,36.80);//ru864
    Serial.print("k");
    Serial.println(k);
   }else if(k==15){
    Lorawan_Geofence_position(41.09,29.11); //eu 868
    Serial.print("k");
    Serial.println(k);
   } */
   
} 


void  ReadVoltage(){
   
    uint16_t result = mcp3021.read();
    result = mcp3021.read();

    Serial.print(F("ADC: "));
    Serial.println(result);

   
    Serial.print(F("mV: "));
    Serial.println(readBatt());

    delay(1000);}
   
float readBatt() {
  float R1 = 56.0; // 56K
  float R2 = 10.0; // 10K

  do {
    for(int i=0;i<10;i++){
      voltage +=mcp3021.read();
    }
    voltage=voltage/10;
    voltage = (voltage * 3.3) / 1024.0;
    voltage = voltage / (R2/(R1+R2));
  } while (voltage > 16.0);
  return voltage ;
}
   

void cameraconfig(){
  CameraON;
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
// 0-8 corresponfing to 320x240, 352x288, 640x480, 800x480, 1024x768, 1280x960, 1600x1200, 2048x1536, 2592x1944
//  FRAMESIZE_QVGA 320x240, FRAMESIZE_VGA 640x480, FRAMESIZE_SVGA 800x600
//  FRAMESIZE_XGA 1024x768, FRAMESIZE_UXGA 1600x1200, FRAMESIZE_QXGA 2048x1536
  config.frame_size = FRAMESIZE_VGA;
  config.jpeg_quality = 10;
  config.fb_count = 1;

  // camera init
  cam_err = esp_camera_init(&config);
  if (cam_err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", cam_err);
    delay(2000);
    return;
  }else Serial.println("Camera..OK");

  delay(2000);//kamera beyaz dengesi vb. ayarları yapmak için zamana ihtiyaç duyuyor  
   
}


void setupGPS() {
 
  if (myGPS.begin() == false) //Connect to the Ublox module using Wire port
  {
    Serial.println(F("Ublox GPS not detected at default I2C address. Please check wiring. Freezing."));
    while (1);
  }

  // do not overload the buffer system from the GPS, disable UART output
  myGPS.setUART1Output(0); //Disable the UART1 port output
  myGPS.setUART2Output(0); //Disable Set the UART2 port output
  myGPS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)

  //myGPS.enableDebugging(); //Enable debug messages over Serial (default)

  myGPS.setNavigationFrequency(2);//Set output to 2 times a second. Max is 10
  byte rate = myGPS.getNavigationFrequency(); //Get the update rate of this module
  Serial.print("Current update rate for GPS: ");
  Serial.println(rate);

  myGPS.saveConfiguration(); //Save the current settings to flash and BBR  
 
}

void setupBMP(){
    if (!bmp280.init()) {
    Serial.println("Device not connected or broken!");}
}

void updateTelemetry() {
 
  float latitude = myGPS.getLatitude() / 10000000.f;
  float longtitude= myGPS.getLongitude() / 10000000.f;
  float heading= myGPS.getHeading() / 100000.f;
  float tempAltitudeLong = 0; //meters or feet
  float tempAltitudeShort = 0; //km or miles
  float tempSpeed = 0; //km or miles
  float tempTemperature = 0; //Celcius or Fahrenheit
  byte  uydu= myGPS.getSIV();
  byte  imza= 0x56; // Sensör dataları için ayrı bir data geldiğini anlamamız için

  float pressure= bmp280.getPressure()/ 100.f;



  
  if(measurementSystem == 0){  //Metric
   
    tempAltitudeLong = myGPS.getAltitude() / 1000.f; //meters
    tempAltitudeShort = tempAltitudeLong / 1000.f; //kilometers  
    tempSpeed = myGPS.getGroundSpeed() * 0.0036f; //km/hour      

    tempTemperature = bmp280.getTemperature(); //Celsius  
   
   } else { //Imperial
   
    tempAltitudeLong = (myGPS.getAltitude() * 3.2808399)  / 1000.f;//feet
    tempAltitudeShort = tempAltitudeLong / 5280.f;//miles      
    tempSpeed = myGPS.getGroundSpeed() *  0.00223694f; //mile/hour    

    tempTemperature =  (bmp280.getTemperature()* 1.8f) + 32; //Fahrenheit  
  }    
   
  memset(m_lora_app_data.buffer, 0, 38);
 
  //latitude,longtitude,altitude,speed,course,sattelite,battery,temp,pressure
  
  byteAddByte(imza,&counter,m_lora_app_data.buffer);// Kamera datası değil GPS ve BMP datasını olduğunu anlamamız için attığımız farklı imza 0x86
  floatAddByte(latitude,&counter,m_lora_app_data.buffer); // gps lat ve long degerleri 1000 ile carpılabilir !!!!
  floatAddByte(longtitude,&counter,m_lora_app_data.buffer);
  floatAddByte(tempAltitudeLong,&counter,m_lora_app_data.buffer);  
  floatAddByte(tempAltitudeShort,&counter,m_lora_app_data.buffer);
  floatAddByte(tempSpeed,&counter,m_lora_app_data.buffer);
  floatAddByte(heading,&counter,m_lora_app_data.buffer);  
  byteAddByte(uydu,&counter,m_lora_app_data.buffer);  

  floatAddByte(pressure,&counter,m_lora_app_data.buffer);
  floatAddByte(tempTemperature,&counter,m_lora_app_data.buffer);
  floatAddByte(voltage,&counter,m_lora_app_data.buffer);
  
  counter=0;
  /*
  Serial.print("Latitude:");
  Serial.print(latitude);
  Serial.print("Longtitude:");
  Serial.print(longtitude);
  Serial.print("GPS-Altitude:");
  Serial.print(tempAltitudeLong);
  Serial.print("GPS-Altitude-2:");
  Serial.print(tempAltitudeShort);
  Serial.print("Speed:");
  Serial.print(tempSpeed);
  Serial.print("Heading:");
  Serial.print(heading);
  Serial.print("Uydu Sayısı:");
  Serial.print(uydu);
  Serial.print("BMP180-Basınç:");
  Serial.print(pressure);
  Serial.print("BMP180-Sıcaklık:");
  Serial.print(tempTemperature);
  Serial.print("Voltage:");
  Serial.print(voltage);  
  
  Serial.println();
 */
}


void printGPSandSensorData()
{
    lastTime = millis(); //Update the timer

    byte fixType = myGPS.getFixType();

    Serial.print(F("FixType: "));
    Serial.print(fixType);    

    int SIV = myGPS.getSIV();
    Serial.print(F(" Sats: "));
    Serial.print(SIV);

    float flat = myGPS.getLatitude() / 10000000.f;
   
    Serial.print(F(" Lat: "));
    Serial.print(flat);    

    float flong = myGPS.getLongitude() / 10000000.f;    
    Serial.print(F(" Long: "));
    Serial.print(flong);        

    float altitude = myGPS.getAltitude() / 1000;
    Serial.print(F(" Alt: "));
    Serial.print(altitude);
    Serial.print(F(" (m)"));

    //float speed = myGPS.getGroundSpeed() * 0.0036f;
    //Serial.print(F(" Speed: "));
    //Serial.print(speed);
    //Serial.print(F(" (km/h)"));

    //long heading = myGPS.getHeading() / 100000;
    //Serial.print(F(" Heading: "));
    //Serial.print(heading);
    //Serial.print(F(" (degrees)"));
       
    Serial.print(" Time: ");    
    Serial.print(myGPS.getYear());
    Serial.print("-");
    Serial.print(myGPS.getMonth());
    Serial.print("-");
    Serial.print(myGPS.getDay());
    Serial.print(" ");
    Serial.print(myGPS.getHour());
    Serial.print(":");
    Serial.print(myGPS.getMinute());
    Serial.print(":");
    Serial.print(myGPS.getSecond());

    Serial.print(" Temp: ");
    Serial.print(bmp280.getTemperature());
    Serial.print(" C");
   
    Serial.print(" Press: ");    
    Serial.print(bmp280.getPressure());
    Serial.print(" hPa");
    
    Serial.print(" Voltage: ");    
    Serial.print(readBatt());
    Serial.print(" V");
    
    Serial.println();
}
 
//SSDV FOTOGRAFI FLASHA KAYIT ARDINDAN SSDV FORMATINA DÖNÜŞTÜRÜP FLASHA KAYIT EDİLİP KAYIT EDİLEN DOSYA ÜZERİNDEN GÖNDERİLEN KISIM AYRICA GÜÇ KESİNTİSİ OLURSA KALAN YERDEN DEVAM EDİLİP TAMAMLANANA KADAR GÖNDERİM YAPILIYOR.
int iread(uint8_t* buffer,int numBytes,uint16_t cbIndex ){

File file2 = SPIFFS.open(FILE_PHOTO, FILE_READ);
 
 // have we reached past end of imagebuffer
  if((cbIndex + numBytes ) < lenn ){    
    bufSize = numBytes;  
  }
  else{  
    bufSize = lenn - cbIndex; // fb->len
    indexson=5; // işlemlerini sonlandırmak için    
}

// clear the dest buffer
memset(buffer,0,numBytes);

file2.seek(cbIndex,SeekSet); // index okuması için flash hafızadaki resim datasının bufferındaki cursoru ayarlıyoruz.

file2.read(buffer,bufSize);

if(indexson==5){
indexson=2; //bir daha döngüye girmesin diye 2 yazdık bir amac yok başka sayıda olabilir!
file2.close();    
}
  return bufSize;
}

int process_ssdv(){
   if(imageID<EEPROM.read(14)) imageID=EEPROM.read(14); //uçuş bitip 2.resim çekilirken reset alıp koddaki aynı ımageıd alıp çökme olmasını engellemek için 
  // initialise ssdv config structure
  ssdv_enc_init(&ssdv, SSDV_TYPE_NOFEC, (char *)callsign, imageID++, ssdvQuality);
  // set the output lora packet buffer for ssdv where the final ssdv packet will end up
  ssdv_enc_set_buffer(&ssdv, flashBuff);

  File filessdvv = SPIFFS.open(SSDVDATA, FILE_APPEND);
 
   while(1){
 
    while((c = ssdv_enc_get_packet(&ssdv)) == SSDV_FEED_ME)
    {          
      indexx += iread(imgBuff, IMG_BUFF_SIZE,indexx);
      Serial.print("index = ");
      Serial.println(indexx);    
      ssdv_enc_feed(&ssdv, imgBuff, IMG_BUFF_SIZE);    
    }
   
    if(indexx>640) indexsayac++;
   
    if((c == SSDV_EOI))
    {
     filessdvv.close();  
     EEPROM.writeFloat(10,indexsayac);
     EEPROM.commit();
     Serial.println("ssdv EOI");  
     Photo_Flash_To_LoRa();
     break;    
    }
    else if(c != SSDV_OK)
    {        
        Serial.println("ssdv Error");
        break;
    }
   
      if (!filessdvv) {
        Serial.println("Failed to open file in append mode");
      }
      else {    
 
      filessdvv.seek(cur,SeekSet);  
      filessdvv.write(flashBuff,256);
      cur+=256; }
}
      cur=0;
      indexx=0;
}

void Photo_Flash_To_LoRa(){
      
    EEPROM.write(2,imageID);
    EEPROM.commit();    
   
    File filessdv2 = SPIFFS.open(SSDVDATA, FILE_READ);  //Bu dosyayı global değişken yapıp eksikfoto gönder donksiyonunda kapatabiliriz şuan sıkıntısız çalışmaktadır.Yerini değiştirmedim. 
    if (!filessdv2){
        Serial.println("Failed to open file in read mode");
    }    
   
    while(1){ // SSDV kendi datasını oluşturup flasha yazmayı bitirdikten sonra kod buraya gelip flashtan ssdv datalarını okuyor.ve gönderim yapıyor.      
    
/*   if(lmh_join_status_get() != LMH_SET){ // UÇUŞ KODUNDAN BURASI AKTİF EDİLECEK!!  !!!! KAMERA DATASI GÖNDERİRKEN LORAWAN BAĞLANTISI KOPARSA BOŞ PAKET ATMAMAK İÇİN!!!
      lorawan_baslat=5;
      Serial.println("Did not join network, skip sending frame");   
      delay(10000); //LoRaWAN Initialize için bekliyoruz...   
      save_photo(); // Bazen test ederken reset attığımızda lorawana bağlanmadı tekrar bağlanmasını bekledikten sonra aynı resim datasını iletmesi için bu fonksiyonu tekrar cağırdık.
    }*/
    
   if((millis()- last_packet < TX_INTERVAL)|| (voltage > battMin)){  
    
   m_lora_app_data.port = LORAWAN_APP_PORT;
   
   Serial.print(" packet sent");
   Serial.println(ssdvPacketCount);
   
   filessdv2.seek(cur2,SeekSet);  
   Serial.print("Read Cursor Pozisyonu=");
   Serial.println(filessdv2.position());  
   filessdv2.read(ssdvBuff,256);
   
   byte currdusukbyte = lowByte(cur2);
   byte curryuksekbyte = highByte(cur2);
   EEPROM.write(8,currdusukbyte);
   EEPROM.write(9,curryuksekbyte);
   EEPROM.commit();
   
   for(uint16_t i = 0; i < 256; i++) {
      ssdvBuff[i] = ssdvBuff[i+1]; } //LoRa buffer üzerinden max 255byte gönderebildiğimiz için 1 byte eksik gönderiyoruz. Gateway onu en sonda ekleme yapıyor.

   EEPROM.write(3,ssdvPacketCount);
   EEPROM.commit();

   lmh_error_status error;   
   for(uint8_t a=0; a<PaketSayi;a++){

   switch(PaketSayi)
   {
     case 2:{ // paketi eğer 2'ye bölerek gönderme yapıyorsak burası çalışacaktır.               
                 Serial.println("Data 2 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 134);//buffer temizle
                   
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 2; 
 
                 for(int z=0;z<128;z++)m_lora_app_data.buffer[(5+z)]=ssdvBuff[z+(a*128)]; // 1 ssdvbuffdan 1 adet fazla data okuduk fakat gateway tarafında onu kullanmayacağız.!!
                 
                 m_lora_app_data.buffsize = 133;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); //gönderme kısmı                 
     }break;
      
     case 3:{ // paketi eğer 3'ye bölerek gönderme yapıyorsak burası çalışacaktır.
                 Serial.println("Data 3 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 91);//buffer temizle
                 
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 3; 
 
                 for(int z=0;z<85;z++)m_lora_app_data.buffer[(5+z)]=ssdvBuff[z+(a*85)];
                 
                 m_lora_app_data.buffsize = 90;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); //gönderme kısmı                   
     }break;
                
     case 6:{// paketi eğer 6'ya bölerek gönderme yapıyorsak burası çalışacaktır.     

                 Serial.println("Data 6 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 49);//buffer temizle
                 
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 6;
          
                 for(int z=0;z<43;z++)m_lora_app_data.buffer[(5+z)]=ssdvBuff[z+(a*43)];     
                 
                 m_lora_app_data.buffsize = 48;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); //gönderme kısmı                     
     }break;
}

  if (error == LMH_SUCCESS)
  {
    // the packet was successfully transmitted  
    EEPROM.write(4,a);  
    EEPROM.commit();
    Serial.print(EEPROM.read(4));
    Serial.println(". Paket ");  
       
    if(a==(PaketSayi-1)){
    ssdvPacketCount++;
    EEPROM.write(3,ssdvPacketCount);
    cur2+=256;
         
    byte ccurrdusukbyte = lowByte(cur2);
    byte ccurryuksekbyte = highByte(cur2);
    EEPROM.write(8,ccurrdusukbyte);
    EEPROM.write(9,ccurryuksekbyte);
    EEPROM.commit();  
  }
   Serial.print("Data: ");
   Serial.println((char*)m_lora_app_data.buffer);
   Serial.print("Size: ");
   Serial.println(m_lora_app_data.buffsize);
   Serial.print("Port: ");
   Serial.println(m_lora_app_data.port);
      
   Serial.println(F("success!"));
   Serial.printf("lmh_send result %d\n", error);  
  }
  
  while(!lorawan_tx_finish){Serial.print(""); } //seriali kaldırmamız gerekiyor if koşulu yazsam bile çalışmadı while döngüsünden cıkmadı.
  lorawan_tx_finish=false;
      
//delay(region_wait_time); //EU868 DR5-SF7 90BYTE İÇİN HER PAKET ARASI BEKLENECEK SÜRE 24.1sn  .YER TESTİ İÇİN KULLANIYORUZ. UÇUŞTA BURASI KAPATILACAK.
  CamTrackerSleep(region_wait_time); // UÇUŞ KODUNDA AKTİF EDECEĞİZ VE BEKLEME ANLARINDA UYKUYA GEÇİŞ YAPACAĞIZ.  
}
  
/* // Bu kısım light-sleep kısmının test edilmesi için yazıldı.
  if(EEPROM.read(3)==5 && EEPROM.read(4)==1){
   Serial.println("Light-Sleep giriliyor...");  
    
   LedOFF;   
   CameraOFF;
   GpsOFF;
   delay(5000);
   
   esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
   esp_light_sleep_start();
   }     */         

   Serial.print("SSDV Packet Number:");    
   Serial.println(EEPROM.read(3));   // Epproma bakıyoruz kac ssdv paket gönderdik.
   memset(ssdvBuff,0,256);

   if(ssdvPacketCount==(indexsayac-1)){ //Resim toplam data uzunluğuna geldik mi veya geçtik mi?  
       
      for(int i=0; i<14; i++)EEPROM.write(i,0);
      EEPROM.write(14,imageID); //uçuş bittikten sonra image ıd hafızada tutuyoruz!!
      EEPROM.commit();  
      SPIFFS.remove(SSDVDATA); // SSDV den gelen dataları kayıt ettiğimiz dosya bir öncekini uçuş başlamadan önce sileceğiz. Yeni bir dosya ismi yazarak yeni ssdv datalarını oraya kayıt edeceğiz.
      
      cur2=0;
      indexsayac=0;
      ssdvPacketCount=0;      
      GpsOFF; // ikinci resim çekilirken ı2c hatası almamak için GPS'i kapatıyoruz!! GPS kodda ayarladığımız süre sonra kendini açarak konum bilgini verecek.
      CameraON;
      CameraSetup=false;
      catchEror=0;     
      
      filessdv2.close();
      Serial.print("!!!!!SSDV BİTTİ!!!");      
      break;
   }
 }else{
  GpsON; //!!GPS açıyoruz!!
  break; //Döngüden çıkıp sensör datasını gönderiyoruz!!
 }
    
} 
}

void eksikfoto_tamamla(){ // Enerji kesilirse veya kart reset alırsa ve resim çekilip flasha kayıt edilmişse bu fonksiyondan devam ediyoruz.

byte h = EEPROM.read(0);
byte p = EEPROM.read(1);

byte t = EEPROM.read(8);
byte s = EEPROM.read(9);
cur3 = t + (s<<8);
   
indexsayac=EEPROM.readFloat(10);  
imageID=EEPROM.read(2); // Kaçıncı resmi aktarıyoruz.
ssdvPacketCount_2=EEPROM.read(3); // En son kacıncı 3lü paket attık? 20. mi 25. mi ?

Serial.print("Güç sistemine ikinci giriş başarılı şu an flash hafızadaki konum:");
Serial.println(cur3); // Flashtan en son hangi kısımda kaldık kontrol ediyoruz.
   
File filessdv4 = SPIFFS.open(SSDVDATA, FILE_READ);  
       
while(1){   
/*
   if(lmh_join_status_get() != LMH_SET){ //UÇUŞ KODUNDA AKTİF EDİLECEK!! !!!! KAMERA DATASI GÖNDERİRKEN LORAWAN BAĞLANTISI KOPARSA BOŞ PAKET ATMAMAK İÇİN!!!
      lorawan_baslat=5;
      Serial.println("Did not join network, skip sending frame"); 
      delay(10000); //LoRaWAN Initialize için bekliyoruz...
      save_photo();// Bazen test ederken reset attığımızda lorawana bağlanmadı tekrar bağlanmasını bekledikten sonra aynı resim datasını iletmesi için bu fonksiyonu tekrar cağırdık.
    }*/

   if((millis()- last_packet < TX_INTERVAL)|| (voltage > battMin)){ 
   
   m_lora_app_data.port = LORAWAN_APP_PORT;
   
   if (!filessdv4){
        Serial.println("Failed to open file in reading mode");
    }
   else{
   
   memset(yeniBuff,0,256);   // önceden data olabilir bufferı doldurmadan temizlemek iyidir :)  
   filessdv4.seek(cur3,SeekSet); // cursor için önemli nokta cursor x.kısımda olsun oradaki değeri okuyarak başlıyor kursor 0 dan 256 kadar toplam 256 data okuyor 256.yı okumuyor! Ardından 256 yapıp oradan devam ediyoruz.

   Serial.print("Read Cursor Pozisyonu=");
   Serial.println(filessdv4.position());  // Cursor pozisyonunu kontrol ediyoruz.
   filessdv4.read(yeniBuff,256);
   
   byte curdusukbyte = lowByte(cur3); // cursor 2 byte olduğu için eppromda iki ayrı yerde tutup birleştiriyoruz.
   byte curyuksekbyte = highByte(cur3);
   EEPROM.write(8,curdusukbyte);
   EEPROM.write(9,curyuksekbyte);
   EEPROM.commit(); // Commit olmazsa epproma yazmaz.  
  }
   
    for(uint16_t i = 0; i < 256; i++) {
      yeniBuff[i] = yeniBuff[i+1];    } // LoRa max 255 byte gönderebiliyoruz o yüzden 1 byte eksik gönderip gatewayde ekleyeceğiz.
   
   Serial.print(" packet sent");
   Serial.println(ssdvPacketCount_2);  
   
   EEPROM.write(2,imageID); // Resim görüntüsü tek sefer yazmak için üst tarafa aldık.
   EEPROM.write(3,ssdvPacketCount_2);
   EEPROM.commit();

   // Eğer kart reset alır veya güç problemi olursa  hangi parcanın kacıncısındayız onun kontrol ediyoruz. 25 paketin 1. parçası mı? 2.parçası mı? 3. parçası mı?
   if(EEPROM.read(4)!=(PaketSayi-1))     devamm=EEPROM.read(4)+1;
   else devamm=0;

   lmh_error_status error;
   for(uint8_t a=devamm; a<PaketSayi;a++){
   switch(PaketSayi)
    {
     case 2:{// paketi eğer 2'ye bölerek gönderme yapıyorsak burası çalışacaktır.               
                 Serial.println("Data 2 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 134);//buffer temizle
                   
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount_2;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 2; 
 
                 for(int z=0;z<128;z++)m_lora_app_data.buffer[(5+z)]=yeniBuff[z+(a*128)]; // 1 ssdvbuffdan 1 adet fazla data okuduk fakat gateway tarafında onu kullanmayacağız.!!
                 
                 m_lora_app_data.buffsize = 133;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); // datanın gönderme kısmı  
     }break;
      
     case 3:{// paketi eğer 3'ye bölerek gönderme yapıyorsak burası çalışacaktır.
                 Serial.println("Data 3 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 91);//buffer temizle
                 
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount_2;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 3; 
 
                 for(int z=0;z<85;z++)m_lora_app_data.buffer[(5+z)]=yeniBuff[z+(a*85)];
                 
                 m_lora_app_data.buffsize = 90;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); // datanın gönderme kısmı           
     }break;
                
     case 6:{// paketi eğer 6'ya bölerek gönderme yapıyorsak burası çalışacaktır.     

                 Serial.println("Data 6 pakette gönderiliyor!!");
                 memset(m_lora_app_data.buffer, 0, 49);//buffer temizle
                 
                 if(a==(PaketSayi-1)) {      
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount_2;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 6;  
    
                 for(int z=0;z<40;z++)m_lora_app_data.buffer[(5+z)]=yeniBuff[z+(a*43)]; //son kalan data gönderilimi yapılıyor.

                 m_lora_app_data.buffer[45]= 0;
                 m_lora_app_data.buffer[46]= 0;
                 }else{ 
                 m_lora_app_data.buffer[0]= 0x55;
                 m_lora_app_data.buffer[1]= imageID;
                 m_lora_app_data.buffer[2]= ssdvPacketCount_2;
                 m_lora_app_data.buffer[3]= a;
                 m_lora_app_data.buffer[4]= 6;
          
                 for(int z=0;z<43;z++)m_lora_app_data.buffer[(5+z)]=yeniBuff[z+(a*43)];     
                 }
                 
                 m_lora_app_data.buffsize = 48;
                 error = lmh_send(&m_lora_app_data, CurrentConfirm); // datanın gönderme kısmı                     
     }break;
}    
        
    if (error == LMH_SUCCESS)
    {
      // the packet was successfully transmitted    
      EEPROM.write(4,a);
      EEPROM.commit();  //Gönderme işlemi başarılıysa giden parcayı epproma yazıyoruz.
      Serial.print(EEPROM.read(4));
      Serial.println(". Paket ");   
     
      if(a==(PaketSayi-1)){
        ssdvPacketCount_2++;  
        EEPROM.write(3,ssdvPacketCount_2);

        cur3+=256;

        byte curdusukkbyte = lowByte(cur3);
        byte curyuksekkbyte = highByte(cur3);
        EEPROM.write(8,curdusukkbyte);
        EEPROM.write(9,curyuksekkbyte);
        EEPROM.commit();  
      }
      
     Serial.print("Data: ");
     Serial.println((char*)m_lora_app_data.buffer);
     Serial.print("Size: ");
     Serial.println(m_lora_app_data.buffsize);
     Serial.print("Port: ");
     Serial.println(m_lora_app_data.port);
     
     Serial.println(F("success!"));
     Serial.printf("lmh_send result %d\n", error);    
    }
    
  while(!lorawan_tx_finish){Serial.print(""); } //seriali kaldırmamız gerekiyor if koşulu yazsam bile çalışmadı while döngüsünden cıkmadı   
  lorawan_tx_finish=false;
  //delay(region_wait_time); //EU868 DR5-SF7 90BYTE İÇİN HER PAKET ARASI BEKLENECEK SÜRE 24.1sn .YER TESTİ İÇİN KULLANIYORUZ. UÇUŞTA BURASI KAPATILACAK.
  CamTrackerSleep(region_wait_time); // UÇUŞ KODUNDA AKTİF EDECEĞİZ VE BEKLEME ANLARINDA UYKUYA GEÇİŞ YAPACAĞIZ.
}
/* // Bu kısım light-sleep denemek için yapıldı.
  if(EEPROM.read(3)==10 && EEPROM.read(4)==1){
   Serial.println("Light-Sleep giriliyor...");  
    
   LedOFF;   
   CameraOFF;
   GpsOFF;
   delay(5000);
   
   esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * mS_TO_S_FACTOR);
   esp_light_sleep_start();
   }
   
  if(EEPROM.read(3)==15 && EEPROM.read(4)==1){
   Serial.println("Light-Sleep giriliyor...");  
    
   LedOFF;   
   CameraOFF;
   GpsOFF;
   delay(5000);
   
   esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * mS_TO_S_FACTOR);
   esp_light_sleep_start();
   }*/
   
   Serial.print("SSDV Packet Number:");    
   Serial.println(EEPROM.read(3));   // Epproma bakıyoruz kac ssdv paket gönderdik.     
   memset(yeniBuff,0,256);
 
   if(ssdvPacketCount_2==(indexsayac-1)){ //Resim toplam data uzunluğuna geldik mi veya geçtik mi?  
   
      for(int i=0; i<14; i++)EEPROM.write(i,0); // Tüm eppromu temizliyoruz.
      EEPROM.write(14,imageID); //uçuş bittikten sonra image ıd hafızada tutuyoruz!!
      EEPROM.commit();  
      SPIFFS.remove(SSDVDATA); //SSDV den gelen dataları kayıt ettiğimiz dosya bir öncekini uçuş başlamadan önce sileceğiz. Yeni bir dosya ismi yazarak yeni ssdv datalarını oraya kayıt edeceğiz.
      
      cur2=0;
      cur3=0;
      indexsayac=0;
      ssdvPacketCount=0;
      ssdvPacketCount_2=0;      
      GpsOFF; // ikinci resim çekilirken ı2c hatası almamak için GPS'i kapatıyoruz!! GPS kodda ayarladığımız süre sonra kendini açarak konum bilgini verecek.     
      CameraON;
      CameraSetup=false;
      catchEror=0;
                         
      filessdv4.close(); // Flashtaki acıp okuduğumuz dosyayı kapatıyoruz.
      Serial.print("SSDV EOI SONN !!!");          
      break;
    }

  }else{  
    GpsON; //!!GPS açıyoruz!!
    break;  //Döngüden çıkıp sensör datasını gönderiyoruz!!
  }

} 
}

/*************************************************************************************************************************************************/
// Check if photo capture was successful
bool checkPhoto( fs::FS &fs ) {
  File f_pic = fs.open( FILE_PHOTO );
  unsigned int pic_sz = f_pic.size();
  Serial.print("Pictures Size:");
  Serial.println(pic_sz);
  return ( pic_sz > 100); 
  //return ( pic_sz > 100 && pic_sz < 9100  );// Bu kısmı istediğimiz boyuta göre ayarlayabiliriz.Böylelikle 8 saate isteline boyut arasına uyan fotoğrafı göndermiş oluruz. 
}

/*************************************************************************************************************************************************/


void save_photo()
{
 
 
Serial.print("CHECK : ");
Serial.println( EEPROM.read(7)); // Güç kesintisinden dolayı devam eden resim mi yoksa yeni resim mi eppromdan kontrol ediyoruz.Devam eden resimse kodu farklı devam ediyor.

if(EEPROM.read(7)==106){
     
   eksikfoto_tamamla();  
}

else{
  
  camera_fb_t *fb = NULL;  
  bool ok=0; // Boolean indicating if the picture has been taken correctly

  do {
  Serial.print("Taking picture: ");  
 //delay(10000); //Kamera fotograf cekerken ne kadar dataakım tüketiyor görmek için sonradan kaldırılacak!  
  fb = esp_camera_fb_get();
  
  if (!fb) {
      Serial.println("Camera capture failed");
      save_photo(); // kamera yaklayamazsa
    }
      
    // Photo file name
    Serial.printf("Picture file name: %s\n", FILE_PHOTO);
    File file = SPIFFS.open(FILE_PHOTO, FILE_WRITE);
    delay(500);
    // Insert the data in the photo file
   
    if (!file) {
      Serial.println("Failed to open file in writing mode");
    }
    else {
      file.write(fb->buf, fb->len); // payload (image), payload length      
      res_len= (uint16_t)fb->len;
      Serial.print("The picture has been saved in ");
      Serial.print(FILE_PHOTO);
      Serial.print(" - Size: ");
      Serial.print(file.size());
      Serial.println(" bytes");    }

      byte resdusukbyte = lowByte(res_len);
      byte resyuksekbyte = highByte(res_len);
      EEPROM.write(0,resdusukbyte);
      EEPROM.write(1,resyuksekbyte);
      EEPROM.commit();
      byte d = EEPROM.read(0);
      byte y = EEPROM.read(1);
      lenn = d + (y<<8); // bu kısım iread fonksiyonunun resmi flash üzerinden okuyabilmesi için önemlidir.Kesinlikle kaldırmayalım!!!!
   
      file.close(); 
      esp_camera_fb_return(fb); 

      
      // check if file has been correctly saved in SPIFFS
      ok = checkPhoto(SPIFFS);
      }while (!ok); 
      
      // önceden_resim_cekildi_mi?
      EEPROM.write(7,106); // Fotograf çekildiğinde bu bu fonksiyona girip flasha fotograf cekildiğini anlamamız için bu değeri girdik reset alırsak bu değeri kontrol edip devam ediyoruz.
      EEPROM.commit();
      
      esp_camera_deinit(); //akım tüketimi ve tekrardan resim atılması için yaptık!!
      CameraOFF; //Fotoğraf başarıyla çekildi flasha kayıt ettik ve epproma bu bilgiyi verdik artık kamerayı kapatabiliriz.
      
      process_ssdv();  //SSDV dönüşüm işlemleri başlatan fonskiyon
 }
}

/*************************************************************************************************************************************************/

void setupUBloxDynamicModel() {
    // If we are going to change the dynamic platform model, let's do it here.
    // Possible values are:
    // PORTABLE, STATIONARY, PEDESTRIAN, AUTOMOTIVE, SEA, AIRBORNE1g, AIRBORNE2g, AIRBORNE4g, WRIST, BIKE
    //DYN_MODEL_AIRBORNE4g model increases ublox max. altitude limit from 12.000 meters to 50.000 meters.
    if (myGPS.setDynamicModel(DYN_MODEL_AIRBORNE4g) == false) // Set the dynamic model to DYN_MODEL_AIRBORNE4g
    {
      Serial.println(F("***!!! Warning: setDynamicModel failed !!!***"));
    }
    else
    {
      ublox_high_alt_mode_enabled = true;
      #if defined(DEVMODE)
        Serial.print(F("Dynamic platform model changed successfully! : "));
        Serial.println(myGPS.getDynamicModel());
      #endif  
    }
}
/**************************** LIGHT SLEEP FUNCTION  ****************************************************/ 
void CamTrackerSleep(uint32_t sleeptime){
   Serial.println("...LightSleep Başlatılıyor..."); //test için sonradan kaldırılabilir. 
   delay(100); //test için sonradan kaldırılabilir.    
   LedOFF;   
   CameraOFF;
   GpsOFF;
   
   esp_sleep_enable_timer_wakeup(sleeptime * mS_TO_S_FACTOR); //uS olarak yazmamız gerek 
   esp_light_sleep_start();
}

/**************************** SENSÖR VERİLERİNİ BYTE CEVİRME İŞLEMLERİNİ YAPTIĞIMIZ ALAN BUFFERA BYTE-BYTE YAZMAK İÇİN YAPMAK ZORUNDAYIZ   **********************/
void floatAddByte(float data,int *count,uint8_t *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void intAddByte(int data,int *count,uint8_t *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,uint8_t *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void uint16AddByte(uint16_t data,int *count,uint8_t *array){
  for(int i=0;i<2;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void byteAddByte(byte data,int *count,uint8_t *array){
    array[*count]=data;
    (*count)++;
}
