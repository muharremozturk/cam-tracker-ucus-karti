EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_Pressure-Sensors-Transducers:BMP280 U5
U 1 1 61172C8B
P 5600 3550
F 0 "U5" H 5250 3900 60  0000 L CNN
F 1 "BMP280" H 5800 3300 60  0000 L CNN
F 2 "digikey-footprints:LGA-8_2x2.5mm_BMP280" H 5800 3750 60  0001 L CNN
F 3 "https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BMP280-DS001.pdf" H 5800 3850 60  0001 L CNN
F 4 "828-1064-1-ND" H 5800 3950 60  0001 L CNN "Digi-Key_PN"
F 5 "BMP280" H 5800 4050 60  0001 L CNN "MPN"
F 6 "Sensors, Transducers" H 5800 4150 60  0001 L CNN "Category"
F 7 "Pressure Sensors, Transducers" H 5800 4250 60  0001 L CNN "Family"
F 8 "https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BMP280-DS001.pdf" H 5800 4350 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/bosch-sensortec/BMP280/828-1064-1-ND/6136315" H 5800 4450 60  0001 L CNN "DK_Detail_Page"
F 10 "SENSOR PRESSURE ABS" H 5800 4550 60  0001 L CNN "Description"
F 11 "Bosch Sensortec" H 5800 4650 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5800 4750 60  0001 L CNN "Status"
	1    5600 3550
	1    0    0    -1  
$EndComp
Text HLabel 5100 3550 0    50   Input ~ 0
SDA
Text HLabel 5100 3450 0    50   Input ~ 0
SCL
Wire Wire Line
	5100 3450 5200 3450
Wire Wire Line
	5200 3550 5100 3550
$Comp
L power:GND #PWR044
U 1 1 61173AE1
P 5650 3950
F 0 "#PWR044" H 5650 3700 50  0001 C CNN
F 1 "GND" H 5655 3777 50  0000 C CNN
F 2 "" H 5650 3950 50  0001 C CNN
F 3 "" H 5650 3950 50  0001 C CNN
	1    5650 3950
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 61176BE9
P 5650 2750
AR Path="/6115C67C/61176BE9" Ref="#PWR?"  Part="1" 
AR Path="/611727C1/61176BE9" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 5650 2600 50  0001 C CNN
F 1 "+3.3V" H 5665 2923 50  0000 C CNN
F 2 "" H 5650 2750 50  0001 C CNN
F 3 "" H 5650 2750 50  0001 C CNN
	1    5650 2750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 61177916
P 6100 3400
AR Path="/6115C67C/61177916" Ref="#PWR?"  Part="1" 
AR Path="/611727C1/61177916" Ref="#PWR043"  Part="1" 
F 0 "#PWR043" H 6100 3250 50  0001 C CNN
F 1 "+3.3V" H 6115 3573 50  0000 C CNN
F 2 "" H 6100 3400 50  0001 C CNN
F 3 "" H 6100 3400 50  0001 C CNN
	1    6100 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3450 6100 3450
Wire Wire Line
	6100 3450 6100 3400
Wire Wire Line
	6000 3550 6100 3550
Wire Wire Line
	6100 3550 6100 3450
Connection ~ 6100 3450
Wire Wire Line
	5600 3150 5600 3050
Wire Wire Line
	5700 3050 5700 3150
Wire Wire Line
	5600 3850 5600 3950
Wire Wire Line
	5600 3950 5650 3950
Wire Wire Line
	5700 3950 5650 3950
Wire Wire Line
	5700 3850 5700 3950
Connection ~ 5650 3950
$Comp
L 805_cap:Cap C?
U 1 1 611838B0
P 5950 2900
AR Path="/609302C0/611838B0" Ref="C?"  Part="1" 
AR Path="/6093034B/611838B0" Ref="C?"  Part="1" 
AR Path="/6092FBF4/611838B0" Ref="C?"  Part="1" 
AR Path="/6115C67C/611838B0" Ref="C?"  Part="1" 
AR Path="/611727C1/611838B0" Ref="C17"  Part="1" 
F 0 "C17" V 5954 3003 50  0000 L CNN
F 1 "100nF" V 6045 3003 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5950 2900 50  0001 L BNN
F 3 "" H 5950 2900 50  0001 L BNN
	1    5950 2900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR042
U 1 1 611854EF
P 5950 3100
F 0 "#PWR042" H 5950 2850 50  0001 C CNN
F 1 "GND" H 5955 2927 50  0000 C CNN
F 2 "" H 5950 3100 50  0001 C CNN
F 3 "" H 5950 3100 50  0001 C CNN
	1    5950 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3050 5650 3050
Wire Wire Line
	5950 2800 5650 2800
Wire Wire Line
	5650 2800 5650 2750
Wire Wire Line
	5650 2800 5650 3050
Connection ~ 5650 2800
Connection ~ 5650 3050
Wire Wire Line
	5650 3050 5700 3050
$EndSCHEMATC
