EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_GPS:MAX-M8Q U6
U 1 1 61178408
P 5600 3450
F 0 "U6" H 5150 4100 50  0000 C CNN
F 1 "MAX-M8Q" H 5950 2800 50  0000 C CNN
F 2 "RF_GPS:ublox_MAX" H 6000 2800 50  0001 C CNN
F 3 "https://www.u-blox.com/sites/default/files/MAX-M8-FW3_DataSheet_%28UBX-15031506%29.pdf" H 5600 3450 50  0001 C CNN
	1    5600 3450
	1    0    0    -1  
$EndComp
NoConn ~ 5000 3050
NoConn ~ 5000 3150
NoConn ~ 5000 3650
NoConn ~ 5000 3750
NoConn ~ 6200 3750
NoConn ~ 6200 3250
$Comp
L 805_bobbin:Bob L2
U 1 1 6117E476
P 7200 3800
F 0 "L2" V 7250 3700 50  0000 C CNN
F 1 "27nH" V 7150 3650 50  0000 C CNN
F 2 "others:L_0805_2012Metric" H 7200 3800 50  0001 L BNN
F 3 "" H 7200 3800 50  0001 L BNN
	1    7200 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R?
U 1 1 6117F6F3
P 5900 1700
AR Path="/609302C0/6117F6F3" Ref="R?"  Part="1" 
AR Path="/6093043F/6117F6F3" Ref="R?"  Part="1" 
AR Path="/6092FBF4/6117F6F3" Ref="R?"  Part="1" 
AR Path="/6115C67C/6117F6F3" Ref="R?"  Part="1" 
AR Path="/61178059/6117F6F3" Ref="R14"  Part="1" 
F 0 "R14" V 6000 1650 50  0000 L CNN
F 1 "10K" V 5800 1600 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5940 1690 50  0001 C CNN
F 3 "~" H 5900 1700 50  0001 C CNN
	1    5900 1700
	0    -1   -1   0   
$EndComp
$Comp
L 805_cap:Cap C?
U 1 1 6118695E
P 6550 3450
AR Path="/609302C0/6118695E" Ref="C?"  Part="1" 
AR Path="/6093034B/6118695E" Ref="C?"  Part="1" 
AR Path="/6092FBF4/6118695E" Ref="C?"  Part="1" 
AR Path="/6115C67C/6118695E" Ref="C?"  Part="1" 
AR Path="/61178059/6118695E" Ref="C19"  Part="1" 
F 0 "C19" H 6550 3600 50  0000 L CNN
F 1 "47pF" H 6500 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6550 3450 50  0001 L BNN
F 3 "" H 6550 3450 50  0001 L BNN
	1    6550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 2750 5400 2400
Wire Wire Line
	5500 2750 5500 2400
Wire Wire Line
	5400 2400 5500 2400
Connection ~ 5500 2400
Wire Wire Line
	5700 2400 5700 2750
Wire Wire Line
	5550 1900 5550 1700
Wire Wire Line
	5550 1700 5750 1700
Wire Wire Line
	6150 1700 6050 1700
Text HLabel 6250 2100 2    50   Input ~ 0
GPS_Power
$Comp
L 805_cap:Cap C?
U 1 1 6118A7AD
P 4800 2600
AR Path="/609302C0/6118A7AD" Ref="C?"  Part="1" 
AR Path="/6093034B/6118A7AD" Ref="C?"  Part="1" 
AR Path="/6092FBF4/6118A7AD" Ref="C?"  Part="1" 
AR Path="/6115C67C/6118A7AD" Ref="C?"  Part="1" 
AR Path="/611727C1/6118A7AD" Ref="C?"  Part="1" 
AR Path="/61178059/6118A7AD" Ref="C18"  Part="1" 
F 0 "C18" V 4804 2703 50  0000 L CNN
F 1 "100nF" V 4895 2703 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4800 2600 50  0001 L BNN
F 3 "" H 4800 2600 50  0001 L BNN
	1    4800 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 1700 5550 1600
Connection ~ 5550 1700
$Comp
L power:GND #PWR046
U 1 1 6118BE34
P 4800 2800
F 0 "#PWR046" H 4800 2550 50  0001 C CNN
F 1 "GND" H 4805 2627 50  0000 C CNN
F 2 "" H 4800 2800 50  0001 C CNN
F 3 "" H 4800 2800 50  0001 C CNN
	1    4800 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR048
U 1 1 6118C67A
P 5600 4200
F 0 "#PWR048" H 5600 3950 50  0001 C CNN
F 1 "GND" H 5605 4027 50  0000 C CNN
F 2 "" H 5600 4200 50  0001 C CNN
F 3 "" H 5600 4200 50  0001 C CNN
	1    5600 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4200 5600 4150
Wire Wire Line
	4800 2400 5400 2400
Connection ~ 5400 2400
Wire Wire Line
	4800 2500 4800 2400
Text HLabel 4900 3450 0    50   Input ~ 0
SDA
Text HLabel 4900 3350 0    50   Input ~ 0
SCL
Wire Wire Line
	4900 3350 5000 3350
Wire Wire Line
	5000 3450 4900 3450
$Comp
L power:GND #PWR047
U 1 1 6118E6DC
P 7200 4050
F 0 "#PWR047" H 7200 3800 50  0001 C CNN
F 1 "GND" H 7205 3877 50  0000 C CNN
F 2 "" H 7200 4050 50  0001 C CNN
F 3 "" H 7200 4050 50  0001 C CNN
	1    7200 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 4050 7200 4000
Wire Wire Line
	6450 3450 6200 3450
Wire Wire Line
	6750 3450 7200 3450
Wire Wire Line
	7200 3450 7200 3600
$Comp
L Device:Antenna AE1
U 1 1 6121678C
P 7200 3100
F 0 "AE1" H 7280 3089 50  0000 L CNN
F 1 "Antenna" H 7280 2998 50  0000 L CNN
F 2 "others:ANT_GPS_RAINSUN1003" H 7200 3100 50  0001 C CNN
F 3 "~" H 7200 3100 50  0001 C CNN
	1    7200 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 3450 7200 3300
Connection ~ 7200 3450
NoConn ~ 5850 2750
$Comp
L Device:Q_PMOS_GSD Q4
U 1 1 611F0247
P 5650 2100
F 0 "Q4" H 5855 2054 50  0000 L CNN
F 1 "Q_PMOS_GSD" H 5855 2145 50  0000 L CNN
F 2 "digikey-footprints:SOT-23-3" H 5850 2200 50  0001 C CNN
F 3 "~" H 5650 2100 50  0001 C CNN
	1    5650 2100
	-1   0    0    1   
$EndComp
Wire Wire Line
	6150 1700 6150 2100
Wire Wire Line
	5500 2400 5550 2400
Wire Wire Line
	5850 2100 6150 2100
Connection ~ 6150 2100
Wire Wire Line
	6150 2100 6250 2100
Wire Wire Line
	5550 2400 5550 2300
Connection ~ 5550 2400
Wire Wire Line
	5550 2400 5700 2400
$Comp
L power:+3.3V #PWR?
U 1 1 6118A7A7
P 5550 1600
AR Path="/6115C67C/6118A7A7" Ref="#PWR?"  Part="1" 
AR Path="/611727C1/6118A7A7" Ref="#PWR?"  Part="1" 
AR Path="/61178059/6118A7A7" Ref="#PWR045"  Part="1" 
F 0 "#PWR045" H 5550 1450 50  0001 C CNN
F 1 "+3.3V" H 5565 1773 50  0000 C CNN
F 2 "" H 5550 1600 50  0001 C CNN
F 3 "" H 5550 1600 50  0001 C CNN
	1    5550 1600
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 6157A532
P 5600 2700
AR Path="/6115C67C/6157A532" Ref="#PWR?"  Part="1" 
AR Path="/611727C1/6157A532" Ref="#PWR?"  Part="1" 
AR Path="/61178059/6157A532" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 5600 2550 50  0001 C CNN
F 1 "+3.3V" H 5615 2873 50  0000 C CNN
F 2 "" H 5600 2700 50  0001 C CNN
F 3 "" H 5600 2700 50  0001 C CNN
	1    5600 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 2750 5600 2700
$EndSCHEMATC
